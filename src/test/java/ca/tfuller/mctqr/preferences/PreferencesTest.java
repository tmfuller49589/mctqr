package ca.tfuller.mctqr.preferences;

import org.junit.jupiter.api.Test;

import ca.tfuller.mctqr.io.PojoIo;
import ca.tfuller.mctqr.preferences.Preferences;

public class PreferencesTest {

	@Test
	public void saveTest() {
		Preferences pref = new Preferences();
		pref.setAnchorSize(13);
		pref.setBubbleSize(10);
		pref.setChoicesPerQuestion(4);
		pref.setColumns(3);
		pref.setFontSize(10);
		pref.setInstructor("T. Fuller");
		pref.setNumberOfQuestions(42);

		PojoIo<Preferences> prefIo = new PojoIo<>("prefs.txt", pref);
		prefIo.save();
	}

	@Test
	public void readTest() {
		Preferences pref = new Preferences();
		pref.setAnchorSize(13);
		pref.setBubbleSize(10);
		pref.setChoicesPerQuestion(4);
		pref.setColumns(3);
		pref.setFontSize(10);
		pref.setInstructor("T. Fullerxyz");
		pref.setNumberOfQuestions(42);

		PojoIo<Preferences> prefIo = new PojoIo<>("prefs.txt", pref);
		prefIo.load();
		pref.dump();
	}

}
