package ca.tfuller.mctqr.googleclassroom;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import com.google.api.services.classroom.model.Course;
import com.google.api.services.classroom.model.CourseWork;

import ca.tfuller.mctqr.googleclassroom.ClassroomData;

class GoogleClassroom {
	protected static final Logger logger = LogManager.getLogger(GoogleClassroom.class);

	/*
	@Test
	void getCourses() {
		ClassroomData cd = new ClassroomData();
		try {
			ClassroomData.getCourses();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/

	/*
	@Test
	void listStudents() throws IOException {
		ClassroomData cd = new ClassroomData();
		List<Course> courseList = null;
		try {
			courseList = ClassroomData.getCourses();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		List<Student> studentList = ClassroomData.getStudents("50307099886");
		for (Student s : studentList) {
			logger.info(s.toPrettyString());
		}
	}
	*/

	@Test
	void listAssignments() throws IOException {
		ClassroomData cd = new ClassroomData();
		List<Course> courseList = null;
		try {
			courseList = ClassroomData.getCourses();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int idx = 0;
		logger.info("getting assignments for course " + courseList.get(idx).getName());
		String courseId = courseList.get(idx).getId();
		List<CourseWork> cwl = ClassroomData.getCourseWork(courseId);

		for (CourseWork cw : cwl) {
			logger.info(cw.toPrettyString());
		}
	}
}
