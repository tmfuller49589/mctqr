package ca.tfuller.mctqr.jfx.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import ca.tfuller.mctqr.geometry.AnchorFinder;
import ca.tfuller.mctqr.geometry.PathUtils;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Path;
import javafx.stage.Stage;

public class DisplayContoursTest extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		MainController.loadPreferences();

		StackPane root = new StackPane();
		primaryStage.setScene(new Scene(root, 300, 250));
		primaryStage.show();

		//File folder = new File("/home/tfuller/git/mctqr/src/main/resources/");
		File folder = new File("/home/tfuller/git/mctqr/");
		// String sourceFile = folder.getAbsolutePath() + "/s61.jpg";
		//String sourceFile = folder.getAbsolutePath() + "/scan1.jpg";
		String sourceFile = folder.getAbsolutePath() + "/registeredR.png";
		FileInputStream fis;
		try {
			fis = new FileInputStream(sourceFile);
			Image image = new Image(fis);

			DisplayContours dp = new DisplayContours(image);
			dp.computeContours();
			// find the anchors from the contours
			List<Path> contourLoops = PathUtils.splitPaths(dp.getContourLevelPaths()[0]);
			AnchorFinder anchorFinder = new AnchorFinder(contourLoops, image);
			anchorFinder.findAnchors();
			dp.setAnchorFinder(anchorFinder);
			dp.setContourLoops(contourLoops);

			dp.refresh();
			fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
