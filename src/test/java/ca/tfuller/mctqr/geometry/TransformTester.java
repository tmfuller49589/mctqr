package ca.tfuller.mctqr.geometry;

import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.QRDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import ca.tfuller.mctqr.imageregistration.MatrixOps;

public class TransformTester {
	protected static final Logger logger = LogManager.getLogger(TransformTester.class);

	@Test
	public void transformTest() {
		double[][] x = { { 1, 1 }, { 2, 3 }, { 3, 2 } };
		double[][] xprimeT = { { 0, 2 }, { 1, 2 }, { -2, -1 } };

		RealMatrix origPoints = MatrixUtils.createRealMatrix(x);
		int numPoints = origPoints.getRowDimension();
		logger.debug("number of points is " + numPoints);
		RealMatrix xprimeTT = MatrixUtils.createRealMatrix(xprimeT);
		RealMatrix X = MatrixOps.buildX(origPoints);
		MatrixOps.dump(X);

		RealMatrix xprime = MatrixOps.toColumnMatrix(xprimeTT);

		DecompositionSolver solver = new QRDecomposition(X).getSolver();
		RealMatrix affine = solver.solve(xprime);

		logger.debug("dumping affine");
		MatrixOps.dump(affine);

		RealMatrix affine2 = MatrixOps.reshape(affine, 2, 3);
		RealMatrix tmp = MatrixUtils.createRealMatrix(1, 3);
		tmp.setEntry(0, 0, 0);
		tmp.setEntry(0, 1, 0);
		tmp.setEntry(0, 2, 1);
		affine2 = MatrixOps.vstack(affine2, tmp);

		logger.debug("dumping reshaped affine");
		MatrixOps.dump(affine2);

		// compute transformed points
		RealMatrix ones = MatrixOps.createOnesMatrix(1, numPoints);
		logger.debug("dumping origPointsOverOne");
		RealMatrix origPointsOverOne = MatrixOps.vstack(origPoints.transpose(), ones);
		MatrixOps.dump(origPointsOverOne);

		RealMatrix xComputed = affine2.multiply(origPointsOverOne);

		MatrixOps.dump(xComputed);

	}
}
