package ca.tfuller.mctqr.geometry;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Affine;
import javafx.scene.transform.MatrixType;
import javafx.stage.Stage;

public class ReadTransformWriteImageTest extends Application {
	Pane root;
	Stage stage;
	Canvas canvas;

	//@Test
	public void test() throws IOException {
		File folder = new File("/home/tfuller/git/mctqr/src/main/resources/");
		String sourceFile = folder.getAbsolutePath() + "/scan1r.jpg";
		//File folder = new File("/home/tfuller/git/mctqr/mcTests/f4a4e453-009d-49a9-a30a-bdc781c0dbe6/");
		//String sourceFile = folder.getAbsolutePath() + "/answerKey.png";
		FileInputStream fis;
		fis = new FileInputStream(sourceFile);
		Image image = new Image(fis);
		//	Affine affine = createAffine();

		//	System.out.println("javafx affine transform");
		//System.out.println(affine);

		fis.close();

		BufferedImage bi = SwingFXUtils.fromFXImage(image, null);
		AffineTransform affTrans = createAwtTransform();
		System.out.println(affTrans);
		AffineTransformOp op = new AffineTransformOp(affTrans, AffineTransformOp.TYPE_BICUBIC);
		BufferedImage transformedImage = op.filter(bi, null);

		//write the image
		File file = new File("readTransWriteTest.png");

		boolean result = ImageIO.write(transformedImage, "png", file);
		System.out.println("wrote to file " + file.getAbsolutePath());
		System.out.println("result is " + result);

	}

	public static AffineTransform createAwtTransform() {
		// this one correctly rotates and scales scan1r but wrong offset
		double m00 = 0.7209370172727233;
		double m01 = -0.00938380576090639;
		double m02 = -6.139383990765426;
		double m10 = 0.01410160884913544;
		double m11 = 0.720864840883902;
		double m12 = -26.83594291467627;

		double[] flatAffine = new double[] { m00, m10, m01, m11, m02, m12 };
		double[] flatAffine1 = new double[] { 1, 0, 0, 0, 1, 0 };

		AffineTransform at = new AffineTransform(flatAffine);
		return at;
	}

	public static Affine createAffine() {

		// this one correctly rotates and scales scan1r but wrong offset
		double[] flatAffine2 = new double[] { 0.7209370172727233, -0.00938380576090639, -6.139383990765426,
				0.01410160884913544, 0.720864840883902, -26.83594291467627, 0.0, 0.0, 1.0 };

		double[] flatAffine1 = new double[] { 0.7209370172727233, -0.00938380576090639, 0, 0.01410160884913544,
				0.720864840883902, 0, 0.0, 0.0, 1.0 };
		double[] flatAffine = new double[] { 1, 0, 0, 0, 1, 0, 0.0, 0.0, 1.0 };
		Affine aff = new Affine(flatAffine1, MatrixType.MT_2D_3x3, 0);
		return aff;
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.stage = primaryStage;
		root = new Pane();
		primaryStage.setScene(new Scene(root, 300, 250));
		primaryStage.show();
		test();
	}
}
