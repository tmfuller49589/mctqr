package ca.tfuller.mctqr.geometry;

import ca.tfuller.mctqr.geometry.Circle;
import ca.tfuller.mctqr.geometry.CircleFit;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public class CircleFitTest {

	public static void main(String[] args) {
		CircleFitTest cft = new CircleFitTest();
		cft.pxCountTest();
	}

	public void pxCountTest() {
		Circle c = createCircle();

		c.dumpPath();

		int pxCount = c.getPixelCount();
		System.out.println("Pixel count is " + pxCount);
	}

	public void test2() {
		Path path = new Path();
		MoveTo pe = new MoveTo(0, 0);
		path.getElements().add(pe);

		for (int i = 1; i < 7; ++i) {
			double x = i / 2d;
			double y = x * x;
			System.out.println(x + " " + y + ";");
			LineTo lt = new LineTo(x, y);
			path.getElements().add(lt);
		}

		ClosePath cp = new ClosePath();
		path.getElements().add(cp);

		Circle c = new Circle(path);

		CircleFit cf = new CircleFit(c);
		cf.fit();
		cf.dump();
	}

	private Circle createCircle() {
		Path path = new Path();
		double x0 = 0;
		double y0 = 0;

		int N = 30;
		double radius = 10d;

		boolean first = true;
		for (double ang = 0; ang < 2 * Math.PI; ang += 2 * Math.PI / N) {
			double x = radius * Math.cos(ang) + x0;
			double y = radius * Math.sin(ang) + y0;
			System.out.println(x + " " + y + " " + ang + ";");

			if (first) {
				MoveTo pe = new MoveTo(x, y);
				path.getElements().add(pe);
				first = false;
			} else {
				LineTo lt = new LineTo(x, y);
				path.getElements().add(lt);
			}
		}
		ClosePath cp = new ClosePath();
		path.getElements().add(cp);

		Circle c = new Circle(path);

		return c;
	}

	public void test() {
		Circle c = createCircle();
		CircleFit cf = new CircleFit(c);
		cf.fit();
		cf.dump();
	}

}
