package ca.tfuller.mctqr.geometry;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import ca.tfuller.mctqr.geometry.Square;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public class SquareTest {

	Random rand = new Random();
	protected static final Logger logger = LogManager.getLogger(SquareTest.class);

	//@Test
	public void oneSquareTest() {

		SquareTest squareTest = new SquareTest();

		double xTopLeft = rand.nextDouble() * 300d;
		double yTopLeft = rand.nextDouble() * 500d;
		int pointsPerSide = 40;
		double sideLength = rand.nextDouble() * 20 + 20d;
		Double randFact = 0.2d;

		Path p = squareTest.createSquarePath(xTopLeft, yTopLeft, pointsPerSide, sideLength, randFact);
		Square square = new Square(xTopLeft + sideLength / 2, yTopLeft + sideLength / 2, p);
		square.isSquare();

	}

	@Test
	public void extremaTest() {

		SquareTest squareTest = new SquareTest();

		Stats statsAngleBetweenDiagonals = new Stats();
		Stats statsBisectorDiagFactor = new Stats();
		Stats statsDiagonalDiffNormalized = new Stats();
		Stats statsRmsResidual = new Stats();
		Stats statsSidesPlumbFactor = new Stats();

		int N = 1000;

		for (int i = 0; i < N; ++i) {
			double xTopLeft = rand.nextDouble() * 300d;
			double yTopLeft = rand.nextDouble() * 500d;
			int pointsPerSide = 40;
			double sideLength = rand.nextDouble() * 20 + 20d;
			Double randFact = 0.2d;

			Path p = squareTest.createSquarePath(xTopLeft, yTopLeft, pointsPerSide, sideLength, randFact);
			Square square = new Square(xTopLeft + sideLength / 2, yTopLeft + sideLength / 2, p);
			square.isSquare();

			statsAngleBetweenDiagonals.add(square.getAngleBetweenDiagonals());
			statsBisectorDiagFactor.add(square.getBisectorDiagFactor());
			statsDiagonalDiffNormalized.add(square.getDiagonalDiffNormalized());
			statsRmsResidual.add(square.getRmsResidual());
			statsSidesPlumbFactor.add(square.getSidesPlumbFactor());

		}

		statsAngleBetweenDiagonals.computeStats();
		statsBisectorDiagFactor.computeStats();
		statsDiagonalDiffNormalized.computeStats();
		statsRmsResidual.computeStats();
		statsSidesPlumbFactor.computeStats();

		logger.debug("angle between diags:");
		statsAngleBetweenDiagonals.dump();
		System.out.println("bisector diagonal factor:");
		statsBisectorDiagFactor.dump();
		System.out.println("diagonal difference normalized:");
		statsDiagonalDiffNormalized.dump();
		System.out.println("rmsResidual:");
		statsRmsResidual.dump();
		System.out.println("sidesPlumbFactor:");
		statsSidesPlumbFactor.dump();

	}

	private Path createSquarePath(Double x, Double y, int nPointsPerSide, Double sideLength, Double randFact) {
		Path path = new Path();

		MoveTo mt = new MoveTo(x, y);
		path.getElements().add(mt);

		// top side
		for (int i = 1; i < nPointsPerSide; ++i) {
			Double dx = (double) i / (double) (nPointsPerSide - 1) * sideLength;
			Double rx = (rand.nextDouble() - 0.5) * sideLength * randFact;
			Double ry = (rand.nextDouble() - 0.5) * sideLength * randFact;
			LineTo lt = new LineTo(x + dx + rx, y + ry);
			path.getElements().add(lt);
		}

		// right side
		for (int i = 1; i < nPointsPerSide; ++i) {
			Double dy = (double) i / (double) (nPointsPerSide - 1) * sideLength;
			Double rx = (rand.nextDouble() - 0.5) * sideLength * randFact;
			Double ry = (rand.nextDouble() - 0.5) * sideLength * randFact;
			LineTo lt = new LineTo(x + sideLength + rx, y + dy + ry);
			path.getElements().add(lt);
		}

		// bottom side
		for (int i = 1; i < nPointsPerSide; ++i) {
			Double dx = (double) i / (double) (nPointsPerSide - 1) * sideLength;
			Double rx = (rand.nextDouble() - 0.5) * sideLength * randFact;
			Double ry = (rand.nextDouble() - 0.5) * sideLength * randFact;
			LineTo lt = new LineTo(x + sideLength - dx + rx, y + sideLength + ry);
			path.getElements().add(lt);
		}

		// left side
		for (int i = 1; i < nPointsPerSide - 1; ++i) {
			Double dy = (double) i / (double) (nPointsPerSide - 1) * sideLength;
			Double rx = (rand.nextDouble() - 0.5) * sideLength * randFact;
			Double ry = (rand.nextDouble() - 0.5) * sideLength * randFact;
			LineTo lt = new LineTo(x + rx, y + sideLength - dy + ry);
			path.getElements().add(lt);
		}

		ClosePath cp = new ClosePath();
		path.getElements().add(cp);
		return path;
	}

	private class Stats {
		List<Double> xVals = new ArrayList<Double>();
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		double mean;
		double standardDeviation;
		double sum = 0.0;

		public void add(Double x) {
			xVals.add(x);
		}

		public void computeStats() {
			computeExtrema();
			computeMean();
			computeStandardDeviation();
		}

		public void computeExtrema() {
			for (Double x : xVals) {
				min = Math.min(x, min);
				max = Math.max(x, max);
			}
		}

		public void dump() {
			logger.debug("N: " + xVals.size());
			logger.debug("mean: " + mean);
			logger.debug("min: " + min);
			logger.debug("max: " + max);
			logger.debug("standard deviation: " + standardDeviation);

		}

		public void computeStandardDeviation() {
			double sumXsquared = 0d;
			for (Double x : xVals) {
				sumXsquared += x * x;
			}
			standardDeviation = Math.sqrt(sumXsquared / xVals.size() - mean * mean);
		}

		public void computeMean() {

			for (Double x : xVals) {
				sum += x;
			}
			mean = sum / xVals.size();
		}
	}
}
