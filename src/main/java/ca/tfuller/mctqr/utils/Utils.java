package ca.tfuller.mctqr.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Utils {
	private static DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE;

	public static String padLongNumber(Long number) {
		int length = ((Long) Long.MAX_VALUE).toString().length();

		String sn = number.toString();
		while (sn.length() < length) {
			sn = "0" + sn;
		}
		return sn;
	}

	public static String padNumber(Long number, int length) {
		String sn = number.toString();
		while (sn.length() < length) {
			sn = "0" + sn;
		}
		return sn;
	}

	public static String getDateString(LocalDate date) {
		String dateStr = dtf.format(date);
		return dateStr;
	}

	public static LocalDate parseDate(String text) {

		LocalDate date = LocalDate.parse(text, dtf);
		return date;
	}
}
