package ca.tfuller.mctqr.utils;

public class IdNameObject<T> {

	private String id;
	private String name;
	private T object;
	private int MAXSTRINGLENGTH = 8000;

	/**
	 * 
	 * @param id
	 * @param name
	 * @param object
	 */
	public IdNameObject(String id, String name, T object) {
		this.id = id;
		this.name = name;
		this.object = object;
	}

	/**
	 * 
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public String toString() {
		String str = name;
		return str;
	}

	@Override
	public boolean equals(Object other) {
		boolean result = false;
		if (other instanceof IdNameObject) {
			@SuppressWarnings("unchecked")
			IdNameObject<T> otherIdName = (IdNameObject<T>) other;

			if (otherIdName.getName().compareTo(name) == 0 && otherIdName.getId().compareTo(id) == 0) {
				result = true;
			}
		}
		return result;
	}

	@Override
	public int hashCode() {
		int hc = 41 * (41 + getId().hashCode()) + getName().hashCode();
		return hc;
	}

	//
	public int getMAXSTRINGLENGTH() {
		return MAXSTRINGLENGTH;
	}

	public void setMAXSTRINGLENGTH(int mAXSTRINGLENGTH) {
		MAXSTRINGLENGTH = mAXSTRINGLENGTH;
	}

}