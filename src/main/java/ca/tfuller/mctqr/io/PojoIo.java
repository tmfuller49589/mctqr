package ca.tfuller.mctqr.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Scanner;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.jfx.view.MessageWindow;
import ca.tfuller.mctqr.utils.IdNameObject;
import ca.tfuller.mctqr.utils.Utils;
import javafx.beans.property.SimpleObjectProperty;

public class PojoIo<T> {
	protected static final Logger logger = LogManager.getLogger(PojoIo.class);

	private final T pojo;
	private final String fileName;
	private BufferedWriter pojoOutputStream;
	private Scanner pojoScanner;

	public PojoIo(String filename, T pojo) {
		fileName = filename;
		this.pojo = pojo;
	}

	public void load() {
		try {
			File file = new File(fileName);
			pojoScanner = new Scanner(file);
			while (pojoScanner.hasNextLine()) {
				String line = pojoScanner.nextLine();
				logger.debug("line is -->" + line + "<--");
				int commaIndex = line.indexOf(",");
				String fieldName = line.substring(0, commaIndex);
				String value = line.substring(commaIndex + 1).trim();
				// logger.debug("field name is ==>" + fieldName + "<==");
				// logger.debug("value is ==>" + value + "<==");

				Field field = pojo.getClass().getDeclaredField(fieldName);
				Method setter = getSetterMethod(fieldName);

				// logger.debug("field type is " + field.getType());
				if (field.getType().equals(javafx.beans.property.SimpleStringProperty.class)) {
					setter.invoke(pojo, value);
				} else if (field.getType().equals(javafx.beans.property.SimpleIntegerProperty.class)) {
					Integer val = Integer.parseInt(value);
					setter.invoke(pojo, val);
				} else if (field.getType().equals(javafx.beans.property.SimpleDoubleProperty.class)) {
					Double val = Double.parseDouble(value);
					setter.invoke(pojo, val);
				} else if (field.getType().equals(javafx.beans.property.SimpleObjectProperty.class)) {
					Type genericFieldType = field.getGenericType();
					if (genericFieldType instanceof ParameterizedType) {
						ParameterizedType aType = (ParameterizedType) genericFieldType;
						Type[] fieldArgTypes = aType.getActualTypeArguments();
						for (Type fieldArgType : fieldArgTypes) {
							// logger.debug("field -->" + field.getName() + "<--
							// fieldArgType -->" + fieldArgType + "<--"
							// + fieldArgType.getTypeName());

							if (fieldArgType.equals(java.time.LocalDate.class)) {
								setter.invoke(pojo, Utils.parseDate(value));
							} else if (fieldArgType.equals(UUID.class)) {
								setter.invoke(pojo, UUID.fromString(value));
								// TODO has to be a better way than string
								// compare
							} else if (fieldArgType.getTypeName().contains("IdNameObject")) {
								IdNameObject idno = new IdNameObject(value, value, null);
								setter.invoke(pojo, idno);
							} else {
								logger.error("cannot handle setting field type " + fieldArgType + " on field "
										+ field.getName());
							}
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		} catch (SecurityException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		}
	}

	public void save() {
		try {
			pojoOutputStream = new BufferedWriter(new FileWriter(fileName));

			for (Field field : pojo.getClass().getDeclaredFields()) {
				Method getter = getGetterMethod(field);
				logger.debug("field is " + field);
				logger.debug("getter is " + getter);
				Object object = null;
				try {
					object = getter.invoke(pojo);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					MessageWindow.showError(e.getMessage());
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					MessageWindow.showError(e.getMessage());
				} catch (InvocationTargetException e) {
					e.printStackTrace();
					MessageWindow.showError(e.getMessage());
				}

				if (object != null) {
					logger.debug("class -->" + object.getClass() + "<-- field is -->" + field
							+ "<-- object instanceof SimpleObjectProperty is "
							+ (field.getType().equals(SimpleObjectProperty.class)));
					if (field.getType().equals(SimpleObjectProperty.class)) {
						logger.debug("object class is " + object.getClass());
						if (object instanceof IdNameObject) {
							logger.debug("found idNameObject");
							IdNameObject obj = (IdNameObject) object;
							pojoOutputStream.write(field.getName() + ", " + obj.getId() + System.lineSeparator());
						} else {
							logger.debug("not and idNameObject");
							pojoOutputStream.write(field.getName() + ", " + object.toString() + System.lineSeparator());
						}
					} else {
						pojoOutputStream.write(field.getName() + ", " + object.toString() + System.lineSeparator());
					}
					logger.debug("wrote -->" + field.getName() + ", " + object.toString() + "<--");
				} else {
					pojoOutputStream.write(field.getName() + ", null");
					logger.debug("wrote -->" + field.getName() + ", null<--");
				}
			}
			pojoOutputStream.close();
			logger.debug("wrote to file -->" + fileName + "<--");
		} catch (IOException e1) {
			MessageWindow.showError(e1.getMessage());
			e1.printStackTrace();
		}

	}

	public void setPreferenceValue(String fieldName, Object value) {
		Method setter = getSetterMethod(fieldName);
		try {
			setter.invoke(pojo, value);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		}
	}

	private Method getSetterMethod(String fieldName) {
		Field field = null;
		try {
			field = pojo.getClass().getDeclaredField(fieldName);
		} catch (NoSuchFieldException e) {
			MessageWindow.showError(e.getMessage());
		} catch (SecurityException e) {
			MessageWindow.showError(e.getMessage());
		}
		return getSetterMethod(field);
	}

	private Method getGetterMethod(String fieldName) {
		Field field = null;
		try {
			field = pojo.getClass().getDeclaredField(fieldName);
		} catch (NoSuchFieldException e) {
			MessageWindow.showError(e.getMessage());
		} catch (SecurityException e) {
			MessageWindow.showError(e.getMessage());
		}
		return getGetterMethod(field);
	}

	private Method getGetterMethod(Field field) {
		return getMethodByPrefix(field, "get");
	}

	private Method getSetterMethod(Field field) {
		return getMethodByPrefix(field, "set");
	}

	private Method getMethodByPrefix(Field field, String prefix) {
		for (Method method : pojo.getClass().getDeclaredMethods()) {
			if (method.getName().startsWith(prefix)) {
				String fieldName = method.getName().replaceFirst(prefix, "");
				String firstLetter = fieldName.substring(0, 1);
				firstLetter = firstLetter.toLowerCase();
				fieldName = firstLetter + fieldName.substring(1);
				if (field.getName().compareTo(fieldName) == 0) {
					return method;
				}
			}
		}
		return null;
	}
}
