package ca.tfuller.mctqr.io;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.services.classroom.model.Course;
import com.google.api.services.classroom.model.Student;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.NotFoundException;

import ca.tfuller.mctqr.geometry.Circle;
import ca.tfuller.mctqr.googleclassroom.ClassroomData;
import ca.tfuller.mctqr.jfx.view.MainController;
import ca.tfuller.mctqr.jfx.view.MessageWindow;
import ca.tfuller.mctqr.mctest.Bubble;
import ca.tfuller.mctqr.mctest.ChoiceEnum;
import ca.tfuller.mctqr.mctest.McTest;
import ca.tfuller.mctqr.mctest.McTestMaster;
import ca.tfuller.mctqr.mctest.McTestMetaData;
import ca.tfuller.mctqr.mctest.McTestStudent;
import ca.tfuller.mctqr.mctest.QRCodeData;
import ca.tfuller.mctqr.mctest.QRDecode;
import ca.tfuller.mctqr.mctest.QuestionMaster;
import ca.tfuller.mctqr.mctest.QuestionStudent;
import ca.tfuller.mctqr.utils.IdNameObject;
import ca.tfuller.mctqr.utils.Utils;
import javafx.geometry.Point2D;

public class McTestIo {
	protected static final Logger logger = LogManager.getLogger(McTestIo.class);
	public static final Path mcTestsPath = Paths.get("mcTests");
	public static final Path tmpDir = Paths.get("tmp");
	private final McTest mcTest;
	private FileWriter mcTestDataFileWriter;

	private static final String charset = "UTF-8"; // or "ISO-8859-1"
	private static final Map<DecodeHintType, Object> hintMap = new EnumMap<DecodeHintType, Object>(
			DecodeHintType.class);
	private static final List<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
	static {
		hintMap.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
		hintMap.put(DecodeHintType.POSSIBLE_FORMATS, formats);
	}

	public McTestIo(McTest mcTest) {
		this.mcTest = mcTest;
	}

	/*
	 * Create directory specified by path if it does not exist.
	 */
	public static void createDir(Path path) {
		if (!Files.exists(path)) {
			try {
				logger.debug("creating " + path.toAbsolutePath());
				Files.createDirectory(path);
			} catch (IOException e) {
				e.printStackTrace();
				MessageWindow.showError(e.getMessage());
			}
		}
	}

	public static void loadScans(List<File> files) {
		// If file is a tiff, split the tiff into individual images
		List<File> tiffSplits = new ArrayList<File>();
		Iterator<File> fileIt = files.iterator();
		while (fileIt.hasNext()) {
			File file = fileIt.next();
			if (file.toPath().toString().endsWith("tif") || file.toPath().toString().endsWith("tiff")
					|| file.toPath().toString().endsWith("TIF") || file.toPath().toString().endsWith("TIFF")) {
				try {
					tiffSplits.addAll(TiffSplit.split(file.getAbsolutePath()));
				} catch (Exception e) {
					e.printStackTrace();
					MessageWindow.showError(e.getMessage());
				}
				// remove tiff file from list of files
				// since tiff file has been split and individual
				// images saved as png
				fileIt.remove();
			}
		}

		// add all the tiffSplits to the list of files
		files.addAll(tiffSplits);

		for (File file : files) {
			try {
				logger.debug("File " + file.getAbsolutePath());

				// String result = qrd.readQRCode("/var/www/caman/mcTest.jpg",
				// charset, hintMap);
				String result = QRDecode.readQRCode(file.getAbsolutePath(), charset, hintMap);
				QRCodeData qrCodeData = new QRCodeData(result);
				logger.info(result);

				// move the file into the appropriate directory:
				// uuid/scans
				Path source = file.toPath();
				Path target = Paths.get(mcTestsPath.toString(), qrCodeData.getUuid().toString(), "scans",
						source.getFileName().toString());
				Path scansDir = Paths.get(mcTestsPath.toString(), qrCodeData.getUuid().toString(), "scans");
				createDir(scansDir);
				Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				logger.debug("not found exception");
			}
		}
	}

	/*
	 * Create mcTest directory mcTests/uuid
	 */
	public void createMcTestDirectory() {
		logger.info("mcTest is " + mcTest);
		Path mcTestDir = Paths.get(mcTestsPath.toString(), mcTest.getMcTestMetaData().getTestUuid().toString());
		createDir(mcTestDir);
	}

	/*
	 * Create mcTests directory and tmp directory.
	 */
	public static void setup() {
		createDir(mcTestsPath);
		createDir(tmpDir);
	}

	/*
	 * Open a FileWriter for test data file.
	 */
	public void openDataFile() throws IOException {
		setup();
		createMcTestDirectory();
		Path path = getMcTestGeometryDataPath();

		mcTestDataFileWriter = new FileWriter(path.toFile());
	}

	/*
	 * compose path for test data file uuid/uuid.dat
	 * create directory mcTests/uuid if needed
	 */
	public Path getMcTestGeometryDataPath() {
		setup();
		createMcTestDirectory();

		Path path = Paths.get(mcTestsPath.toString(), mcTest.getMcTestMetaData().getTestUuid().toString(),
				mcTest.getMcTestMetaData().getTestUuid().toString() + ".dat");

		return path;
	}

	/*
	 * compose path for test answer key file uuid/uuid.key
	 * create directory mcTests/uuid if needed
	 */
	public Path getMcTestAnswerKeyPath() {
		setup();
		createMcTestDirectory();

		Path path = Paths.get(mcTestsPath.toString(), mcTest.getMcTestMetaData().getTestUuid().toString(),
				mcTest.getMcTestMetaData().getTestUuid().toString() + ".key");

		return path;
	}

	/*
	 * compose path for test answer key file uuid/uuid.txt
	 * create directory mcTests/uuid if needed
	 * 
	 */
	public Path getMcTestMetaDataPath() {
		setup();
		createMcTestDirectory();

		Path path = Paths.get(mcTestsPath.toString(), mcTest.getMcTestMetaData().getTestUuid().toString(),
				mcTest.getMcTestMetaData().getTestUuid().toString() + ".txt");

		return path;
	}

	/*
	 * compose path for test answer key image uuid/answerKey.jpg
	 * create directory mcTests/uuid if needed
	 */
	public Path getMcTestAnswerKeyImagePath() {
		setup();
		createMcTestDirectory();

		// create path for test image answer key
		Path path = Paths.get(mcTestsPath.toString(), mcTest.getMcTestMetaData().getTestUuid().toString(),
				"answerKey.png");

		return path;
	}

	public void writeMcTestInfoFile() {
		PojoIo<McTestMetaData> pjio = new PojoIo<McTestMetaData>(getMcTestMetaDataPath().toAbsolutePath().toString(),
				mcTest.getMcTestMetaData());
		pjio.save();
	}

	public void writeMcTestDataFile(String str) throws IOException {
		mcTestDataFileWriter.write(str);
	}

	public void writeMcTestAnswerKeyImage(RenderedImage image) throws IOException {
		Path path = getMcTestAnswerKeyImagePath();
		ImageIO.write(image, "png", path.toFile());
	}

	public void writeMcTestAnswerKeyFile() throws IOException {
		Path path = getMcTestAnswerKeyPath();
		FileWriter fileWriter = new FileWriter(path.toFile());

		for (Entry<Integer, QuestionMaster> entry : mcTest.getMcTestMaster().getQuestions().entrySet()) {
			QuestionMaster q = entry.getValue();
			fileWriter.write(q.getQuestionNumber() + " " + q.getCorrectAnswer() + System.lineSeparator());
		}
		fileWriter.close();
	}

	public void setCourse() throws IOException {
		List<Course> courseList = ClassroomData.getCourses();
		Course course = null;
		for (Course c : courseList) {
			if (c.getId().compareTo(mcTest.getMcTestMetaData().getCourseId()) == 0) {
				course = c;
				break;
			}
		}
		if (course != null) {
			mcTest.getMcTestMetaData().setCourseId(course.getId());
			mcTest.getMcTestMetaData().setCourseName(course.getName());
		}
	}

	public void readMcTestMaster() throws IOException {
		readMcTestMasterInfoFile();
		readMcTestGeometryFile();
		readMcTestAnswerKeyFile();
		// TODO auto detect connection
		setCourse();
	}

	public void readMcTestInfoFile(Path f) {
		PojoIo<McTestMaster> pjio = new PojoIo<McTestMaster>(f.toAbsolutePath().toString(), mcTest.getMcTestMaster());
		pjio.load();
	}

	public void readMcTestAnswerKeyFile() throws IOException {
		Path path = getMcTestAnswerKeyPath();
		if (!Files.exists(path)) {
			return;
		}

		Scanner scanner = new Scanner(path);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] tokens = line.split(" ");
			logger.debug("line is " + line);
			Integer questionNumber = Integer.parseInt(tokens[0]);
			String correctAnswer = tokens[1];
			logger.debug("question " + questionNumber + " correct answer is " + correctAnswer);
			QuestionMaster q = mcTest.getMcTestMaster().getQuestions().get(questionNumber);
			q.setCorrectAnswer(ChoiceEnum.getChoice(correctAnswer));
		}
		scanner.close();
	}

	public static McTestStudent readMcTestStudent(McTest mcTest, File f) {
		logger.debug("reading " + f.getAbsolutePath());
		McTestStudent mcTestStudent = new McTestStudent();

		try {
			String result = QRDecode.readQRCode(f.getAbsolutePath(), charset, hintMap);
			QRCodeData qrCodeData = new QRCodeData(result);
			Student student = ClassroomData.getStudent(qrCodeData.getStudentId(), qrCodeData.getCourseId());
			IdNameObject<Student> idno = new IdNameObject<Student>(student.getUserId(),
					student.getProfile().getName().getFullName(), student);
			mcTestStudent.setStudent(idno);
			logger.debug(result);
			logger.debug("student is " + student.getProfile().getName().getFullName());
			for (Entry<Integer, QuestionMaster> entry : mcTest.getMcTestMaster().getQuestions().entrySet()) {
				QuestionStudent q = new QuestionStudent(entry.getValue());
				mcTestStudent.getQuestions().put(entry.getKey(), q);
			}

		} catch (NotFoundException | IOException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getLocalizedMessage());
		}
		return mcTestStudent;
	}

	public void readMcTestMasterInfoFile() {
		PojoIo<McTestMetaData> pjio = new PojoIo<McTestMetaData>(getMcTestMetaDataPath().toAbsolutePath().toString(),
				mcTest.getMcTestMetaData());
		pjio.load();
	}

	public void readMcTestGeometryFile() throws IOException {
		readMcTestGeometryFile(getMcTestGeometryDataPath());
	}

	public void readMcTestGeometryFile(Path dataPath) throws IOException {
		logger.info("reading " + dataPath.toString());
		mcTest.getMcTestMaster().getQuestions().clear();
		mcTest.getMcTestMaster().getAnchors().clear();

		if (!Files.exists(dataPath)) {
			logger.info(dataPath + " does not exist");
			MessageWindow.showError(dataPath + " does not exist");
			return;
		}
		Scanner scanner = new Scanner(dataPath);
		for (int i = 0; i < mcTest.getMcTestMetaData().getNumberOfQuestions(); ++i) {
			QuestionMaster question = new QuestionMaster();
			question.setQuestionNumber(i + 1);
			mcTest.getMcTestMaster().getQuestions().put(question.getQuestionNumber(), question);
			String line = scanner.nextLine();
			String[] tokens = line.split(" ");
			for (int j = 0; j < mcTest.getMcTestMetaData().getChoicesPerQuestion(); ++j) {

				Circle circle = new Circle();
				Bubble bubble = new Bubble(circle, ChoiceEnum.getChoice(j));
				parseCircleToken(circle, tokens[j + 1]);
				question.addBubble(bubble);
			}
		}

		scanner.nextLine(); // scan the registration squares line

		for (int i = 0; i < 7; ++i) {
			String line = scanner.nextLine();
			line = line.trim();

			// trim off the leading bracket
			line = line.substring(1);

			// trim off the trailing bracket
			line = line.substring(0, line.length() - 1);

			String[] coords = line.split(",");

			Point2D point = new Point2D(Float.parseFloat(coords[0]), Float.parseFloat(coords[1]));
			mcTest.getMcTestMaster().getAnchors().add(point);
		}
		scanner.close();

		mcTest.getMcTestMaster().dump();
	}

	private void parseCircleToken(Circle circle, String token) {
		token = token.trim();

		// trim off the leading bracket
		token = token.substring(1);

		// trim off the trailing bracket
		token = token.substring(0, token.length() - 1);

		String[] coords = token.split(",");

		Point2D center = new Point2D(Double.parseDouble(coords[0]), Double.parseDouble(coords[1]));
		circle.setCenter(center);
		//TODO need to handle zoom consistently
		// radius is diameter/2 but zoom is 2x in jasper report to jpeg
		circle.setR((double) MainController.getPreferences().getBubbleSize());
	}

	public void dump() {
		logger.debug(mcTest.getMcTestMetaData().getTestUuid() + "|testUuid");
		logger.debug(mcTest.getMcTestMetaData().getTestName() + "|test name");
		logger.debug(mcTest.getMcTestMetaData().getCourseId() + " |course id");
		logger.debug(mcTest.getMcTestMetaData().getCourseName() + " |course name");
		logger.debug(mcTest.getMcTestMetaData().getInstructor() + "|instructor");
		logger.debug(Utils.getDateString(mcTest.getMcTestMetaData().getDate()) + "|date");
		logger.debug(mcTest.getMcTestMetaData().getNumberOfQuestions() + "|number of questions");
		logger.debug(mcTest.getMcTestMetaData().getChoicesPerQuestion() + "|choices per question");
		logger.debug(mcTest.getMcTestMetaData().getColumns() + "|columns");
		logger.debug(mcTest.getMcTestMetaData().getBubbleSize() + "|bubble size");
		logger.debug(mcTest.getMcTestMetaData().getAnchorSize() + "|anchor size");

	}

	public void closeDataFile() throws IOException {
		mcTestDataFileWriter.close();
	}

}
