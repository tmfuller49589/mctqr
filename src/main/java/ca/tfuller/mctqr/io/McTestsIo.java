package ca.tfuller.mctqr.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.mctest.McTest;
import ca.tfuller.mctqr.mctest.McTestStudent;

public class McTestsIo {
	protected static final Logger logger = LogManager.getLogger(McTestsIo.class);

	public void readStudentTests(McTest mcTest) {
		mcTest.getMcTestStudentMap().clear();
		Path path = Paths.get(McTestIo.mcTestsPath.toString(), mcTest.getMcTestMetaData().getTestUuid().toString(),
				"scans");
		logger.info("listing files in " + path.toString());
		File[] files = path.toFile().listFiles(File::isFile);
		for (File f : files) {
			McTestStudent mct = McTestIo.readMcTestStudent(mcTest, f);
			logger.debug("questions is " + mct.getQuestions());
			mct.setScannedFile(f);
			mcTest.getMcTestStudentMap().put(mct.getStudent().getId(), mct);
		}
	}

	public void readTestMasters(List<McTest> mcTests) throws IOException {
		mcTests.clear();
		File[] directories = McTestIo.mcTestsPath.toFile().listFiles(File::isDirectory);
		for (File f : directories) {
			readMcTestMaster(mcTests, f.toPath());
		}
	}

	private void readMcTestMaster(List<McTest> mcTests, UUID uuid) throws IOException {
		McTest mcTest = new McTest();
		McTestIo mcTestIo = new McTestIo(mcTest);
		mcTests.add(mcTest);
		logger.info("metadata is " + mcTest.getMcTestMetaData());
		mcTest.getMcTestMetaData().setTestUuid(uuid);
		mcTestIo.readMcTestMaster();
	}

	private void readMcTestMaster(List<McTest> mcTests, Path directory) throws IOException {
		readMcTestMaster(mcTests, UUID.fromString(directory.getFileName().toString()));
	}
}
