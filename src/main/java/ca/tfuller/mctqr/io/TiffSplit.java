package ca.tfuller.mctqr.io;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TiffSplit {
	protected static final Logger logger = LogManager.getLogger(TiffSplit.class);

	public static void main(String[] args) {
		String filename = "/home/tfuller/git/mctqr/src/main/resources/scan.tif";
		try {
			TiffSplit.split(filename);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<File> split(String filename) throws Exception {

		String fileNameWithOutExt = FilenameUtils.removeExtension(filename);
		File inputFile = new File(filename);

		InputStream is = new FileInputStream(inputFile);
		TiffSplit tt = new TiffSplit();
		List<BufferedImage> imgs = tt.extractImages(is);

		// TODO check for filename collision
		List<File> files = new ArrayList<File>();

		int indx = 0;
		for (BufferedImage bi : imgs) {
			++indx;
			String s = fileNameWithOutExt + indx + ".png";
			File outputFile = new File(s);
			files.add(outputFile);
			ImageIO.write(bi, "png", outputFile);
			logger.debug("writing " + outputFile);
		}
		is.close();
		return files;
	}

	public List<BufferedImage> extractImages(InputStream fileInput) throws Exception {
		List<BufferedImage> extractedImages = new ArrayList<BufferedImage>();

		try (ImageInputStream iis = ImageIO.createImageInputStream(fileInput)) {

			ImageReader reader = getTiffImageReader();
			reader.setInput(iis);

			int pages = reader.getNumImages(true);
			for (int imageIndex = 0; imageIndex < pages; imageIndex++) {
				BufferedImage bufferedImage = reader.read(imageIndex);
				extractedImages.add(bufferedImage);
			}
		}

		return extractedImages;
	}

	private ImageReader getTiffImageReader() {
		Iterator<ImageReader> imageReaders = ImageIO.getImageReadersByFormatName("TIFF");
		if (!imageReaders.hasNext()) {
			throw new UnsupportedOperationException("No TIFF Reader found!");
		}
		return imageReaders.next();
	}

}
