package ca.tfuller.mctqr.sheetgenerator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.logging.log4j.LogManager; import org.apache.logging.log4j.Logger;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

class AnswerSheetDataSourceFactory implements JRDataSource {
	protected static final Logger logger = LogManager.getLogger(AnswerSheetDataSourceFactory.class);

	List<AnswerSheetData> mcQuestionList = null;

	private int index; // have to initialize to -1 so that next() hits
						// zero on first call

	/**
	 * @param studentsStats
	 * @param studentNumberList
	 * 
	 */
	public AnswerSheetDataSourceFactory(List<AnswerSheetData> studentList) {
		this.mcQuestionList = studentList;
		index = -1;
	}

	/**
	 * 
	 * @return
	 */
	public JRDataSource createDatasource() {
		return new JRBeanCollectionDataSource(createBeanCollection());

	}

	/**
	 * 
	 * @return
	 */
	public JRDataSource createBeanCollectionDatasource() {
		return new JRBeanCollectionDataSource(createBeanCollection());
	}

	/**
	 * 
	 * @return
	 */
	public List<AnswerSheetData> createBeanCollection() {

		index = -1;

		mcQuestionList.add(new AnswerSheetData());
		return mcQuestionList;
	}

	/**
	 * 
	 * @return
	 * @throws net.sf.jasperreports.engine.JRException
	 */
	@Override
	public boolean next() throws JRException {
		index++;
		return index < mcQuestionList.size();

	}

	/**
	 * 
	 * @param field
	 * @return
	 * @throws net.sf.jasperreports.engine.JRException
	 */
	@Override
	public Object getFieldValue(JRField field) throws JRException {
		AnswerSheetData mcqd = mcQuestionList.get(index);

		String sName = field.getName();
		Object object = null;
		try {
			String getter = "get" + sName.substring(0, 1).toUpperCase() + sName.substring(1);
			for (Method method : mcqd.getClass().getMethods()) {
				if (method.getName().equals(getter)) {
					object = method.invoke(mcqd, (Object[]) null);
					break;
				}
			}
		} catch (SecurityException ex) {
			logger.error(ex.getMessage(), ex);
		} catch (IllegalAccessException ex) {
			logger.error(ex.getMessage(), ex);
		} catch (InvocationTargetException ex) {
			logger.error(ex.getMessage(), ex);
		}

		return object;
	}
}