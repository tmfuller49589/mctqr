package ca.tfuller.mctqr.sheetgenerator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.services.classroom.model.Student;

import ca.tfuller.mctqr.mctest.QRCode;

public class AnswerSheetData {

	private Boolean answerKey = false;
	private Student student;

	protected static final Logger logger = LogManager.getLogger(AnswerSheetData.class);

	public AnswerSheetData() {
		super();
	}

	private Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public String getStudentName() {
		if (answerKey) {
			return "answer key";
		}
		return student.getProfile().getName().getFullName();
	}

	public String getQrImageFileName() {
		String qrFileName = QRCode.createFileName(getStudentName());
		logger.debug("AnswerSheetData.getQrImageFileName returning -->" + qrFileName + "<--");
		return qrFileName;
	}

	public Boolean getAnswerKey() {
		return answerKey;
	}

	public void setAnswerKey(Boolean answerKey) {
		this.answerKey = answerKey;
	}

	public String getStudentId() {
		logger.debug("answerKey is " + answerKey + " student is " + student);
		if (answerKey) {
			return "answer key";
		}
		return student.getProfile().getId();
	}
}
