package ca.tfuller.mctqr.sheetgenerator;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.io.McTestIo;
import ca.tfuller.mctqr.jfx.view.MessageWindow;
import ca.tfuller.mctqr.mctest.McTest;
import ca.tfuller.mctqr.mctest.QRCode;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignBreak;
import net.sf.jasperreports.engine.design.JRDesignEllipse;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignImage;
import net.sf.jasperreports.engine.design.JRDesignLine;
import net.sf.jasperreports.engine.design.JRDesignParameter;
import net.sf.jasperreports.engine.design.JRDesignRectangle;
import net.sf.jasperreports.engine.design.JRDesignSection;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.BreakTypeEnum;
import net.sf.jasperreports.engine.type.HorizontalTextAlignEnum;
import net.sf.jasperreports.engine.type.LineDirectionEnum;
import net.sf.jasperreports.engine.type.PositionTypeEnum;
import net.sf.jasperreports.engine.type.VerticalTextAlignEnum;
import net.sf.jasperreports.view.JasperViewer;

/**
 * @author tfuller
 * 
 */
public class AnswerSheet {
	protected static final Logger logger = LogManager.getLogger(AnswerSheet.class);
	protected String fileName = "";

	// ratio of pattern is 1:1:6:1:1
	private static final int regRectBlackBorder = 1;
	private static final int regRectWhiteBorder = 1;
	private static final int regRectBlackSquare = 6;
	private static float jasperZoom = 2.0f;

	private static final int regRectScale = 2;
	private static final int regRectSize = (regRectBlackBorder + regRectWhiteBorder + regRectBlackSquare
			+ regRectWhiteBorder + regRectBlackBorder) * regRectScale;

	McTestIo mcTestIo;
	McTest mcTest;

	public AnswerSheet(McTest mcTest) {
		this.mcTest = mcTest;
		mcTestIo = new McTestIo(mcTest);
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void doIt(List<AnswerSheetData> answerSheetDataList) throws IOException {

		mcTestIo.openDataFile();

		JasperDesign jasperDesign = new JasperDesign();

		try {
			// write out the QR codes for each student
			double halfFontSize = 0;
			int fontSize = 0;
			String courseName = "";
			int bubbleSize = 0;
			int numberOfQuestions = 0;
			int columns = 0;
			int choicesPerQuestion = 0;
			String instructor = "";
			String testName = "";
			for (AnswerSheetData answerSheetData : answerSheetDataList) {
				fontSize = mcTest.getMcTestMetaData().getFontSize();
				halfFontSize = mcTest.getMcTestMetaData().getFontSize() * 0.5;
				courseName = mcTest.getMcTestMetaData().getCourseName();
				bubbleSize = mcTest.getMcTestMetaData().getBubbleSize();
				QRCode.writeQrImage(mcTest, answerSheetData);
				numberOfQuestions = mcTest.getMcTestMetaData().getNumberOfQuestions();
				columns = mcTest.getMcTestMetaData().getColumns();
				choicesPerQuestion = mcTest.getMcTestMetaData().getChoicesPerQuestion();
				instructor = mcTest.getMcTestMetaData().getInstructor();
				testName = mcTest.getMcTestMetaData().getTestName();
			}

			// JasperDesign

			jasperDesign.setName("Answer sheet");
			jasperDesign.setPageWidth(612);
			jasperDesign.setPageHeight(792);
			jasperDesign.setLeftMargin(36);
			jasperDesign.setRightMargin(36);
			jasperDesign.setColumnWidth(612 - 36 - 36);
			jasperDesign.setColumnSpacing(0);
			jasperDesign.setTopMargin(50);
			jasperDesign.setBottomMargin(50);

			// Fonts
			JRDesignStyle normalStyle = new JRDesignStyle();
			normalStyle.setName("Sans_Normal");
			normalStyle.setDefault(true);
			normalStyle.setFontName("Arial");
			normalStyle.setFontSize((float) fontSize);
			normalStyle.setPdfFontName("Helvetica");
			normalStyle.setPdfEncoding("Cp1252");
			normalStyle.setPdfEmbedded(false);
			jasperDesign.addStyle(normalStyle);

			JRDesignStyle boldStyle = new JRDesignStyle();
			boldStyle.setName("Sans_Bold");
			boldStyle.setFontName("Arial");
			boldStyle.setFontSize((float) fontSize);
			boldStyle.setBold(true);
			boldStyle.setPdfFontName("Helvetica-Bold");
			boldStyle.setPdfEncoding("Cp1252");
			boldStyle.setPdfEmbedded(false);
			jasperDesign.addStyle(boldStyle);

			// Parameters
			JRDesignParameter parameter = new JRDesignParameter();
			parameter.setName("ReportTitle");
			jasperDesign.addParameter(parameter);

			// Fields
			JRDesignField jrDesignField;

			jrDesignField = new JRDesignField();
			jrDesignField.setName("studentName");
			jrDesignField.setValueClass(java.lang.String.class);
			try {
				jasperDesign.addField(jrDesignField);
			} catch (JRException e) {
				MessageWindow.showError("Problem generating report " + e.getMessage());
				e.printStackTrace();
			}

			jrDesignField = new JRDesignField();
			jrDesignField.setName("qrImageFileName");
			jrDesignField.setValueClass(java.lang.String.class);
			try {
				jasperDesign.addField(jrDesignField);
			} catch (JRException e) {
				MessageWindow.showError("Problem generating report " + e.getMessage());
				e.printStackTrace();
			}

			// detail band
			JRDesignBand band = new JRDesignBand();

			// class name year
			JRDesignTextField textField = new JRDesignTextField();
			textField.setX(0);
			textField.setY(0);
			textField.setWidth(170);
			textField.setHeight(14);
			textField.setHorizontalTextAlign(HorizontalTextAlignEnum.LEFT);
			textField.setStyle(boldStyle);
			JRDesignExpression expression = new JRDesignExpression();
			expression.setText("\"" + courseName + "\"");
			textField.setExpression(expression);
			band.addElement(textField);

			// Instructor
			textField = new JRDesignTextField();
			textField.setX(0);
			textField.setY(14);
			textField.setWidth(200);
			textField.setHeight(20);
			textField.setHorizontalTextAlign(HorizontalTextAlignEnum.LEFT);
			textField.setStyle(boldStyle);
			expression = new JRDesignExpression();
			expression.setText("\"Instructor:" + instructor + "\"");
			textField.setExpression(expression);
			band.addElement(textField);

			// test name
			textField = new JRDesignTextField();
			textField.setX(0);
			textField.setY(28);
			textField.setWidth(200);
			textField.setHeight(20);
			textField.setHorizontalTextAlign(HorizontalTextAlignEnum.LEFT);
			textField.setStyle(boldStyle);
			expression = new JRDesignExpression();
			// expression.setText("\"" + MarkScriptlet.getInstructor() + "\"");
			expression.setText("\"" + testName + "\"");
			textField.setExpression(expression);
			band.addElement(textField);

			Integer questionNumber = 0;
			Integer questionsPerColumn = (int) Math.ceil(numberOfQuestions / (double) columns);

			Integer rowHeight = fontSize + 3;
			band.setHeight(jasperDesign.getPageHeight() - 200);

			textField = new JRDesignTextField();
			textField.setX(160);
			textField.setY(0);
			textField.setWidth(jasperDesign.getColumnWidth() - 10);
			textField.setHeight(band.getHeight());
			textField.setPositionType(PositionTypeEnum.FIX_RELATIVE_TO_TOP);
			textField.setStyle(boldStyle);
			textField.setHorizontalTextAlign(HorizontalTextAlignEnum.LEFT);
			textField.setVerticalTextAlign(VerticalTextAlignEnum.TOP);
			expression = new JRDesignExpression();
			expression.setText("\"Student: \" + $F{studentName}");
			textField.setExpression(expression);
			band.addElement(textField);

			// Student signature
			textField = new JRDesignTextField();
			textField.setX(160);
			textField.setY(20);
			textField.setWidth(200);
			textField.setHeight(20);
			textField.setHorizontalTextAlign(HorizontalTextAlignEnum.LEFT);
			textField.setStyle(boldStyle);
			expression = new JRDesignExpression();
			expression.setText("\"Signature:\"");
			textField.setExpression(expression);
			band.addElement(textField);
			JRDesignLine jrDesignLine = new JRDesignLine();
			jrDesignLine.setDirection(LineDirectionEnum.TOP_DOWN);
			jrDesignLine.setHeight(1);
			jrDesignLine.setWidth(150);
			jrDesignLine.setX(220);
			jrDesignLine.setY(35);
			band.addElement(jrDesignLine);

			// QR code
			JRDesignExpression qrImageExpression = new JRDesignExpression();
			qrImageExpression.setText("$F{qrImageFileName}");

			JRDesignImage qrImage = new JRDesignImage(jasperDesign);
			qrImage.setX(400);
			qrImage.setY(0);
			qrImage.setWidth((int) (QRCode.size * 0.4));
			qrImage.setHeight((int) (QRCode.size * 0.4));
			qrImage.setExpression(qrImageExpression);
			qrImage.setPositionType(PositionTypeEnum.FIX_RELATIVE_TO_TOP);

			band.addElement(qrImage);

			Integer columnWidth = (int) Math
					.floor(((double) jasperDesign.getColumnWidth() - regRectSize * 2f) / columns);

			int yPos = 0;

			int minAnswerCircleY = Integer.MAX_VALUE;
			int maxAnswerCircleY = Integer.MIN_VALUE;

			outloop: for (int column = 0; column < columns; ++column) {
				logger.debug("processing column " + column);

				for (int row = 0; row < questionsPerColumn; ++row) {

					logger.debug("question " + questionNumber);
					String testDescriptionString = (questionNumber + 1) + ": ";
					textField = new JRDesignTextField();
					yPos = row * rowHeight + 130;

					minAnswerCircleY = Math.min(minAnswerCircleY, yPos);
					maxAnswerCircleY = Math.max(maxAnswerCircleY, yPos);

					textField.setX(column * columnWidth + (int) (regRectSize * 1.2));
					textField.setY(yPos);
					logger.debug("y = " + (row * rowHeight));

					textField.setWidth((int) ((Math.ceil(normalStyle.getFontsize())) * 2));
					textField.setHeight(rowHeight);
					textField.setPositionType(PositionTypeEnum.FLOAT);
					textField.setStyle(normalStyle);
					textField.setHorizontalTextAlign(HorizontalTextAlignEnum.LEFT);
					textField.setVerticalTextAlign(VerticalTextAlignEnum.MIDDLE);
					// textField.setBackcolor(Color.RED);
					// textField.setForecolor(Color.green);
					// textField.setMode(ModeEnum.OPAQUE);
					expression = new JRDesignExpression();

					expression.setText("\"" + ((Integer) (questionNumber + 1)).toString() + ".\"");

					textField.setExpression(expression);
					band.addElement(textField);

					for (int choice = 0; choice < choicesPerQuestion; ++choice) {
						// xPos marks the center of the a b c d choices
						int xPos = (int) (column * columnWidth + regRectSize * 1.2 + fontSize * 2
								+ (choice + 1) * fontSize * 1.5 - halfFontSize);

						JRDesignEllipse jre = new JRDesignEllipse(jasperDesign);

						jre.setBackcolor(Color.white);
						jre.setForecolor(Color.black);

						double ellipseWidth = bubbleSize;
						double ellipseHeight = ellipseWidth;
						jre.setX((int) (xPos - ellipseWidth * 0.5 + fontSize * 0.6 * 0.4));
						jre.setY((int) (yPos + rowHeight * 0.5 - ellipseHeight * 0.5));
						jre.setWidth((int) (ellipseWidth));
						jre.setHeight((int) (ellipseHeight));
						int bblx = (int) ((jre.getX() + ellipseWidth * 0.5 + jasperDesign.getLeftMargin())
								* jasperZoom);
						int bbly = (int) ((jre.getY() + ellipseWidth * 0.5 + jasperDesign.getTopMargin()) * jasperZoom);

						testDescriptionString += "(" + bblx + "," + bbly + ") ";
						band.addElement(jre);

						textField = new JRDesignTextField();
						textField.setX(xPos);
						textField.setY(yPos);
						textField.setWidth(fontSize);
						textField.setHeight(rowHeight);
						textField.setPositionType(PositionTypeEnum.FLOAT);
						textField.setStyle(normalStyle);
						textField.setHorizontalTextAlign(HorizontalTextAlignEnum.LEFT);
						textField.setVerticalTextAlign(VerticalTextAlignEnum.MIDDLE);
						textField.setFontSize((float) (fontSize * 0.6));
						textField.setForecolor(Color.LIGHT_GRAY);
						expression = new JRDesignExpression();
						expression.setText("\"" + Character.toString((char) (choice + 97)) + "\"");
						textField.setExpression(expression);

						band.addElement(textField);

					}

					mcTestIo.writeMcTestDataFile(testDescriptionString + System.lineSeparator());
					++questionNumber;
					if (questionNumber >= numberOfQuestions) {
						break outloop;
					}
				}
			}

			mcTestIo.writeMcTestDataFile("registration squares" + System.lineSeparator());

			// registration rectangles, left side
			for (int i = 0; i < 2; ++i) {
				int y = (int) (minAnswerCircleY - regRectSize
						+ i * (maxAnswerCircleY + regRectSize - (minAnswerCircleY - regRectSize)) / 2f);
				addRegistrationRectangle(0, y, band, jasperDesign);

				int cx = (regRectSize / 2) + jasperDesign.getLeftMargin();
				int cy = (y + regRectSize / 2) + jasperDesign.getTopMargin();
				cx = (int) (cx * jasperZoom);
				cy = (int) (cy * jasperZoom);

				mcTestIo.writeMcTestDataFile("(" + cx + "," + cy + ")" + System.lineSeparator());
			}

			// registration rectangles, right side
			for (int i = 1; i < 3; ++i) {
				int y = minAnswerCircleY - regRectSize
						+ (int) (i * (maxAnswerCircleY + regRectSize - (minAnswerCircleY - regRectSize)) / 2f);
				addRegistrationRectangle(jasperDesign.getColumnWidth() - regRectSize, y, band, jasperDesign);

				int cx = jasperDesign.getColumnWidth() - regRectSize + (regRectSize / 2) + jasperDesign.getLeftMargin();
				int cy = (y + regRectSize / 2) + jasperDesign.getTopMargin();
				cx = (int) (cx * jasperZoom);
				cy = (int) (cy * jasperZoom);
				mcTestIo.writeMcTestDataFile("(" + cx + "," + cy + ")" + System.lineSeparator());
			}

			// registration rectangles, bottom
			for (int i = 0; i < 2; ++i) {
				int x = (int) (i * (jasperDesign.getColumnWidth() - regRectSize) / 2f);
				addRegistrationRectangle(x, maxAnswerCircleY + regRectSize, band, jasperDesign);

				int cx = x + regRectSize / 2 + jasperDesign.getLeftMargin();
				int cy = maxAnswerCircleY + regRectSize + regRectSize / 2 + jasperDesign.getTopMargin();
				cx = (int) (cx * jasperZoom);
				cy = (int) (cy * jasperZoom);

				mcTestIo.writeMcTestDataFile("(" + cx + "," + cy + ")" + System.lineSeparator());

			}

			// registration rectangles, top
			for (int i = 1; i < 2; ++i) {
				int x = (int) (i * (jasperDesign.getColumnWidth() - regRectSize) / 2f);
				addRegistrationRectangle(x, (int) (minAnswerCircleY - regRectSize * 1.2), band, jasperDesign);

				int cx = x + regRectSize / 2 + jasperDesign.getLeftMargin();
				int cy = (int) (minAnswerCircleY - regRectSize * 1.2) + regRectSize / 2 + jasperDesign.getTopMargin();
				cx = (int) (cx * jasperZoom);
				cy = (int) (cy * jasperZoom);
				mcTestIo.writeMcTestDataFile("(" + cx + "," + cy + ")" + System.lineSeparator());
			}

			yPos = yPos + rowHeight * 2;
			logger.debug("printable page width = " + jasperDesign.getColumnWidth());
			logger.debug("regRectWidth = " + regRectSize);

			((JRDesignSection) jasperDesign.getDetailSection()).addBand(band);

			jasperDesign.setFloatColumnFooter(false);

			JRDesignBand footerband = new JRDesignBand();
			footerband.setHeight(33);

			JRDesignBreak jrb = new JRDesignBreak();
			jrb.setType(BreakTypeEnum.PAGE);
			footerband.addElement(jrb);

			jasperDesign.setPageFooter(footerband);

			mcTestIo.closeDataFile();
		} catch (JRException e1) {
			MessageWindow.showError("Problem generating report " + e1.getMessage());
			e1.printStackTrace();
		}

		// compile the report
		Map<String, Object> paramsMap = new HashMap<String, Object>();

		paramsMap.put("ReportTitle", "fill in with appropriate report title");

		try {
			AnswerSheetDataSourceFactory asdsf = new AnswerSheetDataSourceFactory(answerSheetDataList);

			JasperReport report = JasperCompileManager.compileReport(jasperDesign);

			JasperPrint print = JasperFillManager.fillReport(report, paramsMap, asdsf);

			Image reportImage = JasperPrintManager.printPageToImage(print, 0, jasperZoom);
			BufferedImage bImage = new BufferedImage(reportImage.getWidth(null), reportImage.getHeight(null),
					BufferedImage.TYPE_INT_RGB);

			// obtain its graphics
			Graphics2D bImageGraphics = bImage.createGraphics();

			// draw the Image (image) into the BufferedImage (bImage)
			bImageGraphics.drawImage(reportImage, null, null);

			// cast it to rendered image
			RenderedImage rImage = bImage;
			try {
				mcTestIo.writeMcTestAnswerKeyImage(rImage);
			} catch (IOException e) {
				e.printStackTrace();
				MessageWindow.showError(e.getMessage());
			}

			if (fileName.isEmpty()) {
				// view the report with the built-in viewer
				JasperViewer.viewReport(print, false);
			} else {
				try {
					JasperExportManager.exportReportToPdfFile(print, fileName);
				} catch (JRException ex) {
					logger.error(ex.getMessage(), ex);
					MessageWindow.showError(ex.getMessage());
				}
			}
		} catch (JRException e) {
			e.printStackTrace();
			MessageWindow.showError("Problem generating report " + e.getMessage());
		}
	}

	private static void addRegistrationRectangle(int x, int y, JRDesignBand band, JasperDesign jasperDesign) {

		// largest black rectangle, forms the outer border
		JRDesignRectangle jrr = new JRDesignRectangle(jasperDesign);
		jrr.setBackcolor(Color.black);
		jrr.setForecolor(Color.black);
		jrr.setX(x);
		jrr.setY(y);
		jrr.setWidth(regRectSize);
		jrr.setHeight(regRectSize);
		jrr.getLinePen().setLineWidth(1);
		band.addElement(jrr);

		// inner white rectangle, forms the white border
		jrr = new JRDesignRectangle(jasperDesign);
		jrr.setBackcolor(Color.white);
		jrr.setForecolor(Color.white);
		jrr.setX(x + 1 * regRectScale);
		jrr.setY(y + 1 * regRectScale);
		jrr.setWidth((regRectWhiteBorder + regRectBlackSquare + regRectWhiteBorder) * regRectScale);
		jrr.setHeight((regRectWhiteBorder + regRectBlackSquare + regRectWhiteBorder) * regRectScale);
		jrr.getLinePen().setLineWidth(1);
		band.addElement(jrr);

		// inner black rectangle
		jrr = new JRDesignRectangle(jasperDesign);
		jrr.setBackcolor(Color.black);
		jrr.setForecolor(Color.black);
		jrr.setX(x + 2 * regRectScale);
		jrr.setY(y + 2 * regRectScale);
		jrr.setWidth(regRectBlackSquare * regRectScale);
		jrr.setHeight(regRectBlackSquare * regRectScale);
		jrr.getLinePen().setLineWidth(1);
		band.addElement(jrr);

		/* aborted inner white rectangle
		jrr = new JRDesignRectangle(jasperDesign);
		jrr.setBackcolor(Color.white);
		jrr.setForecolor(Color.white);
		jrr.setX(x + 5 * scale);
		jrr.setY(y + 5 * scale);
		jrr.setWidth(3 * scale);
		jrr.setHeight(3 * scale);
		jrr.getLinePen().setLineWidth(1);
		// band.addElement(jrr);
		 * */

	}

}
