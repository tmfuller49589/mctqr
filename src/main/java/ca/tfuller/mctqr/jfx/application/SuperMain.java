package ca.tfuller.mctqr.jfx.application;

public class SuperMain {

	/*
	 *  This class is needed to avoid an issue with JavaFX and a shaded jar.
	 *  https://stackoverflow.com/questions/52653836/maven-shade-javafx-runtime-components-are-missing
	 */
	public static void main(String[] args) {
		Main.main(args);
	}

}
