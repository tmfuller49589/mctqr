package ca.tfuller.mctqr.jfx.application;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.jfx.view.MainController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {
	private Stage primaryStage;
	private BorderPane rootLayout;
	private MainController mainController;
	protected static final Logger logger = LogManager.getLogger(Main.class);

	public Main() {
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			this.primaryStage = primaryStage;
			this.primaryStage.setTitle("MctQr");

			initRootLayout();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initializes the root layout.
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/ca/tfuller/mctqr/jfx/view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			mainController = (MainController) loader.getController();
			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);

			scene.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent ke) {
					if (ke.getCode() == KeyCode.ESCAPE) {
						logger.info("Key Pressed: " + ke.getCode());
						ke.consume(); // <-- stops passing the event to next
										// node
					}
				}
			});

			primaryStage.setScene(scene);
			primaryStage.show();
			mainController.setPrimaryStage(primaryStage);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the main stage.
	 * 
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main(String[] args) {
		launch(args);
	}

}
