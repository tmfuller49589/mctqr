package ca.tfuller.mctqr.jfx.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.geometry.BubbleFinder;
import ca.tfuller.mctqr.imageregistration.ImReg;
import ca.tfuller.mctqr.io.McTestIo;
import ca.tfuller.mctqr.mctest.MarkSheet;
import ca.tfuller.mctqr.mctest.McTest;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class ImRegTest extends Application {

	protected static final Logger logger = LogManager.getLogger(ImRegTest.class);

	public void test() {
		File folder = new File("/home/tfuller/git/mctqr/src/main/resources/");
		String sourceFile = folder.getAbsolutePath() + "/scanFilled.png";

		MainController mc = new MainController();

		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			Image image = new Image(fis);
			McTest mcTest = new McTest();
			mcTest.getMcTestMetaData().setTestUuid(UUID.fromString("f4a4e453-009d-49a9-a30a-bdc781c0dbe6"));
			McTestIo mcTestIo = new McTestIo(mcTest);

			mcTestIo.readMcTestMaster();

			ImReg imReg = new ImReg(mcTest, image);
			imReg.register();
			image = imReg.getImage();

			// write the image
			File file = new File("registeredR.png");
			boolean result = ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
			logger.info("wrote to file " + file.getAbsolutePath());
			logger.info("result is " + result);

			String studentId = null; // todo put in real student id

			BubbleFinder bf = new BubbleFinder(imReg, mcTest.getMcTestStudent(studentId));
			bf.matchScannedBubblesToQuestions();
			bf.dumpBubbleMatch();

			DisplayContours dp = new DisplayContours(image);
			dp.setAnchorFinder(imReg.getAnchorFinder());
			dp.setContourLevelPaths(imReg.getContourLevelPaths());

			dp.setContourLoops(imReg.getContourLoops());

			dp.setMcTestAbstract(mcTest.getMcTestStudent(studentId));

			dp.refresh();
			MarkSheet markSheet = new MarkSheet(mcTest, mcTest.getMcTestStudent(studentId), imReg);
			markSheet.markSheet();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		MainController.loadPreferences();

		StackPane root = new StackPane();
		primaryStage.setScene(new Scene(root, 300, 250));
		primaryStage.show();
		test();
	}

}
