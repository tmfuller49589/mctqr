package ca.tfuller.mctqr.jfx.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.services.classroom.model.Student;

import ca.tfuller.mctqr.imageregistration.ImReg;
import ca.tfuller.mctqr.io.McTestIo;
import ca.tfuller.mctqr.io.McTestsIo;
import ca.tfuller.mctqr.io.PojoIo;
import ca.tfuller.mctqr.mctest.MarkSheet;
import ca.tfuller.mctqr.mctest.McTest;
import ca.tfuller.mctqr.mctest.McTestStudent;
import ca.tfuller.mctqr.preferences.Preferences;
import ca.tfuller.mctqr.utils.IdNameObject;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

// TODO move marking to background thread, show waiting cursor
// TODO upload marks to Google sheet

public class MainController {

	protected static final Logger logger = LogManager.getLogger(MainController.class);

	@FXML
	private TabPane tabPane;

	@FXML
	private Tab mcTestEditTab;
	@FXML
	private Tab markTab;

	@FXML
	private TableView<McTest> mcTestTable;
	@FXML
	private TableColumn<McTest, String> testNameColumn;
	@FXML
	private TableColumn<McTest, String> courseNameColumn;
	@FXML
	private TableColumn<McTest, String> courseIdColumn;

	@FXML
	private TableView<McTestStudent> studentMcTestTable;
	@FXML
	private TableColumn<McTestStudent, IdNameObject<Student>> studentNameColumn;
	@FXML
	private TableColumn<McTestStudent, Number> testScoreColumn;

	@FXML
	private Button drawPathButton;
	@FXML
	private TextField drawPathTextField;
	@FXML
	private Button loadTestScansButton;

	@FXML
	private Button viewAnswerSheetButton;

	@FXML
	private Button contourButton;

	@FXML
	private Button saveAnswerKeyButton;
	@FXML
	private Button markTestsButton;

	@FXML
	private McTestEditorController mcTestEditorController;

	private final ObservableList<McTest> mcTests = FXCollections.observableArrayList();
	private final ObservableList<McTestStudent> studentMcTests = FXCollections.observableArrayList();

	private static Preferences preferences = new Preferences();
	private static PojoIo<Preferences> preferencesIo = new PojoIo<>("prefs.txt", preferences);
	private final McTestsIo mcTestsIo = new McTestsIo();
	private McTest mcTest;
	private DisplayContours dc;
	private Stage primaryStage;

	public MainController() {
		McTestIo.setup();

		preferencesIo.load();
		preferences.dump();

		logger.info("calling loadMcTests()");
		loadMcTests();

		logger.info("back from calling loadMcTests()");
	}

	public static void loadPreferences() {
		preferencesIo.load();
		preferences.dump();
	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		logger.info("in initialize");
		tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {

			@Override
			public void changed(ObservableValue<? extends Tab> observable, Tab oldTab, Tab newTab) {
				if (newTab == mcTestEditTab) {
					logger.debug("edit tab selected");
					logger.info("calling setMcTest with " + mcTest);
					mcTestEditorController.setMcTest(mcTest);
				} else if (newTab == markTab) {
					logger.debug("mark tab selected");
					initMarkTab();
				}
			}
		});

		mcTestEditorController.setMainController(this);

		testNameColumn.setCellValueFactory(cellData -> cellData.getValue().getMcTestMetaData().testNameProperty());
		courseNameColumn.setCellValueFactory(cellData -> cellData.getValue().getMcTestMetaData().courseNameProperty());
		courseIdColumn.setCellValueFactory(cellData -> cellData.getValue().getMcTestMetaData().courseIdProperty());

		// Clear test details.
		showMcTestDescription(null);

		mcTestTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> showMcTestDescription(newValue));

		if (mcTests.size() > 0) {
			mcTest = mcTests.get(0);
		}
		mcTestTable.setItems(mcTests);
		mcTestTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

		mcTestTable.getSelectionModel().select(0);

		// set up table for student tests and scores
		//studentMcTestTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		studentNameColumn.setCellValueFactory(cellData -> cellData.getValue().studentProperty());
		testScoreColumn.setCellValueFactory(cellData -> cellData.getValue().numberCorrectProperty());
		studentMcTestTable.setItems(studentMcTests);

		studentMcTestTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> showStudentMcTest(newValue));

		logger.info("Main controller initialized");

	}

	protected void initMarkTab() {
		logger.info("mcTest is " + mcTest.getMcTestMetaData().getTestName());
		mcTestsIo.readStudentTests(mcTest);
		studentMcTests.clear();
		for (Entry<String, McTestStudent> entry : mcTest.getMcTestStudentMap().entrySet()) {
			studentMcTests.add(entry.getValue());
		}
	}

	private Object showStudentMcTest(McTestStudent mct) {
		logger.debug("newValue is " + mct);

		ImReg imReg = new ImReg(mcTest, mct.getImage());
		try {
			imReg.register();
		} catch (InterruptedException | ExecutionException e) {
			MessageWindow.showError(e.getMessage());
			e.printStackTrace();
		}

		mct.setImage(imReg.getImage());

		DisplayContours dp = new DisplayContours(mct.getImage());
		dp.setAnchorFinder(imReg.getAnchorFinder());
		dp.setContourLevelPaths(imReg.getContourLevelPaths());
		dp.setMcTest(mcTest);
		//dp.setContourLoops(imReg.getContourLoops());

		dp.setMcTestAbstract(mct);
		dp.refresh();
		return null;
	}

	public void loadMcTests() {
		try {
			mcTestsIo.readTestMasters(mcTests);
		} catch (IOException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		}
	}

	public static PojoIo<Preferences> getPreferencesIo() {
		return preferencesIo;
	}

	public static Preferences getPreferences() {
		return preferences;
	}

	private void showMcTestDescription(McTest mcTest1) {
		this.mcTest = mcTest1;
		logger.info("calling setMcTest with " + mcTest1);
		mcTestEditorController.setMcTest(mcTest);
	}

	public McTest getMcTest() {
		if (mcTest != null) {
			logger.debug("mcTest is " + mcTest.getMcTestMetaData().getTestName());
		}
		return mcTest;
	}

	public McTestEditorController getMcTestEditorController() {
		return mcTestEditorController;
	}

	public void clearMcTestSelection() {
		mcTestTable.getSelectionModel().clearSelection();
	}

	public void setSelectedMcTest(McTest mcTest2) {
		mcTestTable.getSelectionModel().clearSelection();
		for (McTest mc : mcTestTable.getItems()) {
			logger.info("mcTest2 is " + mcTest2);
			logger.info("mcTest2 data is " + mcTest2.getMcTestMetaData());
			logger.info(" mcTest data is " + mc.getMcTestMetaData());
			logger.info("mcTest2 uuid is " + mcTest2.getMcTestMetaData().getTestUuid());
			logger.info(" mcTest uuid is " + mc.getMcTestMetaData().getTestUuid());

			if (mc.getMcTestMetaData().getTestUuid().toString()
					.equals(mcTest2.getMcTestMetaData().getTestUuid().toString())) {
				mcTestTable.getSelectionModel().select(mc);
				break;
			}
		}
	}

	@FXML
	public void drawPathAction() {
		int pathNo = Integer.parseInt(drawPathTextField.getText());
		dc.drawPath(pathNo);
	}

	@FXML
	public void contourAction() {
		logger.debug("view answer sheet clicked");
		// String sourceFile =
		// getClass().getClassLoader().getResource("s62.jpg").getFile();
		McTestIo mcTestIo = new McTestIo(mcTest);
		String sourceFile = mcTestIo.getMcTestAnswerKeyImagePath().toAbsolutePath().toString();
		logger.debug("source file is " + sourceFile);
		try {
			FileInputStream fis;
			fis = new FileInputStream(sourceFile);
			Image image = new Image(fis);
			logger.debug("image width is " + image.getWidth());
			dc = new DisplayContours(image);
			dc.setMcTest(mcTest);
			dc.computeContours();
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		}
	}

	@FXML
	public void viewAnswerSheetAction() {
		logger.debug("view answer sheet clicked");
		try {
			BubbleSheetViewer bsv = new BubbleSheetViewer(mcTest);
		} catch (FileNotFoundException e) {
			MessageWindow.showError(e.getMessage());
		} catch (IOException e) {
			MessageWindow.showError(e.getMessage());
		}
	}

	@FXML
	public void saveAnswerKeyAction() {
		McTestIo mcTestIo = new McTestIo(mcTest);
		try {
			mcTestIo.writeMcTestAnswerKeyFile();
		} catch (IOException e) {
			MessageWindow.showError(e.getMessage());
		}
	}

	@FXML
	public void loadTestScans() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select test scans");
		List<File> fileList = fileChooser.showOpenMultipleDialog(primaryStage);

		// list returned by fileChooser is unmodifiable.
		// Make a copy that is modifiable.
		List<File> fileList2 = new ArrayList<File>();
		fileList2.addAll(fileList);
		McTestIo.loadScans(fileList2);
	}

	@FXML
	public void markTestsAction() {
		logger.info("mark tests");
		for (McTestStudent mct : studentMcTests) {
			logger.info(mct.getStudent().getObject().getProfile().getName().getFullName() + " "
					+ mct.getScannedFile().getAbsolutePath());

			try (FileInputStream fis = new FileInputStream(mct.getScannedFile())) {

				Image image = new Image(fis);
				ImReg imReg = new ImReg(mcTest, image);
				imReg.register();
				image = imReg.getImage();

				//BubbleFinder bf = new BubbleFinder(imReg, mct);
				//bf.matchScannedBubblesToQuestions();
				//bf.dumpBubbleMatch();

				//DisplayContours dp = new DisplayContours(image);
				//dp.setAnchorFinder(imReg.getAnchorFinder());
				//dp.setContourLevelPaths(imReg.getContourLevelPaths());
				//dp.setContourLoops(imReg.getContourLoops());
				//dp.setMcTest(mct);
				//dp.refresh();
				MarkSheet markSheet = new MarkSheet(mcTest, mct, imReg);
				markSheet.markSheet();

				// write the image
				//File file = new File("r" + mct.getScannedFile().toPath().getFileName());
				//boolean result = ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
				//logger.info("wrote to file " + file.getAbsolutePath());
				//logger.debug("result is " + result);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
				MessageWindow.showError(e.getMessage());
			} catch (InterruptedException e) {
				e.printStackTrace();
				MessageWindow.showError(e.getMessage());
			} catch (ExecutionException e) {
				e.printStackTrace();
				MessageWindow.showError(e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				MessageWindow.showError(e.getMessage());
			}
		}
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

}