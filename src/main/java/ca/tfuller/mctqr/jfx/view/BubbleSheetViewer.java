package ca.tfuller.mctqr.jfx.view;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.geometry.Circle;
import ca.tfuller.mctqr.io.McTestIo;
import ca.tfuller.mctqr.mctest.Bubble;
import ca.tfuller.mctqr.mctest.McTest;
import ca.tfuller.mctqr.mctest.QuestionMaster;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;

public class BubbleSheetViewer {
	private final Image image;
	protected static final Logger logger = LogManager.getLogger(BubbleSheetViewer.class);
	private double scaleFactor = 1.0;
	private final Stage stage;
	private final Group root;
	private final Canvas canvas;
	private final Scene scene;
	private final Scale scale = new Scale();
	private final McTest mcTest;
	private final McTestIo mcTestIo;

	public BubbleSheetViewer(McTest mcTest) throws IOException {
		this.mcTest = mcTest;
		mcTestIo = new McTestIo(mcTest);
		mcTestIo.readMcTestMaster();

		stage = new Stage();
		root = new Group();
		scene = new Scene(root, 850, 850 * 11 / 8.5);
		scale.setPivotX(0);
		scale.setPivotY(0);

		Path path = mcTestIo.getMcTestAnswerKeyImagePath();
		InputStream is = new FileInputStream(path.toFile());
		BufferedInputStream bis = new BufferedInputStream(is);

		image = new Image(bis);
		bis.close();

		canvas = new Canvas(image.getWidth(), image.getHeight());

		final GraphicsContext graphicsContext = canvas.getGraphicsContext2D();

		root.getChildren().add(canvas);
		root.getTransforms().add(scale);

		stage.setTitle("answer key");
		stage.setScene(scene);
		stage.show();

		graphicsContext.drawImage(image, 0, 0);
		fillMcTestBubbles();

		setScale(stage.getWidth(), stage.getHeight());

		canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				mouseClicked(event.getX(), event.getY());
			}
		});

		canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {

			}
		});

		canvas.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {

			}
		});

		ChangeListener<Number> stageSizeListener = (observable, oldValue, newValue) -> setScale(stage.getWidth(),
				stage.getHeight());

		stage.widthProperty().addListener(stageSizeListener);
		stage.heightProperty().addListener(stageSizeListener);

	}

	private void fillMcTestBubbles() {
		for (Entry<Integer, QuestionMaster> entry : mcTest.getMcTestMaster().getQuestions().entrySet()) {
			QuestionMaster q = entry.getValue();
			fillQuestionBubbles(q);
		}
	}

	private void setScale(double newWidth, double newHeight) {

		double scalex = newWidth / image.getWidth();
		double scaley = newHeight / image.getHeight();

		scaleFactor = Math.min(scalex, scaley);

		scale.setX(scaleFactor);
		scale.setY(scaleFactor);
	}

	private void mouseClicked(double x, double y) {
		logger.debug(x + ", " + y);

		for (Entry<Integer, QuestionMaster> entry : mcTest.getMcTestMaster().getQuestions().entrySet()) {
			QuestionMaster q = entry.getValue();
			for (Bubble b : q.getBubbles()) {
				if (b.getGeneratedCircle().inside(x, y)) {
					logger.debug("clicked a circle question " + q.getQuestionNumber() + " choice " + b.getChoice());
					q.setCorrectAnswer(b.getChoice());
					fillQuestionBubbles(q);
					return;
				}
			}
		}
	}

	private void fillQuestionBubbles(QuestionMaster q) {
		final GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
		for (Bubble b : q.getBubbles()) {
			if (b.getChoice().equals(q.getCorrectAnswer())) {
				graphicsContext.setFill(Color.LAWNGREEN);
			} else {
				graphicsContext.setFill(Color.DARKGREY);
			}
			Circle c = b.getGeneratedCircle();
			graphicsContext.fillOval(c.getCenter().getX() - c.getR(), c.getCenter().getY() - c.getR(), c.getR() * 2,
					c.getR() * 2);
		}
	}
}
