package ca.tfuller.mctqr.jfx.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class MessageWindow {

	static int callDepth = 0;
	protected static final Logger logger = LogManager.getLogger(MessageWindow.class);

	public static void showError(String msg) {
		msg = msg + "\nStack trace:";
		Exception exception = new Exception();
		for (int i = 1; i < exception.getStackTrace().length; ++i) {
			msg += "\n" + exception.getStackTrace()[i].getMethodName();
			if (i > 6) {
				break;
			}
		}

		Alert a = new Alert(AlertType.ERROR);
		a.setTitle("Error");
		a.setContentText(justifyLeft(100, msg, ' '));
		a.showAndWait();
	}

	public static void showMessage(String msg) {
		Alert a = new Alert(AlertType.INFORMATION);
		a.setTitle("Info");
		a.showAndWait();
	}

	public static String justifyLeft(int width, String st, char splitChar) {
		StringBuffer buf = new StringBuffer(st);
		int lastspace = -1;
		int linestart = 0;
		int i = 0;

		while (i < buf.length()) {
			if (buf.charAt(i) == splitChar) {
				lastspace = i;
			}
			if (buf.charAt(i) == '\n') {
				lastspace = -1;
				linestart = i + 1;
			}
			if (i > ((linestart + width) - 1)) {
				if (lastspace != -1) {
					buf.setCharAt(lastspace, '\n');
					linestart = lastspace + 1;
					lastspace = -1;
				} else {
					buf.insert(i, '\n');
					linestart = i + 1;
				}
			}
			i++;
		}
		return buf.toString();
	}
}
