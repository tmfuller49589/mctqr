package ca.tfuller.mctqr.jfx.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.geometry.AnchorFinder;
import ca.tfuller.mctqr.geometry.Geometry;
import ca.tfuller.mctqr.geometry.ImageToArray;
import ca.tfuller.mctqr.geometry.Square;
import ca.tfuller.mctqr.imageregistration.ImReg;
import ca.tfuller.mctqr.marchingsquares.Algorithm;
import ca.tfuller.mctqr.mctest.Bubble;
import ca.tfuller.mctqr.mctest.McTest;
import ca.tfuller.mctqr.mctest.McTestAbstract;
import ca.tfuller.mctqr.mctest.McTestStudent;
import ca.tfuller.mctqr.mctest.QuestionAbstract;
import ca.tfuller.mctqr.mctest.QuestionMaster;
import ca.tfuller.mctqr.mctest.QuestionStudent;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;

public class DisplayContours {
	private static final long serialVersionUID = 1L;

	protected static final Logger logger = LogManager.getLogger(DisplayContours.class);
	private final Group root;
	private final Canvas canvas;
	private final Scene scene;
	private final Scale scale = new Scale();
	private double scaleFactor = 1.0;

	private Path[] contourLevelPaths = null;
	private List<Path> contourLoops = new ArrayList<Path>();
	private McTestAbstract<? extends QuestionAbstract> mcTestAbstract = null;
	private McTest mcTest = null;

	private AnchorFinder anchorFinder;
	private ImReg imReg;
	private final Image image;

	private int mouseX;
	private int mouseY;

	private final Stage stage;

	public void setAnchorFinder(AnchorFinder anchorFinder) {
		this.anchorFinder = anchorFinder;
	}

	enum DrawTypeEnum {
		STROKE, FILL;
	}

	public static void main(String[] args) {
		File folder = new File("/home/tfuller/git/mctqr/src/main/resources/");

		// String sourceFile = folder.getAbsolutePath() + "/s61.jpg";
		String sourceFile = folder.getAbsolutePath() + "/img1r270.jpg";
		FileInputStream fis;
		try {
			fis = new FileInputStream(sourceFile);
			Image image = new Image(fis);

			DisplayContours dp = new DisplayContours(image);
			dp.computeContours();
			fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// dp.prepareGUI();
		// dp.setVisible(true);

	}

	public DisplayContours(Image image) {
		super();
		this.image = image;

		stage = new Stage();
		root = new Group();
		scene = new Scene(root, 850, 850 * 11 / 8.5);
		scale.setPivotX(0);
		scale.setPivotY(0);

		// primaryStage is the main top level window created by platform
		stage.setTitle("JavaFX Demo");
		stage.setScene(scene);
		stage.show();

		canvas = new Canvas(image.getWidth(), image.getHeight());

		final GraphicsContext graphicsContext = canvas.getGraphicsContext2D();

		root.getChildren().add(canvas);
		root.getTransforms().add(scale);

		stage.setTitle("answer key");
		stage.setScene(scene);
		stage.show();

		graphicsContext.drawImage(image, 0, 0);

		// setScale(stage.getWidth(), stage.getHeight());

		canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				mouseClicked(event.getX(), event.getY());
			}
		});

		canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {

			}
		});

		canvas.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {

			}
		});

		ChangeListener<Number> stageSizeListener = (observable, oldValue, newValue) -> setScale(stage.getWidth(),
				stage.getHeight());

		stage.widthProperty().addListener(stageSizeListener);
		stage.heightProperty().addListener(stageSizeListener);
	}

	public void computeContours() {

		try {

			Algorithm alg = new Algorithm();
			double levels[] = { 0.5 };

			double[][] data = ImageToArray.convert(image);
			contourLevelPaths = alg.buildContours(data, levels);

			logger.debug("gps size is " + contourLevelPaths.length);
			Path allContoursPath = contourLevelPaths[0];

			if (allContoursPath.getElements().size() == 0) {
				logger.debug("oops, no path elements");
			}

		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	public void setContourLevelPaths(Path[] contourLevelPaths) {
		this.contourLevelPaths = contourLevelPaths;
	}

	public void refresh() {
		GraphicsContext g2 = canvas.getGraphicsContext2D();

		String mouseLoc = "(" + mouseX + ", " + mouseY + ")";
		g2.strokeText(mouseLoc, 50, 70);

		//drawPath(g2, contourLevelPaths[0]); //this  line will draw all contours

		if (mcTestAbstract != null) {
			for (Entry<Integer, ? extends QuestionAbstract> entry : mcTestAbstract.getQuestions().entrySet()) {
				QuestionAbstract q = entry.getValue();
				if (mcTestAbstract instanceof McTestStudent) {
					markQuestion(g2, q);
				} else {
					for (Bubble b : q.getBubbles()) {
						if (b.getScannedCircle() == null || b.getScannedCircle().getPath() == null) {
							logger.debug("scanned path is null");
						} else {
							g2.setStroke(Color.YELLOW);
							g2.setFill(Color.YELLOW);
							logger.debug("drawing scanned circle " + b.getScannedCircle().getCenter().getX() + " "
									+ b.getScannedCircle().getCenter().getY() + " r = " + b.getScannedCircle().getR());
							drawPath(g2, b.getScannedCircle().getPath(), DrawTypeEnum.STROKE);
						}

						if (b.getGeneratedCircle().getPath() == null) {
							g2.setFill(Color.BLUEVIOLET);
							logger.debug("generated path is null");
							//g2.fillOval(b.getGeneratedCircle().getCenter().getX(), b.getGeneratedCircle().getCenter().getY(),
							//		b.getGeneratedCircle().getR(), b.getGeneratedCircle().getR() * 0.75);
							double r = b.getGeneratedCircle().getR();
							logger.debug("drawing generated circle " + b.getGeneratedCircle().getCenter().getX() + " "
									+ b.getGeneratedCircle().getCenter().getY() + " r = " + r);
							double x = b.getGeneratedCircle().getCenter().getX() - r;
							double y = b.getGeneratedCircle().getCenter().getY() - r;

							//g2.fillOval(x, y, r * 2, r * 2);
							g2.strokeOval(x, y, r * 2, r * 2);
						} else {
							g2.setFill(Color.GREEN);
							drawPath(g2, b.getGeneratedCircle().getPath(), DrawTypeEnum.STROKE);
						}
					}
				}
			}
		}

		g2.setFill(Color.AQUA);
		for (Square s : anchorFinder.getAnchors()) {
			drawPath(g2, s.getPath(), DrawTypeEnum.FILL);
		}

		g2.setFill(Color.RED);
		g2.setStroke(Color.RED);
		for (Path gp : contourLoops) {
			if (Geometry.getBoundsArea(gp) < 2000) {
				drawPath(g2, gp, DrawTypeEnum.STROKE);
			}
		}
	}

	private void markQuestion(GraphicsContext g, QuestionAbstract qAbstract) {
		McTestStudent mcts = (McTestStudent) mcTestAbstract;
		QuestionStudent q = (QuestionStudent) qAbstract;
		logger.info("Test: " + mcts.getStudent().getName() + " question: " + q.getQuestionNumber() + " is correct: "
				+ q.getCorrect());
		Bubble firstBubble = q.getBubbles().get(0);
		Point2D point = firstBubble.getGeneratedCircle().getCenter();
		Point2D pointO = point.add(-30, 0);
		if (q.getCorrect()) {
			drawCheck(g, pointO);
		} else {
			drawX(g, pointO);
		}
		for (Bubble b : q.getBubbles()) {
			QuestionMaster qm = mcTest.getMcTestMaster().getQuestions().get(q.getQuestionNumber());
			double r = b.getGeneratedCircle().getR();
			double x = b.getGeneratedCircle().getCenter().getX() - r;
			double y = b.getGeneratedCircle().getCenter().getY() - r;
			logger.debug("bubble " + b.getChoice() + " filled is " + b.getFilled());
			if (b.getFilled() && b.getChoice().equals(qm.getCorrectAnswer())) {
				g.setFill(Color.GREEN);
				g.strokeOval(x, y, r * 2, r * 2);
			} else if (b.getFilled()) {
				g.setFill(Color.RED);
				g.strokeOval(x, y, r * 2, r * 2);
			}
		}
	}

	public void drawCheck(GraphicsContext g, Point2D point) {
		double x1 = point.getX();
		double x2 = point.getX() + 5;
		double y1 = point.getY();
		double y2 = point.getY() + 5;
		double x3 = point.getX() + 15;
		double y3 = point.getY() - 15;
		g.setStroke(Color.GREEN);
		g.setLineWidth(3);
		g.strokeLine(x1, y1, x2, y2);
		g.strokeLine(x2, y2, x3, y3);
	}

	public void drawX(GraphicsContext g, Point2D point) {
		double x1 = point.getX();
		double x2 = point.getX() + 10;
		double y1 = point.getY();
		double y2 = point.getY() + 10;
		g.setStroke(Color.RED);
		g.setLineWidth(3);
		g.strokeLine(x1, y1, x2, y2);
		g.strokeLine(x1, y2, x2, y1);
	}

	public void setContourLoops(List<Path> contourLoops) {
		this.contourLoops = contourLoops;
	}

	private void drawPath(GraphicsContext gc, Path path, DrawTypeEnum drawType) {
		gc.beginPath();
		//logger.debug("drawing path, #elements = " + path.getElements().size());

		for (PathElement pe : path.getElements()) {
			if (pe instanceof MoveTo) {
				// logger.debug("draw path got move to " + ((MoveTo) pe).getX() +
				// ", " + ((MoveTo) pe).getY());
				gc.moveTo(((MoveTo) pe).getX(), ((MoveTo) pe).getY());
			} else if (pe instanceof LineTo) {
				// logger.debug("draw path got line to " + ((LineTo) pe).getX() +
				// ", " + ((LineTo) pe).getY());
				gc.lineTo(((LineTo) pe).getX(), ((LineTo) pe).getY());
			} else if (pe instanceof ClosePath) {
				gc.closePath();
				switch (drawType) {
				case STROKE:
					gc.stroke();
					break;
				case FILL:
					gc.fill();
					break;
				}
			}
		}
	}

	public void dumpPathCenters() {
		logger.debug("dumping path centres, size = " + contourLoops.size());

		for (Path gp : contourLoops) {
			Rectangle2D rect = Geometry.getBounds(gp);
			logger.debug("(" + Geometry.getCenterX(rect) + ", " + Geometry.getCenterY(rect) + ")");
		}
	}

	void pathDump(Path gp) {
		logger.debug(" path dump");

	}

	private void mouseClicked(double x, double y) {
		logger.debug(x + ", " + y);
	}

	private void setScale(double newWidth, double newHeight) {

		if (contourLevelPaths == null) {
			return;
		}

		double scalex = newWidth / image.getWidth();
		double scaley = newHeight / image.getHeight();

		scaleFactor = Math.min(scalex, scaley);

		scale.setX(scaleFactor);
		scale.setY(scaleFactor);
	}

	public void drawPath(int pathNo) {
		GraphicsContext g2 = canvas.getGraphicsContext2D();
		g2.setFill(Color.LIME);
		drawPath(g2, contourLoops.get(pathNo), DrawTypeEnum.STROKE);
	}

	public Path[] getContourLevelPaths() {
		return contourLevelPaths;
	}

	public void setMcTestAbstract(McTestAbstract<? extends QuestionAbstract> mcTest) {
		this.mcTestAbstract = mcTest;
	}

	public void setMcTest(McTest mcTest) {
		this.mcTest = mcTest;
	}
}