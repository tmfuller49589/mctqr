package ca.tfuller.mctqr.jfx.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.services.classroom.model.Course;
import com.google.api.services.classroom.model.Student;

import ca.tfuller.mctqr.googleclassroom.ClassroomData;
import ca.tfuller.mctqr.io.McTestIo;
import ca.tfuller.mctqr.mctest.McTest;
import ca.tfuller.mctqr.sheetgenerator.AnswerSheet;
import ca.tfuller.mctqr.sheetgenerator.AnswerSheetData;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;

public class McTestEditorController {
	protected static final Logger logger = LogManager.getLogger(McTestEditorController.class);
	ClassroomData classroomData;
	@FXML
	private ComboBox<Course> courseComboBox;
	@FXML
	private TextField instructorNameField;
	@FXML
	private TextField testNameField;
	@FXML
	private DatePicker testDatePicker;

	@FXML
	private Spinner<Integer> choicesPerQuestionSpinner;
	@FXML
	private Spinner<Integer> columnsSpinner;
	@FXML
	private Spinner<Integer> fontSizeSpinner;
	@FXML
	private Spinner<Integer> bubbleSizeSpinner;
	@FXML
	private Spinner<Integer> numberOfQuestionsSpinner;

	private ObjectProperty<Integer> numberOfQuestionsObject;
	private ObjectProperty<Integer> columnsObject;
	private ObjectProperty<Integer> fontSizeObject;
	private ObjectProperty<Integer> bubbleSizeObject;
	private ObjectProperty<Integer> choicesPerQuestionObject;

	@FXML
	private Button saveTestButton;
	@FXML
	private Button newTestButton;
	@FXML
	private Button deleteTestButton;
	@FXML
	private Button printButton;
	@FXML
	private Label testUuidLabel;

	private MainController mainController;
	private McTest mcTest;
	private boolean dataBound = false;

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		classroomData = new ClassroomData();

		courseComboBox.setConverter(new StringConverter<Course>() {
			@Override
			public String toString(Course course) {
				if (course == null) {
					return "null course";
				}
				return course.getName();
			}

			@Override
			public Course fromString(String string) {
				// combobox is not editable,
				// so we don't need to convert from string to object 
				return null;
			}
		});

		try {
			List<Course> courses = ClassroomData.getCourses();
			ObservableList<Course> ol = FXCollections.observableArrayList(courses);

			courseComboBox.setItems(ol);

		} catch (IOException e) {
			e.printStackTrace();
		}

		choicesPerQuestionSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 7));
		columnsSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10));
		fontSizeSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 20));
		bubbleSizeSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 30));
		numberOfQuestionsSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 500));

		logger.debug("initialized");
		bindData();
	}

	public void unbindData() {

		if (mcTest == null) {
			return;
		}
		if (dataBound == false) {
			return;
		}
		instructorNameField.textProperty().unbindBidirectional(mcTest.getMcTestMetaData().instructorNameProperty());
		testNameField.textProperty().unbindBidirectional(mcTest.getMcTestMetaData().testNameProperty());

		testDatePicker.valueProperty().unbindBidirectional(mcTest.getMcTestMetaData().dateProperty());
		testUuidLabel.textProperty().unbind();

		numberOfQuestionsSpinner.getValueFactory().valueProperty().unbindBidirectional(numberOfQuestionsObject);
		choicesPerQuestionSpinner.getValueFactory().valueProperty().unbindBidirectional(choicesPerQuestionObject);
		columnsSpinner.getValueFactory().valueProperty().unbindBidirectional(columnsObject);
		fontSizeSpinner.getValueFactory().valueProperty().unbindBidirectional(fontSizeObject);
		bubbleSizeSpinner.getValueFactory().valueProperty().unbindBidirectional(bubbleSizeObject);
	}

	public void bindData() {

		if (mcTest == null) {
			logger.debug("mcTest is null, not binding");
			return;
		}
		logger.debug("mcTest binding");

		instructorNameField.textProperty().bindBidirectional(mcTest.getMcTestMetaData().instructorNameProperty());
		testNameField.textProperty().bindBidirectional(mcTest.getMcTestMetaData().testNameProperty());

		testDatePicker.valueProperty().bindBidirectional(mcTest.getMcTestMetaData().dateProperty());

		testUuidLabel.textProperty().bind(Bindings.createObjectBinding(() -> {
			if (mcTest.getMcTestMetaData().testUuidProperty().get() != null) {
				return mcTest.getMcTestMetaData().testUuidProperty().get().toString();
			}
			return "";
		}, mcTest.getMcTestMetaData().testUuidProperty()));

		numberOfQuestionsSpinner.getValueFactory().valueProperty().bindBidirectional(numberOfQuestionsObject);
		choicesPerQuestionSpinner.getValueFactory().valueProperty().bindBidirectional(choicesPerQuestionObject);
		columnsSpinner.getValueFactory().valueProperty().bindBidirectional(columnsObject);
		fontSizeSpinner.getValueFactory().valueProperty().bindBidirectional(fontSizeObject);
		bubbleSizeSpinner.getValueFactory().valueProperty().bindBidirectional(bubbleSizeObject);

		dataBound = true;
	}

	/**
	 * Sets the mcTestDescription to be edited in the tab.
	 * 
	 * @param
	 */
	public void setMcTest(McTest mcTestx) {

		logger.info("setting mcTest to " + mcTestx);
		logger.info("calling unbind data");
		unbindData();
		this.mcTest = mcTestx;

		if (mcTest != null && mcTest.getMcTestMetaData() != null) {
			logger.debug("mc test set to name -->" + mcTest.getMcTestMetaData().getTestName() + "<-- -->"
					+ mcTest.getMcTestMetaData().getCourseName() + "<-- -->"
					+ mcTest.getMcTestMetaData().getInstructor() + "<--");

			numberOfQuestionsObject = mcTest.getMcTestMetaData().numberOfQuestionsProperty().asObject();
			choicesPerQuestionObject = mcTest.getMcTestMetaData().choicesPerQuestionProperty().asObject();
			columnsObject = mcTest.getMcTestMetaData().columnsProperty().asObject();
			fontSizeObject = mcTest.getMcTestMetaData().fontSizeProperty().asObject();
			bubbleSizeObject = mcTest.getMcTestMetaData().bubbleSizeProperty().asObject();

			logger.info("calling bind data");
			bindData();

			Course selectedCourse = null;
			for (Course c : courseComboBox.getItems()) {
				if (c.getId().compareTo(mcTest.getMcTestMetaData().getCourseId()) == 0) {
					selectedCourse = c;
					break;
				}
			}
			courseComboBox.getSelectionModel().select(selectedCourse);
		}
	}

	@FXML
	public void courseComboBoxAction(ActionEvent event) {
		logger.debug("course combobox action");
		logger.info("selected course is " + courseComboBox.getSelectionModel().getSelectedItem().getName());
		logger.info("action event is " + event);
		Course course = courseComboBox.getSelectionModel().getSelectedItem();
		if (course == null) {
			return;
		}

		if (mcTest != null) {
			mcTest.getMcTestMetaData().setCourseName(course.getName());
			mcTest.getMcTestMetaData().setCourseId(course.getId());
		}
	}

	@FXML
	private void newTestAction() {
		mainController.clearMcTestSelection();
		mcTest = new McTest();
		mcTest.getMcTestMetaData().initialize();
		mcTest.getMcTestMetaData().setDefaults();
		logger.info("calling setMcTest with " + mcTest);
		setMcTest(mcTest);

		logger.info("mcTest is " + mcTest);
		logger.info("mcTestMetaData is " + mcTest.getMcTestMetaData());

		if (courseComboBox.getSelectionModel().getSelectedItem() != null) {
			mcTest.getMcTestMetaData().setCourseName(courseComboBox.getSelectionModel().getSelectedItem().getName());
			mcTest.getMcTestMetaData().setCourseId(courseComboBox.getSelectionModel().getSelectedItem().getId());
		}

	}

	@FXML
	private void deleteTestAction() {
		McTestIo mcTestIo = new McTestIo(mcTest);
		try {
			mcTestIo.readMcTestGeometryFile();
			mcTestIo.readMcTestMasterInfoFile();
		} catch (IOException e) {
			e.printStackTrace();
			MessageWindow.showError(e.getMessage());
		}

	}

	@FXML
	private void saveTestAction() {
		if (isInputValid()) {
			McTestIo mcTestIo = new McTestIo(mcTest);
			// keep a reference to current mcTest
			// because reloadMcTests in mainController will
			// set mcTest to null via selection listener
			McTest mcTestTmp = mcTest;

			logger.info("test date is " + mcTest.getMcTestMetaData().getDate());
			mcTestIo.writeMcTestInfoFile();

			logger.info("test course is " + mcTest.getMcTestMetaData().getCourseName() + " "
					+ mcTest.getMcTestMetaData().getCourseId());
			printAction();

			logger.info("reloading all tests");
			mainController.loadMcTests();
			logger.info("done reloading all tests");
			logger.info("setting selected test in main controller");
			mainController.setSelectedMcTest(mcTestTmp);
		}
	}

	@FXML
	private void printAction() {
		try {
			List<AnswerSheetData> answerSheetDataList = new ArrayList<AnswerSheetData>();
			List<Student> students = ClassroomData.getStudents(mcTest.getMcTestMetaData().getCourseId());

			AnswerSheetData answerKey = new AnswerSheetData();
			initAnswerSheetData(answerKey, null);
			answerSheetDataList.add(answerKey);

			for (Student student : students) {
				AnswerSheetData asd = new AnswerSheetData();
				initAnswerSheetData(asd, student);
				answerSheetDataList.add(asd);
			}

			AnswerSheet as = new AnswerSheet(mcTest);
			as.doIt(answerSheetDataList);

		} catch (IOException e2) {
			e2.printStackTrace();
			MessageWindow.showError(e2.getMessage());
		}
	}

	protected void initAnswerSheetData(AnswerSheetData asd, Student student) {

		if (student == null) {
			asd.setAnswerKey(true);
		}
		asd.setStudent(student);
	}

	/**
	 * Validates the user input in the text fields.
	 * 
	 * @return true if the input is valid
	 */
	private boolean isInputValid() {
		String errorMessage = "";

		if (errorMessage.length() == 0) {
			return true;
		}
		MessageWindow.showError("invalid field");

		return false;
	}

	public McTest getMcTest() {
		logger.debug("mcTest is " + mcTest.getMcTestMetaData().getTestName());
		return mcTest;
	}

	public void setMainController(MainController mainController) {
		this.mainController = mainController;
	}
}