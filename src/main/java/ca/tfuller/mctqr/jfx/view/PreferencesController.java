package ca.tfuller.mctqr.jfx.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.preferences.Preferences;
import javafx.beans.property.ObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;

public class PreferencesController {
	protected static final Logger logger = LogManager.getLogger(PreferencesController.class);

	@FXML
	private TextField instructorNameField;
	@FXML
	private Spinner<Integer> choicesPerQuestionSpinner;
	@FXML
	private Spinner<Integer> columnsSpinner;
	@FXML
	private Spinner<Integer> fontSizeSpinner;
	@FXML
	private Spinner<Integer> bubbleSizeSpinner;
	@FXML
	private Spinner<Integer> numberOfQuestionsSpinner;

	@FXML
	private Button saveButton;

	private Preferences preferences;

	@FXML
	private void initialize() {
		choicesPerQuestionSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 7));
		columnsSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10));
		fontSizeSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 20));
		bubbleSizeSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 30));
		numberOfQuestionsSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 500));
		bindData();
	}

	public void bindData() {
		preferences = MainController.getPreferences();

		preferences.instructorNameProperty().bindBidirectional(instructorNameField.textProperty());
		preferences.choicesPerQuestionProperty()
				.bindBidirectional((ObjectProperty) choicesPerQuestionSpinner.getValueFactory().valueProperty());
		preferences.columnsProperty()
				.bindBidirectional((ObjectProperty) columnsSpinner.getValueFactory().valueProperty());
		preferences.fontSizeProperty()
				.bindBidirectional((ObjectProperty) fontSizeSpinner.getValueFactory().valueProperty());
		preferences.bubbleSizeProperty()
				.bindBidirectional((ObjectProperty) bubbleSizeSpinner.getValueFactory().valueProperty());

		preferences.numberOfQuestionsProperty()
				.bindBidirectional((ObjectProperty) numberOfQuestionsSpinner.getValueFactory().valueProperty());

		/* also works
		Bindings.bindBidirectional(columnsSpinner.getValueFactory().valueProperty(),
				(Property) preferences.columnsProperty());
		*/

		MainController.getPreferencesIo().load();
	}

	@FXML
	private void saveAction() {
		if (isInputValid()) {
			MainController.getPreferencesIo().save();
		}
	}

	/**
	 * Validates the user input in the text fields.
	 * 
	 * @return true if the input is valid
	 */
	private boolean isInputValid() {
		String errorMessage = "";

		if (errorMessage.length() == 0) {
			return true;
		}
		MessageWindow.showError("invalid entry");

		return false;
	}
}