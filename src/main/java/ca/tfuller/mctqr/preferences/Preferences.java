package ca.tfuller.mctqr.preferences;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Preferences implements Serializable {
	protected static final Logger logger = LogManager.getLogger(Preferences.class);
	private static final long serialVersionUID = 1L;
	private final SimpleStringProperty instructor;
	private final SimpleIntegerProperty numberOfQuestions;
	private final SimpleIntegerProperty choicesPerQuestion;
	private final SimpleIntegerProperty columns;
	private final SimpleIntegerProperty bubbleSize;
	private final SimpleIntegerProperty anchorSize;
	private final SimpleIntegerProperty fontSize;

	public Preferences() {
		instructor = new SimpleStringProperty();
		numberOfQuestions = new SimpleIntegerProperty();
		columns = new SimpleIntegerProperty();
		bubbleSize = new SimpleIntegerProperty();
		fontSize = new SimpleIntegerProperty();
		choicesPerQuestion = new SimpleIntegerProperty();
		anchorSize = new SimpleIntegerProperty();

	}

	public IntegerProperty anchorSizeProperty() {
		return anchorSize;
	}

	public IntegerProperty choicesPerQuestionProperty() {
		return choicesPerQuestion;
	}

	public IntegerProperty fontSizeProperty() {
		return fontSize;
	}

	public IntegerProperty bubbleSizeProperty() {
		return bubbleSize;
	}

	public IntegerProperty columnsProperty() {
		return columns;
	}

	public IntegerProperty numberOfQuestionsProperty() {
		return numberOfQuestions;
	}

	public StringProperty instructorNameProperty() {
		return instructor;
	}

	public int getNumberOfQuestions() {
		return numberOfQuestions.get();
	}

	public void setNumberOfQuestions(int numberOfQuestions) {
		this.numberOfQuestions.set(numberOfQuestions);
	}

	public int getChoicesPerQuestion() {
		return choicesPerQuestion.get();
	}

	public void setChoicesPerQuestion(int choicesPerQuestion) {
		this.choicesPerQuestion.set(choicesPerQuestion);
	}

	public int getColumns() {
		return columns.get();
	}

	public void setColumns(int columns) {
		this.columns.set(columns);
	}

	public int getBubbleSize() {
		return bubbleSize.get();
	}

	public void setBubbleSize(int bubbleSize) {
		this.bubbleSize.set(bubbleSize);
	}

	public int getAnchorSize() {
		return anchorSize.get();
	}

	public void setAnchorSize(int anchorSize) {
		this.anchorSize.set(anchorSize);
	}

	public int getFontSize() {
		return fontSize.get();
	}

	public void setFontSize(int fontSize) {
		this.fontSize.set(fontSize);
	}

	public String getInstructor() {
		return instructor.get();
	}

	public void setInstructor(String instructor) {
		this.instructor.set(instructor);
	}

	public void dump() {
		logger.info("instructor is -->" + instructor + "<--");
		logger.info("numberOfQuestions is -->" + numberOfQuestions + "<--");
		logger.info("columns is -->" + columns + "<--");
		logger.info("bubbleSize is -->" + bubbleSize + "<--");
		logger.info("fontSize is -->" + fontSize + "<--");
		logger.info("choicesPerQuestion is -->" + choicesPerQuestion + "<--");
		logger.info("anchorSize is -->" + anchorSize + "<--");

	}

}
