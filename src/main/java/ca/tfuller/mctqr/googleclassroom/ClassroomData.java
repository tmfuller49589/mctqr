package ca.tfuller.mctqr.googleclassroom;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.classroom.Classroom;
import com.google.api.services.classroom.ClassroomScopes;
import com.google.api.services.classroom.model.Course;
import com.google.api.services.classroom.model.CourseWork;
import com.google.api.services.classroom.model.ListCourseWorkResponse;
import com.google.api.services.classroom.model.ListCoursesResponse;
import com.google.api.services.classroom.model.ListStudentsResponse;
import com.google.api.services.classroom.model.Student;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;

/** Application name. */

public class ClassroomData {
	protected static final Logger logger = LogManager.getLogger(ClassroomData.class);

	private static final String APPLICATION_NAME = "ClassroomData";

	/** Directory to store user credentials for this application. */
	private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"),
			".credentials/monkeymark");

	/** Global instance of the {@link FileDataStoreFactory}. */
	private static FileDataStoreFactory DATA_STORE_FACTORY;

	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	/** Global instance of the HTTP transport. */
	private static HttpTransport HTTP_TRANSPORT;

	private static List<Course> courses = new ArrayList<Course>();
	private static final HashMap<String, List<Student>> studentsCourseMap = new HashMap<String, List<Student>>();

	/**
	 * Global instance of the scopes required by this quickstart.
	 *
	 * If modifying these scopes, delete your previously saved credentials at
	 * ~/.credentials/classroom.googleapis.com-java-quickstart
	 */
	private static final List<String> SCOPES = Arrays.asList(ClassroomScopes.CLASSROOM_COURSES_READONLY,
			ClassroomScopes.CLASSROOM_ROSTERS, ClassroomScopes.CLASSROOM_COURSEWORK_STUDENTS,
			ClassroomScopes.CLASSROOM_COURSEWORK_ME, "https://www.googleapis.com/auth/userinfo.profile");

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @return an authorized Credential object.
	 * @throws IOException
	 */
	public static Credential authorize() throws IOException {
		// Load client secrets.
		InputStream in = ClassroomData.class.getResourceAsStream("/cliSecNew.json");
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES).setDataStoreFactory(DATA_STORE_FACTORY).setAccessType("offline").build();

		LocalServerReceiver lsr = new LocalServerReceiver.Builder().setHost("localhost").setPort(54665).build();

		Credential credential = new AuthorizationCodeInstalledApp(flow, lsr).authorize("user");
		logger.info("Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());

		return credential;
	}

	/**
	 * Build and return an authorized Classroom client service.
	 * 
	 * @return an authorized Classroom client service
	 * @throws IOException
	 */
	public static com.google.api.services.classroom.Classroom getClassroomService() throws IOException {
		Credential credential = authorize();
		return new com.google.api.services.classroom.Classroom.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
				.setApplicationName(APPLICATION_NAME).build();

	}

	/**
	 * Build and return an authorized Classroom client service.
	 * 
	 * @return an authorized Classroom client service
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public static com.google.api.services.sheets.v4.Sheets getSheetService()
			throws IOException, GeneralSecurityException {
		final String spreadsheetId = "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms";
		final String range = "Class Data!A2:E";
		Credential credential = authorize();
		Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
				.setApplicationName(APPLICATION_NAME).build();
		ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
		List<List<Object>> values = response.getValues();
		if (values == null || values.isEmpty()) {
			System.out.println("No data found.");
		} else {
			System.out.println("Name, Major");
			for (List row : values) {
				// Print columns A and E, which correspond to indices 0 and 4.
				System.out.printf("%s, %s\n", row.get(0), row.get(4));
			}
		}
		return service;
	}

	public static List<Course> getCourses() throws IOException {
		if (courses == null || courses.size() == 0) {
			com.google.api.services.classroom.Classroom service = getClassroomService();

			// List the courses that the user has access to.
			Classroom.Courses.List coursesList = service.courses().list();
			do {
				ListCoursesResponse response = coursesList.setPageSize(10).execute();

				courses.addAll(response.getCourses());
				coursesList.setPageToken(response.getNextPageToken());

				if (courses == null || courses.size() == 0) {
					logger.info("No courses found.");
				} else {
					logger.info("Courses:");
					for (Course course : courses) {// Build a new authorized API
													// client
													// service.
						logger.info("course is " + course.getName() + " " + course.getId());
					}
				}
			} while (coursesList.getPageToken() != null && coursesList.getPageToken().length() > 0);
		}
		return courses;
	}

	public static Student getStudent(String studentId, String courseId) throws IOException {
		List<Student> studList = getStudents(courseId);
		for (Student stud : studList) {
			if (stud.getProfile().getId().compareTo(studentId) == 0) {
				return stud;
			}
		}
		return null;
	}

	public static List<Student> getStudents(String courseId) throws IOException {

		logger.debug("course id is " + courseId);
		// check for cache first
		if (studentsCourseMap.containsKey(courseId)) {
			return studentsCourseMap.get(courseId);
		}

		com.google.api.services.classroom.Classroom service = getClassroomService();

		com.google.api.services.classroom.Classroom.Courses.Students.List ccslr = service.courses().students()
				.list(courseId);

		List<Student> studentList = new ArrayList<Student>();
		do {
			ListStudentsResponse lsr = ccslr.setPageSize(10).execute();
			if (lsr.getStudents() != null) {
				studentList.addAll(lsr.getStudents());
			}
			ccslr.setPageToken(lsr.getNextPageToken());
		} while (ccslr.getPageToken() != null && ccslr.getPageToken().length() > 0);

		studentsCourseMap.put(courseId, studentList);

		return studentList;
	}

	public static List<CourseWork> getCourseWork(String courseId) throws IOException {

		logger.debug("course id is " + courseId);

		com.google.api.services.classroom.Classroom service = getClassroomService();

		Classroom.Courses.CourseWork.List request = service.courses().courseWork().list(courseId);
		List<CourseWork> courseworkList = new ArrayList<CourseWork>();
		do {
			ListCourseWorkResponse lcr = request.setPageSize(10).execute();
			if (lcr.getCourseWork() != null) {
				courseworkList.addAll(lcr.getCourseWork());
			}
			request.setPageToken(lcr.getNextPageToken());
		} while (request.getPageToken() != null && request.getPageToken().length() > 0);

		return courseworkList;
	}

	public static void main(String[] args) throws IOException {
		ClassroomData cd = new ClassroomData();
		ClassroomData.getCourses();
	}
}