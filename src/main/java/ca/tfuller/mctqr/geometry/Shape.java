package ca.tfuller.mctqr.geometry;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.geometry.Point2D;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;

public class Shape {

	protected static final Logger logger = LogManager.getLogger(Shape.class);

	protected Point2D center;
	protected Double rmsResidual;
	protected Double xMin;
	protected Double xMax;
	protected Double yMin;
	protected Double yMax;

	protected Path path;
	protected List<Point2D> points = new ArrayList<Point2D>();

	public Shape(Path path) {
		this.path = path;
	}

	public Shape(Double x, Double y, Path path) {
		super();
		center = new Point2D(x, y);
		this.path = path;
	}

	public Shape() {
	}

	@Override
	public Shape clone() {
		Shape clone = new Shape();
		clone.center = new Point2D(center.getX(), center.getY());
		clone.rmsResidual = rmsResidual;
		clone.xMax = xMax;
		clone.xMin = xMin;
		clone.yMax = yMax;
		clone.yMin = yMin;

		return clone;
	}

	public Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		this.path = path;
		extractPoints();
	}

	public Point2D getCenter() {
		return center;
	}

	public void setCenter(Point2D center) {
		this.center = center;
	}

	public List<Point2D> getPoints() {
		return points;
	}

	public void setPoints(List<Point2D> points) {
		this.points = points;
	}

	/*
	 * Calculate distance between center of
	 * this shape and Shape s.
	 */
	public Double distance(Shape s) {
		return distance(s.getCenter().getX(), s.getCenter().getY());
	}

	public Point2D computeMean() {
		double sumx = 0;
		double sumy = 0;
		for (Point2D point : points) {
			sumx += point.getX();
			sumy += point.getY();
		}

		double cx = sumx / points.size();
		double cy = sumy / points.size();

		Point2D p = new Point2D(cx, cy);
		return p;
	}

	/*
	 * Calculate distance between center of
	 * this shape and (x,y).
	 */
	public Double distance(double xx, double yy) {
		double dx = center.getX() - xx;
		double dy = center.getY() - yy;
		return Math.sqrt(dx * dx + dy * dy);
	}

	/* 
	 * Return true if (xx,yy) is inside this shape.
	 */
	public Boolean inside(double xx, double yy) {
		return null;
	}

	public void extractPoints() {
		if (path != null) {
			for (PathElement pe : path.getElements()) {
				if (pe instanceof MoveTo) {
					Point2D p = new Point2D(((MoveTo) pe).getX(), ((MoveTo) pe).getY());
					points.add(p);
				} else if (pe instanceof LineTo) {
					Point2D p = new Point2D(((LineTo) pe).getX(), ((LineTo) pe).getY());
					points.add(p);
				} else if (pe instanceof ClosePath) {

				}
			}
		}
	}

	public Double getRmsResidual() {
		return rmsResidual;
	}

	public void setRmsResidual(Double rms) {
		this.rmsResidual = rms;
	}

	public void findMaxima() {
		xMin = Double.MAX_VALUE;
		xMax = Double.MIN_VALUE;
		yMin = Double.MAX_VALUE;
		yMax = Double.MIN_VALUE;

		for (Point2D p : points) {
			xMin = Math.min(xMin, p.getX());
			xMax = Math.max(xMax, p.getX());
			yMin = Math.min(yMin, p.getY());
			yMax = Math.max(yMax, p.getY());
		}
	}

	public Double getxMin() {
		return xMin;
	}

	public void setxMin(Double xMin) {
		this.xMin = xMin;
	}

	public Double getxMax() {
		return xMax;
	}

	public void setxMax(Double xMax) {
		this.xMax = xMax;
	}

	public Double getyMin() {
		return yMin;
	}

	public void setyMin(Double yMin) {
		this.yMin = yMin;
	}

	public Double getyMax() {
		return yMax;
	}

	public void setyMax(Double yMax) {
		this.yMax = yMax;
	}

	public void dumpPath() {
		for (PathElement pe : path.getElements()) {
			if (pe instanceof MoveTo) {
				MoveTo mt = (MoveTo) pe;
				logger.debug("move to " + mt.getX() + ", " + mt.getY());
			} else if (pe instanceof LineTo) {
				LineTo lt = (LineTo) pe;
				logger.debug("line to " + lt.getX() + ", " + lt.getY());
			} else if (pe instanceof ClosePath) {
				logger.debug("close path");
			} else {
				logger.debug("unknown path element");
			}
		}
	}

}
