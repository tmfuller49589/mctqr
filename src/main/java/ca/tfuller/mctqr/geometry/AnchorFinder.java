package ca.tfuller.mctqr.geometry;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.jfx.view.MainController;
import ca.tfuller.mctqr.mctest.McTestAbstract;
import ca.tfuller.mctqr.mctest.QuestionAbstract;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.shape.Path;

public class AnchorFinder {
	protected static final Logger logger = LogManager.getLogger(AnchorFinder.class);

	private List<Square> anchors;
	private List<Path> paths;
	private final double imageWidth;

	public AnchorFinder(List<Path> paths, final Image image) {
		this.paths = paths;
		this.imageWidth = Math.min(image.getWidth(), image.getHeight());
	}

	/*
	 * Get the scaling factor necessary to scale the anchors from a scanned test
	 * to the anchor positions as generated.
	 * Measure the max width from left anchors to right anchors.
	 */
	public double getScalingFactorFromAnchors(McTestAbstract<? extends QuestionAbstract> mcTest) {
		double anchorWidth = getLeftAnchorToRightAnchorWidth();
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;

		for (Point2D point : mcTest.getAnchors()) {
			min = Math.min(point.getX(), min);
			max = Math.max(point.getX(), max);
		}

		double aw = max - min;
		double scale = anchorWidth / aw;
		return scale;
	}

	public double getLeftAnchorToRightAnchorWidth() {
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;

		for (Square s : anchors) {
			min = Math.min(s.getCenter().getX(), min);
			max = Math.max(s.getCenter().getX(), max);
		}
		return max - min;
	}

	public double getAnchorSize() {
		double sumSize = 0d;
		for (Square s : anchors) {
			sumSize += s.getSize();
		}
		double meanSize = sumSize / anchors.size();
		return meanSize;
	}

	public void findAnchors() {
		anchors = new ArrayList<Square>();

		List<Square> squares = new ArrayList<Square>();
		List<Square> candidateAnchors = new ArrayList<Square>();

		for (Path p : paths) {
			Square s = new Square(p);
			logger.debug("considering square");
			s.dump();
			if (s.isSquare()) {

				logger.debug("found a square, checking size");
				if (checkSize(s)) {
					candidateAnchors.add(s);
					logger.debug("found a square, size is good");
				} else {
					// found a square,
					// could be an interior anchor square
					// save it for later
					squares.add(s);
				}
			}
		}
		logger.debug("found " + candidateAnchors.size() + " candidate anchors ");

		// check for smaller square inside anchor
		int ccCounter = 0;
		for (Square anc : candidateAnchors) {
			for (Square s : squares) {
				double centerDiff = (anc.getCenter().subtract(s.getCenter())).magnitude()
						/ (0.5 * (anc.getCenter().add(s.getCenter()).magnitude()));
				if (centerDiff < 0.05) {
					++ccCounter;

					// centers match, compare sizes
					// large square has 1:1:6:1:1 = 10
					// inside square is just 6
					double computedSmallerSize = 6d / 10d * anc.getSize();
					double sizediff = (s.getSize() - computedSmallerSize) / computedSmallerSize;
					logger.debug("found concentric square " + ccCounter + " smallerSquareSize = " + s.getSize()
							+ " computedSmallerSize is " + computedSmallerSize + " sizeDiff is " + sizediff);

					if (sizediff < 0.05 && anc.getSize() > s.getSize()) {
						logger.debug("found interior smaller square");
						anchors.add(anc);
					}
				}
			}
		}

		logger.debug("found " + anchors.size() + " confirmed anchors");
	}

	private boolean checkSize(Square s) {
		double anchorSizePx = MainController.getPreferences().getAnchorSize();
		double scaledAnchorSize = anchorSizePx / (612 * 2) * imageWidth;

		logger.debug("anchorSizePx = " + anchorSizePx + " imageWidth = " + imageWidth);
		double diffFactor = Math.abs(s.getSize() - scaledAnchorSize) / scaledAnchorSize;
		logger.debug(
				"size is " + s.getSize() + " scaledAnchorSize is " + scaledAnchorSize + " diffFactor is " + diffFactor);
		if (diffFactor < 0.05) {
			return true;
		}
		return false;
	}

	public List<Path> getPaths() {
		return paths;
	}

	public void setPaths(List<Path> paths) {
		this.paths = paths;
	}

	public List<Square> getAnchors() {
		return anchors;
	}

	public void setAnchors(List<Square> anchors) {
		this.anchors = anchors;
	}

	public void dump() {
		logger.debug("-------------------------------------------------------------");
		logger.debug("dumping " + anchors.size() + " anchors");
		for (Square s : anchors) {
			s.dump();
		}
		logger.debug("-------------------------------------------------------------");
	}
}
