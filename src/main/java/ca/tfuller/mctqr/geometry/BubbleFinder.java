package ca.tfuller.mctqr.geometry;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.imageregistration.ImReg;
import ca.tfuller.mctqr.jfx.view.MainController;
import ca.tfuller.mctqr.mctest.Bubble;
import ca.tfuller.mctqr.mctest.McTestStudent;
import ca.tfuller.mctqr.mctest.QuestionAbstract;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.shape.Path;

// not currently in use
// generated circle is sufficiently accurate (so far)
// after scanned image is registered to master image of mc test
public class BubbleFinder {

	protected static final Logger logger = LogManager.getLogger(BubbleFinder.class);
	private final ImReg imReg;
	private final McTestStudent mcTestStudent;
	private final double squareThreshold = 0.40d; // max deviation from perfect square
	private final double bubbleSizeThreshold = 0.40d; // max deviation from estimated
	private final List<Path> bubblePaths = new ArrayList<Path>();
	private final double rmsResidualThreshold = 200d;

	public BubbleFinder(ImReg imReg, McTestStudent mcTest) {
		this.imReg = imReg;
		this.mcTestStudent = mcTest;
	}

	public void dumpBubbleMatch() {
		logger.debug("singularity counter is " + CircleFit.getSingularityCounter());
		logger.debug("solver counter is " + CircleFit.getSolverCounter());
		for (Entry<Integer, ? extends QuestionAbstract> entry : mcTestStudent.getQuestions().entrySet()) {
			QuestionAbstract q = entry.getValue();
			for (Bubble b : q.getBubbles()) {
				logger.debug("Question " + q.getQuestionNumber() + " bubble " + b.getChoice().toString());
				logger.debug("Question " + q.getQuestionNumber() + " generated circle is " + b.getGeneratedCircle());
				logger.debug("Question " + q.getQuestionNumber() + " generated circle center is "
						+ b.getGeneratedCircle().getCenter());
				logger.debug("Question " + q.getQuestionNumber() + " scannedCircle center is "
						+ b.getScannedCircle().getCenter());
				if (b.getScannedCircle() != null && b.getScannedCircle().getCenter() != null) {
					double dist = b.getGeneratedCircle().getCenter().distance(b.getScannedCircle().getCenter());
					logger.debug("dist is " + dist);
				} else {
					logger.debug("scanned circle is null");

				}
			}
		}
	}

	public void matchScannedBubblesToQuestions() {
		logger.debug("mcTest is " + mcTestStudent);
		logger.debug("questions is " + mcTestStudent.getQuestions());
		addBubbles();

		List<Path> tempBubblePath = new ArrayList<Path>();
		tempBubblePath.addAll(bubblePaths);
		logger.debug("tempBubblePath size is " + tempBubblePath.size());

		for (Entry<Integer, ? extends QuestionAbstract> entry : mcTestStudent.getQuestions().entrySet()) {
			QuestionAbstract q = entry.getValue();

			logger.debug("question " + q.getQuestionNumber());
			for (Bubble b : q.getBubbles()) {

				logger.debug("bubble location " + b.getGeneratedCircle().getCenter().getX() + ", "
						+ b.getGeneratedCircle().getCenter().getY());
				double minDist = Double.MAX_VALUE;
				Path minPath = null;
				for (Path path : tempBubblePath) {
					//logger.debug("path.getBoundsInLocal() is " + path.getBoundsInLocal());
					double dist = dist(path.getBoundsInLocal(), b.getGeneratedCircle().getCenter());
					if (dist < minDist) {
						minPath = path;
						minDist = dist;
					}
				}
				Circle circ = new Circle(minPath);
				logger.debug(
						"min dist is " + minDist + " closest point is " + circ.getCenter() + " minpath is " + minPath);

				b.setScannedCircle(circ);
				tempBubblePath.remove(minPath);

			}
		}
	}

	public List<Path> getBubblePaths() {
		return bubblePaths;
	}

	private void addBubbles() {
		bubblePaths.clear();
		logger.debug("imReg.getContourLoops().size() " + imReg.getContourLoops().size());
		List<Path> contourLoops = new ArrayList<Path>();
		contourLoops.addAll(imReg.getContourLoops());
		//removeDuplicateBubbles(contourLoops);
		for (Path path : contourLoops) {
			addPathIfBubble(path);
		}
	}

	private void addPathIfBubble(Path newPath) {

		// TODO need to handle zoom consistently
		float bubbleSizeImage = MainController.getPreferences().getBubbleSize() * 2;

		Rectangle2D rect = Geometry.getBounds(newPath);
		double size = 0.5 * (rect.getWidth() + rect.getHeight());
		double squareness = Math.abs((rect.getWidth() - rect.getHeight()) / size);
		double bubbleSizeDeviation = Math.abs(bubbleSizeImage - size) / bubbleSizeImage;

		// check that potential path is inside anchor region
		if (Geometry.getCenterX(rect) < imReg.getLeftAnchor() || Geometry.getCenterX(rect) > imReg.getRightAnchor()
				|| Geometry.getCenterY(rect) < imReg.getTopAnchor()
				|| Geometry.getCenterY(rect) > imReg.getBottomAnchor()) {
			//logger.debug("candidate bubble is outside of anchor region");
			return;
		}

		Circle circ = new Circle();
		circ.setPath(newPath);
		CircleFit cf = new CircleFit(circ);
		cf.fit();

		logger.debug(Geometry.getCenterString(rect) + " squareness = " + squareness + " rect size = " + size
				+ " bubbleSizeDeviation = " + bubbleSizeDeviation + " bubbleSizeImage = " + bubbleSizeImage
				+ " circRmsResidual = " + circ.getRmsResidual());

		if ((squareness < squareThreshold) && (bubbleSizeDeviation < bubbleSizeThreshold)
				&& circ.getRmsResidual() < rmsResidualThreshold) {
			bubblePaths.add(newPath);
			logger.debug(" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
			logger.debug(Geometry.getCenterX(rect) + ", " + Geometry.getCenterY(rect) + " squareness = " + squareness
					+ " rect size = " + size + " bubbleSizeDeviation = " + bubbleSizeDeviation + " bubbleSizeImage = "
					+ bubbleSizeImage + " resid " + circ.getRmsResidual());
			logger.debug("paths size is " + bubblePaths.size());
			logger.debug(" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
		}
	}

	private void removeDuplicateBubbles(List<Path> paths) {
		List<Path> deletePaths = new ArrayList<Path>();
		logger.debug("before removing duplicates paths.size is " + paths.size());
		for (int i = 0; i < paths.size() - 1; ++i) {
			for (int j = i + 1; j < paths.size(); ++j) {
				Rectangle2D recti = Geometry.getBounds(paths.get(i));
				Rectangle2D rectj = Geometry.getBounds(paths.get(j));
				double sizeDiff = Math.abs((Geometry.getSize(recti) - Geometry.getSize(rectj))
						/ (0.5 * Geometry.getSize(recti) + Geometry.getSize(rectj)));

				if (sizeDiff < 0.15 && (Geometry.rectContains(recti, rectj) || Geometry.rectContains(recti, rectj))) {
					logger.debug("found duplicate");
					// keep the larger
					if (recti.getWidth() > rectj.getWidth()) {
						deletePaths.add(paths.get(j));
					} else {
						deletePaths.add(paths.get(i));
					}
				}
			}
		}

		paths.removeAll(deletePaths);
		logger.debug("after removing duplicates paths.size is " + paths.size());
	}

	double dist(Bounds b, Point2D p) {
		double dx = (b.getCenterX() - p.getX());
		double dy = (b.getCenterY() - p.getY());
		return Math.sqrt(dx * dx + dy * dy);
	}

}
