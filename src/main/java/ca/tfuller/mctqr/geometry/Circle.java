package ca.tfuller.mctqr.geometry;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.shape.Path;

public class Circle extends Shape implements Serializable {
	private static final long serialVersionUID = 1L;

	protected static final Logger logger = LogManager.getLogger(Circle.class);

	private Double r;

	public Circle(Double x, Double y, Double r, Path path) {
		super();
		this.r = r;
		center = new Point2D(x, y);
		this.path = path;
		extractPoints();
	}

	public Circle() {
	}

	public Circle(Path path) {
		this.path = path;
		extractPoints();
		CircleFit cf = new CircleFit(this);
		cf.fit();

	}

	public int getPixelCount() {
		int pxCount = 0;
		double xmin = Math.floor(center.getX() - r);
		double xmax = Math.ceil(center.getX() + r);
		double ymin = Math.floor(center.getY() - r);
		double ymax = Math.ceil(center.getX() + r);

		logger.debug("xmin = " + xmin);
		logger.debug("xmax = " + xmax);
		logger.debug("ymin = " + ymin);
		logger.debug("ymax = " + ymax);

		for (double x = xmin; x <= xmax; ++x) {
			for (double y = ymin; y <= ymax; ++y) {
				if (path.contains(x, y)) {
					logger.debug("adding " + x + " " + y + " " + Math.sqrt(x * x + y * y));
					++pxCount;
				} else {
					logger.debug("no add " + x + " " + y + " " + Math.sqrt(x * x + y * y));
				}
			}
		}
		return pxCount;
	}

	public double getBlackFraction(Image image, double interiorRadius) {

		PixelReader pxReader = image.getPixelReader();

		double pxSum = 0d;
		int pxCount = 0;

		for (double px = center.getX() - interiorRadius; px <= center.getX() + interiorRadius; ++px) {
			for (double py = center.getY() - interiorRadius; py <= center.getY() + interiorRadius; ++py) {
				Point2D point = new Point2D(px, py);
				if (point.distance(center) <= interiorRadius) {
					pxSum += pxReader.getColor((int) px, (int) py).getBrightness();
					++pxCount;
				}
			}
		}
		double frac = pxSum / pxCount;
		return frac;
	}

	public Double getR() {
		return r;
	}

	public void setR(Double r) {
		this.r = r;
	}

	/* 
	 * Return true if (xx,yy) is inside this circle.
	 */
	@Override
	public Boolean inside(double xx, double yy) {
		double dist = distance(xx, yy);
		if (dist <= r) {
			return true;
		}
		return false;
	}

	@Override
	public Double getRmsResidual() {
		return rmsResidual;
	}

	@Override
	public void setRmsResidual(Double rms) {
		this.rmsResidual = rms;
	}

}
