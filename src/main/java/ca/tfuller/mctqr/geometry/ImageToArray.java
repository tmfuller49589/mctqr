package ca.tfuller.mctqr.geometry;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;

public class ImageToArray {

	/*
	 *  Convert image into a double[][] array
	 *  Values range from 0-1 in brightness.
	 */
	public static double[][] convert(Image image) {

		int rows = (int) image.getHeight();
		int cols = (int) image.getWidth();
		double[][] data = new double[rows][cols];

		PixelReader pxReader = image.getPixelReader();

		for (int r = 0; r < rows; ++r) {
			for (int c = 0; c < cols; ++c) {
				data[r][c] = pxReader.getColor(c, r).getBrightness();
			}
		}

		return data;
	}
}
