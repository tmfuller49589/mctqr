package ca.tfuller.mctqr.geometry;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.QRDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.SingularMatrixException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.geometry.Point2D;

// https://dtcenter.org/met/users/docs/write_ups/circle_fit.pdf
// for instructions on circle fitting

public class CircleFit {
	protected static final Logger logger = LogManager.getLogger(CircleFit.class);

	private final Circle circle;
	private final List<Point2D> transformedPoints = new ArrayList<Point2D>();

	private int N;

	private Point2D mean;

	private Double Su, Suu, Suuu;
	private Double Sv, Svv, Svvv;
	private Double Suvv, Svuu, Suv;
	private static Integer singularityCounter = 0;
	private static Integer solverCounter = 0;

	public CircleFit(Circle circle) {
		this.circle = circle;
	}

	public void fit() {
		N = circle.getPoints().size();
		mean = circle.computeMean();

		transposeToMean();
		computeSums();
		solveLinearSystem();
		computeResiduals();
	}

	/* Solve linear system AX = B
	 * 
	 * A is
	 * N   Suv
	 * Suv Svv
	 *
	 * X is circle centers
	 * uc
	 * vc
	 * 
	 * B is
	 * 0.5 * (Suuu + Suvv)
	 * 0.5 * (Svvv + Svuu)
	 * 
	 */

	private void solveLinearSystem() {
		RealMatrix A = MatrixUtils.createRealMatrix(2, 2);
		RealMatrix B = MatrixUtils.createRealMatrix(2, 1);
		A.setEntry(0, 0, transformedPoints.size());
		A.setEntry(0, 1, Suv);
		A.setEntry(1, 0, Suv);
		A.setEntry(1, 1, Svv);
		B.setEntry(0, 0, 0.5 * (Suuu + Suvv));
		B.setEntry(1, 0, 0.5 * (Svvv + Svuu));

		DecompositionSolver solver = new QRDecomposition(A).getSolver();
		RealMatrix X;
		++solverCounter;
		try {
			X = solver.solve(B);
		} catch (SingularMatrixException e) {
			++singularityCounter;
			return;
		}

		Double uc = X.getEntry(0, 0);
		Double vc = X.getEntry(1, 0);

		Double xc = uc + mean.getX();
		Double yc = vc + mean.getY();

		Double alpha = uc * uc + vc * vc + (Suu + Svv) / N;
		Double R = Math.sqrt(alpha);

		circle.setR(R);
		Point2D center = new Point2D(xc, yc);
		circle.setCenter(center);
	}

	public static Integer getSingularityCounter() {
		return singularityCounter;
	}

	public static Integer getSolverCounter() {
		return solverCounter;
	}

	public void dump() {
		logger.debug("points");
		dumpPoints(circle.getPoints());
		logger.debug("transformed points");
		dumpPoints(transformedPoints);

		logger.debug("meanx = " + mean.getX());
		logger.debug("meany = " + mean.getY());

		logger.debug("Su = " + Su);
		logger.debug("Suu = " + Suu);
		logger.debug("Suuu = " + Suuu);
		logger.debug("Sv = " + Sv);
		logger.debug("Svv = " + Svv);
		logger.debug("Svvv = " + Svvv);

		logger.debug("Suv = " + Suv);
		logger.debug("Suvv = " + Suvv);
		logger.debug("Svuu = " + Svuu);

		logger.debug("circle center x: " + circle.getCenter().getX());
		logger.debug("circle center y: " + circle.getCenter().getY());
		logger.debug("circle radius: " + circle.getR());

		logger.debug("fit residuals: " + circle.getRmsResidual());
	}

	public void dumpPoints(List<Point2D> points) {
		for (Point2D point : points) {
			logger.debug(point);
		}
	}

	public void dump(List<Double> list) {
		String s = "";
		for (Double f : list) {
			s += f + " ";
		}
		logger.debug(s);
	}

	/*
	 * Compute the RMS of the difference
	 * between the distance from the circle center and 
	 * the radius of the circle.
	 */
	private void computeResiduals() {
		Double sum = 0d;
		for (Point2D point : circle.getPoints()) {
			Double x = point.getX();
			Double y = point.getY();
			Double dx = x - circle.getCenter().getX();
			Double dy = y - circle.getCenter().getY();
			Double dist = Math.sqrt(dx * dx + dy * dy);
			Double dRsq = Math.pow(circle.getR() - dist, 2);
			sum += dRsq;
		}

		Double meanTmp = sum / circle.getPoints().size();
		Double rms = Math.sqrt(meanTmp);
		circle.setRmsResidual(rms);

	}

	private void computeSums() {
		Su = 0d;
		Suu = 0d;
		Suuu = 0d;
		Sv = 0d;
		Svv = 0d;
		Svvv = 0d;
		Suv = 0d;
		Suvv = 0d;
		Svuu = 0d;

		for (Point2D point : transformedPoints) {

			Double u = point.getX();
			Double v = point.getY();

			Su += u;
			Suu += u * u;
			Suuu += u * u * u;

			Sv += v;
			Svv += v * v;
			Svvv += v * v * v;

			Suv += u * v;
			Suvv += u * v * v;
			Svuu += v * u * u;
		}
	}

	private void transposeToMean() {
		transformedPoints.clear();

		for (Point2D point : circle.getPoints()) {
			Point2D tp = point.subtract(mean);
			transformedPoints.add(tp);
		}
	}

}
