package ca.tfuller.mctqr.geometry;

import java.text.NumberFormat;
import java.util.Locale;

import javafx.geometry.Rectangle2D;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;

public class Geometry {
	public static Rectangle2D getBounds(Path path) {
		double minx = Double.MAX_VALUE;
		double maxx = Double.MIN_VALUE;
		double miny = minx;
		double maxy = maxx;

		double x = 0;
		double y = 0;
		for (PathElement pe : path.getElements()) {
			if (pe instanceof LineTo) {
				LineTo lt = (LineTo) pe;
				x = lt.getX();
				y = lt.getY();
			} else if (pe instanceof MoveTo) {
				MoveTo mt = (MoveTo) pe;
				x = mt.getX();
				y = mt.getY();

			}
			minx = Math.min(x, minx);
			maxx = Math.max(x, maxx);
			miny = Math.min(y, miny);
			maxy = Math.max(y, maxy);
		}
		Rectangle2D rect = new Rectangle2D(minx, miny, maxx - minx, maxy - miny);
		return rect;
	}

	public static double getBoundsArea(Path path) {
		Rectangle2D rect = getBounds(path);
		return rect.getHeight() * rect.getWidth();
	}

	public static double dist(double x1, double y1, double x2, double y2) {
		return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}

	public static double getCenterX(Rectangle2D rect) {
		return (rect.getMaxX() + rect.getMinX()) / 2d;
	}

	public static double getCenterY(Rectangle2D rect) {
		return (rect.getMaxY() + rect.getMinY()) / 2d;
	}

	public static boolean rectContains(Rectangle2D out, Rectangle2D in) {
		if (out.getMinX() <= in.getMinX() && out.getMaxX() >= in.getMaxX() && out.getMinY() <= in.getMinY()
				&& out.getMaxY() >= in.getMaxY()) {
			return true;
		}
		return false;
	}

	public static String getCenterString(Rectangle2D rect) {
		NumberFormat nf = NumberFormat.getInstance(Locale.CANADA);
		nf.setMaximumFractionDigits(1);
		nf.setMinimumFractionDigits(1);
		String str = nf.format(Geometry.getCenterX(rect)) + ", " + nf.format(Geometry.getCenterY(rect));
		return str;
	}

	public static double getSize(Rectangle2D rect) {
		return (rect.getHeight() + rect.getWidth()) * 0.5;
	}
}
