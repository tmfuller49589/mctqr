package ca.tfuller.mctqr.geometry;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.jfx.view.MessageWindow;
import javafx.geometry.Point2D;
import javafx.scene.shape.Path;

public class Square extends Shape {
	protected static final Logger logger = LogManager.getLogger(Square.class);

	Point2D topLeft;
	Point2D topRight;
	Point2D bottomLeft;
	Point2D bottomRight;

	Double threshold = 0.10;
	Double bottomY = null;
	Double topY = null;
	Double rightX = null;
	Double leftX = null;

	Double sidesPlumbFactor;
	Double angleBetweenDiagonals;
	Double bisectorDiagFactor;
	Double diagonalDiffNormalized;

	public Square(Double x, Double y, Path path) {
		super();
		center = new Point2D(x, y);
		this.path = path;
		extractPoints();
	}

	public Square() {
		super();
	}

	public Square(Path p) {
		super(p);
		extractPoints();
		Point2D mean = computeMean();
		setCenter(mean);

	}

	/* 
	 * Return true if (xx,yy) is inside this circle.
	 */
	@Override
	public Boolean inside(double xx, double yy) {
		if (xMin <= xx && xx <= xMax && yMin <= yy && yy <= yMax) {
			return true;
		}
		return false;
	}

	private boolean findCorners() {
		// split points into 4 quadrants
		List<Point2D> tl = new ArrayList<Point2D>();
		List<Point2D> tr = new ArrayList<Point2D>();
		List<Point2D> bl = new ArrayList<Point2D>();
		List<Point2D> br = new ArrayList<Point2D>();

		for (Point2D point : points) {
			if (point.getX() <= center.getX() && point.getY() <= center.getY()) {
				tl.add(point);
			} else if (point.getX() > center.getX() && point.getY() <= center.getY()) {
				tr.add(point);
			} else if (point.getX() <= center.getX() && point.getY() > center.getY()) {
				bl.add(point);
			} else if (point.getX() > center.getX() && point.getY() > center.getY()) {
				br.add(point);
			}
		}

		if (tl.size() == 0) {
			logger.debug("error tl size: " + tl.size() + " " + points.size() + " points in square");
			return false;
		}
		if (tr.size() == 0) {
			logger.debug("error tr size:" + tr.size() + " " + points.size() + " points in square");
			return false;
		}
		if (bl.size() == 0) {
			logger.debug("error bl size:" + bl.size() + " " + points.size() + " points in square");
			return false;
		}
		if (br.size() == 0) {
			logger.debug("error br size:" + br.size() + " " + points.size() + " points in square");
			return false;
		}

		// find longest line from tl to br
		topLeft = null;
		bottomRight = null;
		Double maxDistTlBr = Double.MIN_VALUE;
		for (Point2D p1 : tl) {
			for (Point2D p2 : br) {
				double dist = p1.distance(p2);
				if (dist > maxDistTlBr) {
					topLeft = p1;
					bottomRight = p2;
					maxDistTlBr = dist;
				}
			}
		}

		// find longest line from tr to bl
		topRight = null;
		bottomLeft = null;
		Double maxDistTrBl = Double.MIN_VALUE;
		for (Point2D p1 : tr) {
			for (Point2D p2 : bl) {
				double dist = p1.distance(p2);
				if (dist > maxDistTrBl) {
					topRight = p1;
					bottomLeft = p2;
					maxDistTrBl = dist;
				}
			}
		}

		if (topLeft == null || topRight == null || bottomLeft == null || bottomRight == null) {
			logger.debug("error finding corners");
			return false;
		} else {
			logger.debug("tl-rl diagonal: " + topLeft.toString() + " to " + bottomRight.toString() + " distance = "
					+ maxDistTlBr);
			logger.debug("tr-bl diagonal: " + topRight.toString() + " to " + bottomLeft.toString() + " distance = "
					+ maxDistTrBl);
		}

		return true;
	}

	private void computeDiagonalDifferenceNormalized() {
		Double d1 = topLeft.subtract(bottomRight).magnitude();
		Double d2 = topRight.subtract(bottomLeft).magnitude();

		diagonalDiffNormalized = Math.abs(d1 - d2) / (0.5 * (d1 + d2));
		logger.debug("normalized difference between diagonal lengths = " + diagonalDiffNormalized);

	}

	private void computeAngleBetweenDiagonals() {
		// compute angle between diags
		Point2D d1 = topLeft.subtract(bottomRight);
		Point2D d2 = topRight.subtract(bottomLeft);
		Double scalarProduct = d1.dotProduct(d2);
		angleBetweenDiagonals = Math.acos(scalarProduct / (d1.magnitude() * d2.magnitude()));

		logger.debug("angle between diagonals is " + angleBetweenDiagonals * 180d / Math.PI + " degrees.");
	}

	private Double computeHorizontalAngle(Point2D p1, Point2D p2) {
		Double adj = Math.abs(p2.getX() - p1.getX());
		Double opp = Math.abs(p2.getY() - p1.getY());

		Double angle = Math.atan2(opp, adj);
		return angle;
	}

	private Double computeVerticalAngle(Point2D p1, Point2D p2) {
		Double opp = Math.abs(p2.getX() - p1.getX());
		Double adj = Math.abs(p2.getY() - p1.getY());

		Double angle = Math.atan2(opp, adj);
		return angle;
	}

	/*
	 * Compute the angular deviation of the left and right sides from vertical.  Square the deviation.
	 * Compute the angular deviation of the top and bottom sides from horizontal. Square the deviation.
	 * Take the mean of the squares of the deviations.
	 * Take the root of the mean.
	 */
	private void computeSidesPlumbFactor() {
		Double sum = 0d;
		Double angle;

		angle = (computeVerticalAngle(bottomLeft, topLeft)); // left side
		logger.debug("bottom left is " + bottomLeft);
		logger.debug("top left is " + topLeft);

		logger.debug("left side angle is " + angle * 180d / Math.PI);
		sum += angle * angle;
		angle = (computeHorizontalAngle(topLeft, topRight)); // top side
		logger.debug("top side angle is " + angle * 180d / Math.PI);
		sum += angle * angle;
		angle = (computeVerticalAngle(topRight, bottomRight)); // right side 
		logger.debug("right side angle is " + angle * 180d / Math.PI);
		sum += angle * angle;
		angle = (computeHorizontalAngle(bottomRight, bottomLeft)); // bottom side
		logger.debug("bottom side angle is " + angle * 180d / Math.PI);
		sum += angle * angle;

		logger.debug("sides plumb factor is " + sum);

		sidesPlumbFactor = Math.sqrt(sum / 4d);

	}

	/*
	 * Compute the mean of the horizontal and vertical bisectors of the square.
	 * Scale the mean bisector length by sqrt(2) to get it to diagonal length.
	 * Difference the avg diagonal length and the scaled bisector length and 
	 * normalize by the average of the two.
	 */
	private void computeDiagonalsHVBisectorsFactor() {
		Double width = rightX - leftX;
		Double height = bottomY - topY;
		Double avgBisectorLength = 0.5 * (width + height);

		Double d1 = topLeft.subtract(bottomRight).magnitude();
		Double d2 = topRight.subtract(bottomLeft).magnitude();
		Double avgDiagonalLength = 0.5 * (d1 + d2);

		// scale bisector length to root 2 to compare with diagonal
		Double avgBisectorLegthScaled = avgBisectorLength * Math.sqrt(2);

		// compute the 
		bisectorDiagFactor = Math.abs(avgBisectorLegthScaled - avgDiagonalLength)
				/ (0.5 * (avgBisectorLegthScaled + avgDiagonalLength));
		logger.debug("comparison between HV bisectors and diagonals: " + bisectorDiagFactor);

	}

	public Boolean isSquare() {
		computeSquareSidesResiduals();
		if (!findCorners()) {
			return false;
		}
		computeAngleBetweenDiagonals();
		computeDiagonalDifferenceNormalized();
		computeDiagonalsHVBisectorsFactor();
		computeSidesPlumbFactor();

		if (angleBetweenDiagonals > 1.9) {
			return false;
		} else if (bisectorDiagFactor > 0.12) {
			return false;
		} else if (diagonalDiffNormalized > 0.12) {
			return false;
		} else if (rmsResidual > 2.4) {
			return false;
		} else if (sidesPlumbFactor > 0.13) {
			return false;
		}

		return true;
	}

	private void computeSquareSidesResiduals() {
		findMaxima();

		List<Double> xl = new ArrayList<Double>();
		List<Double> yl = new ArrayList<Double>();
		List<Double> xt = new ArrayList<Double>();
		List<Double> yt = new ArrayList<Double>();
		List<Double> xr = new ArrayList<Double>();
		List<Double> yr = new ArrayList<Double>();
		List<Double> xb = new ArrayList<Double>();
		List<Double> yb = new ArrayList<Double>();

		Double sumR = 0d;
		Double sumL = 0d;
		Double sumT = 0d;
		Double sumB = 0d;

		// This loop groups each point in the path according to what side of the
		// square
		// the point is closest to, assuming the square is defined by xmin,ymin
		// -- xmax,ymax
		// Take the average of the x positions for the points on the left side.
		// Take the average of the x positions for the points on the right side.
		// Take the average of the y positions for the points on the top side.
		// Take the average of the y positions for the points on the bottom
		// side.
		for (Point2D point : points) {

			Double xx = point.getX();
			Double yy = point.getY();

			Double distToLeft = Math.abs(xx - xMin);
			Double distToRight = Math.abs(xx - xMax);
			Double distToTop = Math.abs(yy - yMin);
			Double distToBottom = Math.abs(yy - yMax);
			Double closestSide = Double.MAX_VALUE;
			closestSide = Math.min(closestSide, distToLeft);
			closestSide = Math.min(closestSide, distToRight);
			closestSide = Math.min(closestSide, distToBottom);
			closestSide = Math.min(closestSide, distToTop);

			//	logger.debug("(" + point.getX() + ", " + point.getY() + ") " + closestSide + " " + distToLeft + " "
			//		+ distToRight + " " + distToTop + " " + distToBottom);

			if (closestSide.equals(distToLeft)) {
				xl.add(xx);
				yl.add(yy);
				sumL += xx;
			} else if (closestSide.equals(distToRight)) {
				xr.add(xx);
				yr.add(yy);
				sumR += xx;
			} else if (closestSide.equals(distToTop)) {
				xt.add(xx);
				yt.add(yy);
				sumT += yy;
			} else if (closestSide.equals(distToBottom)) {
				xb.add(xx);
				yb.add(yy);
				sumB += yy;
			} else {
				// should not get here
				logger.debug("closestSide = " + closestSide);
				logger.debug("distToLeft = " + distToLeft);
				logger.debug("distToRight = " + distToRight);
				logger.debug("distToTop = " + distToTop);
				logger.debug("distToBottom = " + distToBottom);

				if (closestSide.equals(distToRight)) {
					logger.debug("closest side is right");
				}
				MessageWindow.showError("error in Shape:isSquare() ");
			}
		}

		// use the means of the point positions to define the location values
		// for the sides
		leftX = sumL / xl.size();
		rightX = sumR / xr.size();
		topY = sumT / yt.size();
		bottomY = sumB / yb.size();

		topLeft = new Point2D(leftX, topY);
		topRight = new Point2D(rightX, topY);
		bottomLeft = new Point2D(leftX, bottomY);
		bottomRight = new Point2D(rightX, bottomY);

		logger.debug("left side location is " + leftX);
		logger.debug(" right side location is " + rightX);
		logger.debug(" top side location is " + topY);
		logger.debug(" bottom side location is " + bottomY);

		Double dss = 0d;
		int N = 0;
		dss += sumSquaredDiff(xl, leftX);
		N += xl.size();
		dss += sumSquaredDiff(xr, rightX);
		N += xr.size();
		dss += sumSquaredDiff(yt, topY);
		N += yt.size();
		dss += sumSquaredDiff(yb, bottomY);
		N += yb.size();

		rmsResidual = Math.sqrt(dss / N);
	}

	private Double sumSquaredDiff(List<Double> vals, Double meanval) {
		Double sumSquaredDiff = 0d;
		for (Double val : vals) {
			sumSquaredDiff += (val - meanval) * (val - meanval);
		}
		return sumSquaredDiff;
	}

	public Double getSidesPlumbFactor() {
		return sidesPlumbFactor;
	}

	public void setSidesPlumbFactor(Double sidesPlumbFactor) {
		this.sidesPlumbFactor = sidesPlumbFactor;
	}

	public Double getAngleBetweenDiagonals() {
		return angleBetweenDiagonals;
	}

	public void setAngleBetweenDiagonals(Double angleBetweenDiagonals) {
		this.angleBetweenDiagonals = angleBetweenDiagonals;
	}

	public Double getBisectorDiagFactor() {
		return bisectorDiagFactor;
	}

	public void setBisectorDiagFactor(Double bisectorDiagFactor) {
		this.bisectorDiagFactor = bisectorDiagFactor;
	}

	public Double getDiagonalDiffNormalized() {
		return diagonalDiffNormalized;
	}

	public void setDiagonalDiffNormalized(Double diagonalDiffNormalized) {
		this.diagonalDiffNormalized = diagonalDiffNormalized;
	}

	public void dump() {
		logger.debug("Square center: " + center);
		if (findCorners()) {
			logger.debug("h side length: " + (topRight.getX() - topLeft.getX()));
			logger.debug("v side length: " + (bottomRight.getY() - topRight.getY()));
		}
	}

	public Double getSize() {
		return 0.5 * (getHeight() + getWidth());
	}

	public Double getHeight() {
		Double w1 = topRight.getX() - topLeft.getX();
		Double w2 = bottomRight.getX() - bottomLeft.getX();
		return 0.5 * (w1 + w2);
	}

	public Double getWidth() {
		Double w1 = topRight.getX() - topLeft.getX();
		Double w2 = bottomRight.getX() - bottomLeft.getX();
		return 0.5 * (w1 + w2);
	}
}
