package ca.tfuller.mctqr.geometry;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;

public class PathUtils {

	protected static final Logger logger = LogManager.getLogger(PathUtils.class);

	public static List<Path> splitPaths(Path path) {
		List<Path> paths = new ArrayList<Path>();

		Path newPath = null;

		if (path.getElements().size() == 0) {
			logger.debug("oops, no path elements");
		}
		for (PathElement pe : path.getElements()) {
			if (pe instanceof MoveTo) {
				newPath = new Path();
				MoveTo mt = (MoveTo) pe;
				MoveTo mt1 = new MoveTo(mt.getX(), mt.getY());
				newPath.getElements().add(mt1);
			} else if (pe instanceof LineTo) {
				LineTo lt = (LineTo) pe;
				LineTo lt1 = new LineTo(lt.getX(), lt.getY());
				newPath.getElements().add(lt1);
			} else if (pe instanceof ClosePath) {
				ClosePath cp = new ClosePath();
				newPath.getElements().add(cp);
				paths.add(newPath);
				newPath = null;
			}
		}
		return paths;
	}
}
