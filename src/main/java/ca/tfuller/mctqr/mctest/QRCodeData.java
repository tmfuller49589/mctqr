package ca.tfuller.mctqr.mctest;

import java.time.LocalDate;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.utils.Utils;

public class QRCodeData {
	protected static final Logger logger = LogManager.getLogger(QRCodeData.class);

	UUID uuid;
	String testName;
	String courseId;
	String courseName;
	String instructor;
	String studentId;
	String studentName;
	LocalDate date;
	Integer numberOfQuestions;
	Integer choicesPerQuestion;
	Integer columns;
	Integer bubbleSize;
	Integer anchorSize;

	public QRCodeData(McTest mcTest) {
		super();
	}

	public QRCodeData(String str) {
		super();
		parse(str);
	}

	public void qrCodeToMcTest(McTest mcTest) {
		mcTest.getMcTestMetaData().setAnchorSize(anchorSize);
		mcTest.getMcTestMetaData().setBubbleSize(bubbleSize);
		mcTest.getMcTestMetaData().setChoicesPerQuestion(choicesPerQuestion);
		mcTest.getMcTestMetaData().setColumns(columns);
		mcTest.getMcTestMetaData().setCourseName(courseName);
		mcTest.getMcTestMetaData().setCourseId(courseId);
		mcTest.getMcTestMetaData().setDate(date);
		mcTest.getMcTestMetaData().setInstructor(instructor);
		mcTest.getMcTestMetaData().setNumberOfQuestions(numberOfQuestions);
		mcTest.getMcTestMetaData().setTestName(testName);
		mcTest.getMcTestMetaData().setTestUuid(uuid);
	}

	public void mcTestDescriptionToQrCode(McTest mcTest) {
		anchorSize = mcTest.getMcTestMetaData().getAnchorSize();
		bubbleSize = mcTest.getMcTestMetaData().getBubbleSize();
		choicesPerQuestion = mcTest.getMcTestMetaData().getChoicesPerQuestion();
		columns = mcTest.getMcTestMetaData().getColumns();
		courseId = mcTest.getMcTestMetaData().getCourseId();
		courseName = mcTest.getMcTestMetaData().getCourseName();
		date = mcTest.getMcTestMetaData().getDate();
		instructor = mcTest.getMcTestMetaData().getInstructor();
		numberOfQuestions = mcTest.getMcTestMetaData().getNumberOfQuestions();
		testName = mcTest.getMcTestMetaData().getTestName();
		uuid = mcTest.getMcTestMetaData().getTestUuid();
	}

	@Override
	public String toString() {
		String s = uuid + "|";
		s += testName + "|";
		s += courseId + "|";
		s += courseName + "|";
		s += instructor + "|";
		s += studentId + "|";
		s += studentName + "|";
		s += Utils.getDateString(date) + "|";
		s += numberOfQuestions + "|";
		s += choicesPerQuestion + "|";
		s += columns + "|";
		s += bubbleSize + "|";
		s += anchorSize;
		return s;
	}

	public void parse(String s) {
		String[] tokens = s.split("\\|");
		for (String ss : tokens) {
			logger.debug("token is -->" + ss + "<--");
		}
		int i = 0;
		uuid = UUID.fromString(tokens[i]);
		++i;
		testName = tokens[i];
		++i;
		courseId = tokens[i];
		++i;
		courseName = tokens[i];
		++i;
		instructor = tokens[i];
		++i;
		studentId = tokens[i];
		++i;
		studentName = tokens[i];
		++i;
		date = Utils.parseDate(tokens[i]);
		++i;
		numberOfQuestions = Integer.parseInt(tokens[i]);
		++i;
		choicesPerQuestion = Integer.parseInt(tokens[i]);
		++i;
		columns = Integer.parseInt(tokens[i]);
		++i;
		bubbleSize = Integer.parseInt(tokens[i]);
		++i;
		anchorSize = Integer.parseInt(tokens[i]);
		++i;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getInstructor() {
		return instructor;
	}

	public void setInstructor(String instructor) {
		this.instructor = instructor;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Integer getNumberOfQuestions() {
		return numberOfQuestions;
	}

	public void setNumberOfQuestions(Integer numberOfQuestions) {
		this.numberOfQuestions = numberOfQuestions;
	}

	public Integer getChoicesPerQuestion() {
		return choicesPerQuestion;
	}

	public void setChoicesPerQuestion(Integer choicesPerQuestion) {
		this.choicesPerQuestion = choicesPerQuestion;
	}

	public Integer getColumns() {
		return columns;
	}

	public void setColumns(Integer columns) {
		this.columns = columns;
	}

	public Integer getBubbleSize() {
		return bubbleSize;
	}

	public void setBubbleSize(Integer bubbleSize) {
		this.bubbleSize = bubbleSize;
	}

	public Integer getAnchorSize() {
		return anchorSize;
	}

	public void setAnchorSize(Integer anchorSize) {
		this.anchorSize = anchorSize;
	}

}
