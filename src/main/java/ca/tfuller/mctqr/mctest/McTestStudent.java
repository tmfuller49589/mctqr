package ca.tfuller.mctqr.mctest;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.api.services.classroom.model.Student;

import ca.tfuller.mctqr.utils.IdNameObject;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

public class McTestStudent extends McTestAbstract<QuestionStudent> {
	private final SimpleObjectProperty<IdNameObject<Student>> student = new SimpleObjectProperty<IdNameObject<Student>>();
	private final SimpleIntegerProperty numberCorrect = new SimpleIntegerProperty();
	private final Map<Integer, QuestionStudent> questions = new HashMap<Integer, QuestionStudent>();

	protected static final Logger logger = LogManager.getLogger(McTestStudent.class);

	public McTestStudent() {
	}

	@Override
	public Map<Integer, QuestionStudent> getQuestions() {
		return questions;
	}

	public void setStudent(IdNameObject<Student> student) {
		this.student.set(student);
	}

	public ObjectProperty<IdNameObject<Student>> studentProperty() {
		return student;
	}

	public IdNameObject<Student> getStudent() {
		return student.get();
	}

	public IntegerProperty numberCorrectProperty() {
		return numberCorrect;
	}

	public int getNumberCorrect() {
		return numberCorrect.get();
	}

	public void setNumberCorrect(int numberCorrect) {
		this.numberCorrect.set(numberCorrect);
	}
}
