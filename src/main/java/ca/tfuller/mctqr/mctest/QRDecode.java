package ca.tfuller.mctqr.mctest;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

public class QRDecode {
	protected static final Logger logger = LogManager.getLogger(QRDecode.class);

	public static String readQRCode(String filePath, String charset, Map<DecodeHintType, Object> hintMap)
			throws FileNotFoundException, IOException, NotFoundException {
		BinaryBitmap binaryBitmap = new BinaryBitmap(
				new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(new FileInputStream(filePath)))));
		Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap, hintMap);

		return qrCodeResult.getText();
	}

	public static void main(String[] args) {
		String charset = "UTF-8"; // or "ISO-8859-1"
		Map<DecodeHintType, Object> hintMap = new EnumMap<DecodeHintType, Object>(DecodeHintType.class);
		// hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		hintMap.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
		List<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
		hintMap.put(DecodeHintType.POSSIBLE_FORMATS, formats);
		// hintMap.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);

		File folder = new File("/home/tfuller/lang/eclipse/workspace/mcscan/src/main/resources");
		FileFilter fileFilter = new WildcardFileFilter("*.JPG", IOCase.INSENSITIVE);
		File[] listOfFiles = folder.listFiles(fileFilter);
		for (int i = 0; i < listOfFiles.length; i++) {
			try {
				logger.debug("File " + listOfFiles[i].getAbsolutePath());

				// String result = qrd.readQRCode("/var/www/caman/mcTest.jpg",
				// charset, hintMap);
				String result = QRDecode.readQRCode(listOfFiles[i].getAbsolutePath(), charset, hintMap);
				logger.debug(result);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				logger.debug("not found exception");
			}
		}

	}
}
