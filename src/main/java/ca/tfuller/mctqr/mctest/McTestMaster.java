package ca.tfuller.mctqr.mctest;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.geometry.Point2D;

public class McTestMaster extends McTestAbstract<QuestionMaster> {

	protected static final Logger logger = LogManager.getLogger(McTestMaster.class);
	private final Map<Integer, QuestionMaster> questions = new HashMap<Integer, QuestionMaster>();

	public McTestMaster() {
	}

	public void dump() {
		logger.debug("======================== dumping mc test ================================");
		for (Entry<Integer, QuestionMaster> entry : questions.entrySet()) {
			entry.getValue().dump();
		}

		for (Point2D p : anchors) {
			logger.debug("registration anchor: (" + p.getX() + ", " + p.getY() + ")");
		}
	}

	@Override
	public Map<Integer, QuestionMaster> getQuestions() {
		return questions;
	}
}
