package ca.tfuller.mctqr.mctest;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class QuestionMaster extends QuestionAbstract {
	private ChoiceEnum correctAnswer;

	protected static final Logger logger = LogManager.getLogger(QuestionMaster.class);

	public QuestionMaster() {
		super();
	}

	public QuestionMaster(ChoiceEnum correctAnswer) {
		super();
		this.correctAnswer = correctAnswer;
	}

	@Override
	public List<Bubble> getBubbles() {
		return bubbles;
	}

	public ChoiceEnum getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(ChoiceEnum correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public void dump() {
		logger.debug("question number " + questionNumber);
		logger.debug("correct answer " + correctAnswer);
		for (Bubble b : bubbles) {
			logger.debug(b.getChoice() + " " + b.getGeneratedCircle().getCenter().getX() + ", "
					+ b.getGeneratedCircle().getCenter().getY());
			if (b.getGeneratedCircle() != null) {
				logger.debug(b.getChoice() + " " + b.getGeneratedCircle().getCenter().getX() + ", "
						+ b.getGeneratedCircle().getCenter().getY());
			} else {
				logger.debug("no scanned bubble");
			}

		}
	}
}
