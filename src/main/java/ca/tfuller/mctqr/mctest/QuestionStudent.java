package ca.tfuller.mctqr.mctest;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class QuestionStudent extends QuestionAbstract {

	private Boolean correct;

	protected static final Logger logger = LogManager.getLogger(QuestionStudent.class);

	public QuestionStudent(QuestionMaster qm) {
		super();
		setQuestionNumber(qm.getQuestionNumber());
		for (Bubble bm : qm.getBubbles()) {
			Bubble b = new Bubble(bm);
			bubbles.add(b);
		}
	}

	public Boolean isCorrect(ChoiceEnum correctAnswer) {
		logger.debug("number of bubbles filled is " + getNumberBubblesFilled());

		if (getNumberBubblesFilled().compareTo(1) != 0) {
			correct = false;
			return false;
		}
		for (Bubble b : bubbles) {
			logger.debug("getFilled is " + b.getFilled() + " choice is " + b.getChoice() + " correctAnswer is "
					+ correctAnswer);
			if (b.getFilled() && b.getChoice().equals(correctAnswer)) {
				correct = true;
				return true;
			}
		}
		correct = false;
		return false;
	}

	public List<ChoiceEnum> getAnswers() {
		ArrayList<ChoiceEnum> answers = new ArrayList<ChoiceEnum>();
		for (Bubble b : bubbles) {
			if (b.getFilled()) {
				answers.add(b.getChoice());
			}
		}
		return answers;
	}

	public String getAnswersAsString() {
		List<ChoiceEnum> answers = getAnswers();
		String s = "";
		for (ChoiceEnum ce : answers) {
			s += ce.toString();
		}
		return s;
	}

	public Integer getNumberBubblesFilled() {
		int numFilled = 0;
		for (Bubble b : bubbles) {
			if (b.getFilled()) {
				++numFilled;
			}
		}
		return numFilled;
	}

	public void dump() {
		logger.debug("question number " + questionNumber);
		logger.debug("answer " + getAnswersAsString());
		logger.debug("number of bubbles is " + bubbles.size());
		for (Bubble b : bubbles) {
			logger.debug(b.getChoice() + " " + b.getGeneratedCircle().getCenter().getX() + ", "
					+ b.getGeneratedCircle().getCenter().getY());
			if (b.getGeneratedCircle() != null) {
				logger.debug(b.getChoice() + " " + b.getGeneratedCircle().getCenter().getX() + ", "
						+ b.getGeneratedCircle().getCenter().getY());
			} else {
				logger.debug("no scanned bubble");
			}
		}
	}

	public Boolean getCorrect() {
		return correct;
	}

	public void setCorrect(Boolean correct) {
		this.correct = correct;
	}

}
