package ca.tfuller.mctqr.mctest;

import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.imageregistration.ImReg;

public class MarkSheet {
	protected static final Logger logger = LogManager.getLogger(MarkSheet.class);

	private final ImReg imReg;
	private final McTestStudent mcTestStudent;
	private final McTest mcTest;

	public MarkSheet(McTest mcTest, McTestStudent mcTestStudent, ImReg imReg) {
		this.imReg = imReg;
		this.mcTestStudent = mcTestStudent;
		this.mcTest = mcTest;
	}

	public void markSheet() {
		// determine which bubbles are filled
		logger.info("imReg is " + imReg);
		logger.info("image is " + imReg.getImage());
		int numCorrect = 0;
		for (Entry<Integer, QuestionStudent> entry : mcTestStudent.getQuestions().entrySet()) {
			QuestionStudent q = entry.getValue();

			for (Bubble b : q.getBubbles()) {
				logger.debug("question " + q.getQuestionNumber() + " bubble is " + b.getScannedCircle());
				//double frac = b.getScannedCircle().getBlackFraction(imReg.getImage(),
				//		mcTest.getMcTestMetaData().getBubbleSize() - 2);
				double frac = b.getGeneratedCircle().getBlackFraction(imReg.getImage(),
						mcTest.getMcTestMetaData().getBubbleSize() - 2);
				//logger.debug("question " + q.getQuestionNumber() + " bubble " + b.getChoice() + " frac = " + frac
				//		+ " center = " + b.getScannedCircle().getCenter() + " radius = "
				//		+ (mcTest.getMcTestMetaData().getBubbleSize() - 2));
				if (frac < 0.1) {
					b.setFilled(true);
				} else {
					b.setFilled(false);
				}
				logger.debug("question " + q.getQuestionNumber() + " bubble " + b.getChoice() + " filled is  "
						+ b.getFilled());
			}

			q.dump();
			QuestionMaster qm = mcTest.getMcTestMaster().getQuestions().get(q.getQuestionNumber());
			if (q.isCorrect(qm.getCorrectAnswer())) {
				++numCorrect;
				logger.info("question " + q.getQuestionNumber() + " is correct: " + qm.getCorrectAnswer());
			} else {
				logger.info("question " + q.getQuestionNumber() + " is not correct, selected: -->"
						+ q.getAnswersAsString() + "<-- , correct is " + qm.getCorrectAnswer());
			}

		}
		mcTestStudent.setNumberCorrect(numCorrect);
		logger.info("number correct is " + numCorrect);
	}

}
