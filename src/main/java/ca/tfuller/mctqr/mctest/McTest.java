package ca.tfuller.mctqr.mctest;

import java.util.HashMap;
import java.util.Map;

public class McTest {
	private McTestMetaData mcTestMetaData = new McTestMetaData();
	private McTestMaster mcTestMaster = new McTestMaster();
	private Map<String, McTestStudent> mcTestStudentMap = new HashMap<String, McTestStudent>();

	public McTest() {

	}

	public McTestStudent getMcTestStudent(String studentId) {
		return mcTestStudentMap.get(studentId);
	}

	public McTestMetaData getMcTestMetaData() {
		return mcTestMetaData;
	}

	public void setMcTestMetaData(McTestMetaData mcTestMetaData) {
		this.mcTestMetaData = mcTestMetaData;
	}

	public McTestMaster getMcTestMaster() {
		return mcTestMaster;
	}

	public void setMcTestMaster(McTestMaster mcTestMaster) {
		this.mcTestMaster = mcTestMaster;
	}

	public Map<String, McTestStudent> getMcTestStudentMap() {
		return mcTestStudentMap;
	}

	public void setMcTestStudentMap(Map<String, McTestStudent> mcTestStudentMap) {
		this.mcTestStudentMap = mcTestStudentMap;
	}

}
