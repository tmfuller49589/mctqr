package ca.tfuller.mctqr.mctest;

import ca.tfuller.mctqr.geometry.Circle;

public class Bubble {

	private Circle generatedCircle;
	private Circle scannedCircle;
	private ChoiceEnum choice;
	private Boolean filled = false;

	public Bubble() {
		super();
	}

	public Bubble(Circle generatedCircle, ChoiceEnum choice) {
		super();
		this.generatedCircle = generatedCircle;
		this.choice = choice;
	}

	public Bubble(Bubble bm) {
		generatedCircle = bm.getGeneratedCircle();
		choice = bm.getChoice();
	}

	public Circle getGeneratedCircle() {
		return generatedCircle;
	}

	public void setGeneratedCircle(Circle generatedCircle) {
		this.generatedCircle = generatedCircle;
	}

	public Circle getScannedCircle() {
		return scannedCircle;
	}

	public void setScannedCircle(Circle scannedCircle) {
		this.scannedCircle = scannedCircle;
	}

	public ChoiceEnum getChoice() {
		return choice;
	}

	public void setChoice(ChoiceEnum choice) {
		this.choice = choice;
	}

	public Boolean getFilled() {
		return filled;
	}

	public void setFilled(Boolean filled) {
		this.filled = filled;
	}

}
