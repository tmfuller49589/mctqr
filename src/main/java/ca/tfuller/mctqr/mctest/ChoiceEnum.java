package ca.tfuller.mctqr.mctest;

public enum ChoiceEnum {
	A, B, C, D, E, F;

	public static ChoiceEnum getChoice(int val) {
		for (ChoiceEnum ce : ChoiceEnum.values()) {
			if (ce.ordinal() == val) {
				return ce;
			}
		}
		return null;
	}

	public static ChoiceEnum getChoice(String correctAnswer) {
		for (ChoiceEnum ce : ChoiceEnum.values()) {
			if (ce.name().compareTo(correctAnswer) == 0) {
				return ce;
			}
		}
		return null;
	}
}
