package ca.tfuller.mctqr.mctest;

import java.util.ArrayList;
import java.util.List;

public class QuestionAbstract {
	protected Integer questionNumber;
	protected final List<Bubble> bubbles = new ArrayList<Bubble>();

	public QuestionAbstract() {
		super();
	}

	public void addBubble(Bubble bubble) {
		bubbles.add(bubble);
	}

	public List<Bubble> getBubbles() {
		return bubbles;
	}

	public Integer getQuestionNumber() {
		return questionNumber;
	}

	public void setQuestionNumber(Integer questionNumber) {
		this.questionNumber = questionNumber;
	}

}
