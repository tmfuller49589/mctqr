package ca.tfuller.mctqr.mctest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.jfx.view.MessageWindow;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;

public abstract class McTestAbstract<T extends QuestionAbstract> {

	protected final List<Point2D> anchors = new ArrayList<Point2D>();

	protected static final Logger logger = LogManager.getLogger(McTestAbstract.class);
	protected Image image;
	protected File scannedFile;

	public McTestAbstract() {
	}

	public abstract Map<Integer, T> getQuestions();

	public Image getImage() {
		if (image == null) {
			try (FileInputStream fis = new FileInputStream(scannedFile)) {
				image = new Image(fis);
			} catch (IOException e) {
				e.printStackTrace();
				MessageWindow.showError(e.getMessage());
			}
		}
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public File getScannedFile() {
		return scannedFile;
	}

	public void setScannedFile(File scannedFile) {
		this.scannedFile = scannedFile;
	}

	public List<Point2D> getAnchors() {
		return anchors;
	}

}
