package ca.tfuller.mctqr.mctest;

import java.time.LocalDate;
import java.util.UUID;

import ca.tfuller.mctqr.jfx.view.MainController;
import ca.tfuller.mctqr.preferences.Preferences;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class McTestMetaData {
	protected final SimpleStringProperty courseName = new SimpleStringProperty();
	protected final SimpleStringProperty courseId = new SimpleStringProperty();
	protected final SimpleStringProperty instructor = new SimpleStringProperty();
	protected final SimpleIntegerProperty numberOfQuestions = new SimpleIntegerProperty();
	protected final SimpleIntegerProperty choicesPerQuestion = new SimpleIntegerProperty();
	protected final SimpleIntegerProperty columns = new SimpleIntegerProperty();
	protected final SimpleIntegerProperty bubbleSize = new SimpleIntegerProperty();
	protected final SimpleIntegerProperty anchorSize = new SimpleIntegerProperty();
	protected final SimpleObjectProperty<LocalDate> date = new SimpleObjectProperty<LocalDate>();
	protected final SimpleStringProperty testName = new SimpleStringProperty();
	protected final SimpleIntegerProperty fontSize = new SimpleIntegerProperty();
	protected final SimpleObjectProperty<UUID> testUuid = new SimpleObjectProperty<UUID>();

	public McTestMetaData() {
	}

	public void initialize() {
		testUuid.set(UUID.randomUUID());
	}

	public ObjectProperty<UUID> testUuidProperty() {
		return testUuid;
	}

	public ObjectProperty<LocalDate> dateProperty() {
		return date;
	}

	public IntegerProperty anchorSizeProperty() {
		return anchorSize;
	}

	public IntegerProperty choicesPerQuestionProperty() {
		return choicesPerQuestion;
	}

	public IntegerProperty fontSizeProperty() {
		return fontSize;
	}

	public IntegerProperty bubbleSizeProperty() {
		return bubbleSize;
	}

	public IntegerProperty columnsProperty() {
		return columns;
	}

	public IntegerProperty numberOfQuestionsProperty() {
		return numberOfQuestions;
	}

	public StringProperty testNameProperty() {
		return testName;
	}

	public StringProperty instructorNameProperty() {
		return instructor;
	}

	public StringProperty courseNameProperty() {
		return courseName;
	}

	public String getCourseName() {
		return courseName.get();
	}

	public void setCourseName(String courseName) {
		this.courseName.set(courseName);
	}

	public StringProperty courseIdProperty() {
		return courseId;
	}

	public String getCourseId() {
		return courseId.get();
	}

	public void setCourseId(String courseId) {
		this.courseId.set(courseId);
	}

	public int getNumberOfQuestions() {
		return numberOfQuestions.get();
	}

	public void setNumberOfQuestions(int numberOfQuestions) {
		this.numberOfQuestions.set(numberOfQuestions);
	}

	public int getChoicesPerQuestion() {
		return choicesPerQuestion.get();
	}

	public void setChoicesPerQuestion(int choicesPerQuestion) {
		this.choicesPerQuestion.set(choicesPerQuestion);
	}

	public int getColumns() {
		return columns.get();
	}

	public void setColumns(int columns) {
		this.columns.set(columns);
	}

	public int getBubbleSize() {
		return bubbleSize.get();
	}

	public void setBubbleSize(int bubbleSize) {
		this.bubbleSize.set(bubbleSize);
	}

	public int getAnchorSize() {
		return anchorSize.get();
	}

	public void setAnchorSize(int anchorSize) {
		this.anchorSize.set(anchorSize);
	}

	public LocalDate getDate() {
		return date.get();
	}

	public void setDate(LocalDate date) {
		this.date.set(date);
	}

	public String getTestName() {
		return testName.get();
	}

	public void setTestName(String testName) {
		this.testName.set(testName);
	}

	public int getFontSize() {
		return fontSize.get();
	}

	public void setFontSize(int fontSize) {
		this.fontSize.set(fontSize);
	}

	public String getInstructor() {
		return instructor.get();
	}

	public void setInstructor(String instructor) {
		this.instructor.set(instructor);
	}

	public UUID getTestUuid() {
		return testUuid.get();
	}

	public void setTestUuid(UUID testUuid) {
		this.testUuid.set(testUuid);
	}

	public void setDefaults() {
		Preferences p = MainController.getPreferences();
		p.dump();
		setAnchorSize(p.getAnchorSize());
		setBubbleSize(p.getBubbleSize());
		setChoicesPerQuestion(p.getChoicesPerQuestion());
		setColumns(p.getColumns());
		setFontSize(p.getFontSize());
		setInstructor(p.getInstructor());
		setNumberOfQuestions(p.getNumberOfQuestions());

	}
}
