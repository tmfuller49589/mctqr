package ca.tfuller.mctqr.imageregistration;

import java.awt.geom.AffineTransform;

import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.QRDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.transform.Affine;
import javafx.scene.transform.MatrixType;

public class RegisterTargetToSource {

	protected static final Logger logger = LogManager.getLogger(RegisterTargetToSource.class);
	RealMatrix generatedAnchors;
	RealMatrix scannedAnchors;

	public RegisterTargetToSource(RealMatrix generatedAnchors, RealMatrix scannedAnchors) {
		this.generatedAnchors = generatedAnchors;
		this.scannedAnchors = scannedAnchors;
	}

	public void register() {
		computeAffine();
	}

	public AffineTransform computeAwtAffine() {
		Affine aff = computeAffine();

		double[] affMat = new double[6];
		affMat[0] = aff.getMxx();
		affMat[1] = aff.getMyx();
		affMat[2] = aff.getMxy();
		affMat[3] = aff.getMyy();
		affMat[4] = aff.getTx();
		affMat[5] = aff.getTy();

		AffineTransform at = new AffineTransform(affMat);
		return at;
	}

	public Affine computeAffine() {
		logger.debug("dumping source anchors");
		dumpPointList(generatedAnchors);

		RealMatrix X = MatrixOps.buildXfromAnchors(scannedAnchors);
		logger.debug("dumping X");
		MatrixOps.dump(X);
		RealMatrix xprime = MatrixOps.buildXprimeColumnMatrix(generatedAnchors);
		MatrixOps.dump(xprime);

		DecompositionSolver solver = new QRDecomposition(X).getSolver();
		RealMatrix affine = solver.solve(xprime);

		RealMatrix affine2 = MatrixOps.reshape(affine, 2, 3);
		RealMatrix tmp = MatrixUtils.createRealMatrix(1, 3);
		tmp.setEntry(0, 0, 0);
		tmp.setEntry(0, 1, 0);
		tmp.setEntry(0, 2, 1);
		affine2 = MatrixOps.vstack(affine2, tmp);

		double[] flatAff = flattenMatrix(affine2);
		Affine aff = new Affine(flatAff, MatrixType.MT_2D_3x3, 0);
		logger.debug(aff);

		logger.debug("dumping reshaped affine");
		MatrixOps.dump(affine2);

		// compute transformed points
		RealMatrix xComputed = affine2.multiply(scannedAnchors);

		logger.debug("generatedAnchors");
		dumpPointList(generatedAnchors);

		logger.debug("scannedAnchors");
		dumpPointList(scannedAnchors);

		logger.debug("mapped scanned Anchors");
		dumpPointList(xComputed);

		return aff;

	}

	private double[] flattenMatrix(RealMatrix m) {
		double[] fm = new double[m.getRowDimension() * m.getColumnDimension()];
		for (int i = 0; i < m.getRowDimension(); ++i) {
			for (int j = 0; j < m.getColumnDimension(); ++j) {
				int idx = i * m.getRowDimension() + j;
				fm[idx] = m.getEntry(i, j);
			}
		}
		return fm;
	}

	public static void dumpPointList(RealMatrix points) {
		int point = 1;
		for (int col = 0; col < points.getColumnDimension(); ++col) {
			logger.debug("point " + point + " (" + points.getEntry(0, col) + ", " + points.getEntry(1, col) + ", "
					+ points.getEntry(2, col) + ")");
			++point;
		}
	}
}
