package ca.tfuller.mctqr.imageregistration;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ca.tfuller.mctqr.geometry.AnchorFinder;
import ca.tfuller.mctqr.geometry.ImageToArray;
import ca.tfuller.mctqr.geometry.PathUtils;
import ca.tfuller.mctqr.geometry.Square;
import ca.tfuller.mctqr.marchingsquares.Algorithm;
import ca.tfuller.mctqr.mctest.McTest;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.shape.Path;
import javafx.scene.transform.Affine;
import javafx.scene.transform.MatrixType;

public class ImReg {

	protected static final Logger logger = LogManager.getLogger(ImReg.class);
	private RealMatrix scannedAnchorMatrix;
	private Image image;
	private Path[] contourLevelPaths = null;
	private List<Path> contourLoops;
	private AnchorFinder anchorFinder;
	private final McTest mcTest;
	double levels[] = { 0.3, 0.4, 0.5, 0.6 };

	public ImReg(McTest mcTest, Image image) {
		this.image = image;
		this.mcTest = mcTest;
	}

	public static Affine createAffine() {

		double[] flatAffine2 = new double[] { 0.7209370172727233, -0.00938380576090639, -6.139383990765426,
				-0.01410160884913544, 0.720864840883902, -26.83594291467627, 0.0, 0.0, 1.0 };
		double[] flatAffine1 = new double[] { 0.7209370172727233, -0.00938380576090639, 0, -0.01410160884913544,
				0.720864840883902, 0, 0.0, 0.0, 1.0 };
		double[] flatAffine = new double[] { 1, 0, 0, -0, 1, 0, 0.0, 0.0, 1.0 };
		Affine aff = new Affine(flatAffine2, MatrixType.MT_2D_3x3, 0);
		return aff;
	}

	public AnchorFinder getAnchorFinder() {
		return anchorFinder;
	}

	public void register() throws InterruptedException, ExecutionException {

		computeContours(image);

		int index = 0;
		for (Path path : contourLevelPaths) {
			// find the anchors from the contours
			contourLoops = PathUtils.splitPaths(path);
			anchorFinder = new AnchorFinder(contourLoops, image);
			anchorFinder.findAnchors();
			if (anchorFinder.getAnchors().size() == 7) {
				logger.debug("got seven anchors on brightness level " + levels[index]);
				break;
			}
			++index;
		}

		scannedAnchorMatrix = convertAnchorListToMatrix(anchorFinder.getAnchors());
		RealMatrix generatedAnchors = convertPointListToMatrix(mcTest.getMcTestMaster().getAnchors());

		scannedAnchorMatrix = orderAnchors(scannedAnchorMatrix);
		generatedAnchors = orderAnchors(generatedAnchors);

		RegisterTargetToSource rtts = new RegisterTargetToSource(generatedAnchors, scannedAnchorMatrix);
		AffineTransform affine = rtts.computeAwtAffine();
		//Affine affine = createAffine();

		logger.debug("javafx affine transform");
		logger.debug(affine);

		BufferedImage bi = SwingFXUtils.fromFXImage(image, null);
		AffineTransformOp op = new AffineTransformOp(affine, AffineTransformOp.TYPE_BICUBIC);
		BufferedImage transformedImage = op.filter(bi, null);
		image = SwingFXUtils.toFXImage(transformedImage, null);

		// recompute contours after rotation
		computeContours(image);
		contourLoops = PathUtils.splitPaths(contourLevelPaths[index]);
		anchorFinder = new AnchorFinder(contourLoops, image);
		anchorFinder.findAnchors();
		if (anchorFinder.getAnchors().size() != 7) {
			for (Path path : contourLevelPaths) {
				// find the anchors from the contours
				contourLoops = PathUtils.splitPaths(path);
				anchorFinder = new AnchorFinder(contourLoops, image);
				anchorFinder.findAnchors();
				if (anchorFinder.getAnchors().size() == 7) {
					break;
				}
				++index;
			}
		}

		scannedAnchorMatrix = convertAnchorListToMatrix(anchorFinder.getAnchors());
		generatedAnchors = convertPointListToMatrix(mcTest.getMcTestMaster().getAnchors());

		scannedAnchorMatrix = orderAnchors(scannedAnchorMatrix);
		generatedAnchors = orderAnchors(generatedAnchors);

		anchorFinder.dump();

		logger.debug("================= registering done ===========================");

	}

	public List<Path> getContourLoops() {
		return contourLoops;
	}

	private void computeContours(Image img) throws InterruptedException, ExecutionException {
		Algorithm alg = new Algorithm();

		double[][] data = ImageToArray.convert(img);
		contourLevelPaths = alg.buildContours(data, levels);

		logger.debug("number of contours (1 contour per level) " + contourLevelPaths.length);

		if (contourLevelPaths[0].getElements().size() == 0) {
			logger.debug("oops, no path elements");
		}
	}

	public Path[] getContourLevelPaths() {
		return contourLevelPaths;
	}

	private RealMatrix convertPointListToMatrix(List<Point2D> points) {
		RealMatrix matrix = MatrixUtils.createRealMatrix(3, 7);
		int col = 0;
		for (Point2D p : points) {
			matrix.setEntry(0, col, p.getX());
			matrix.setEntry(1, col, p.getY());
			matrix.setEntry(2, col, 1);
			++col;
		}
		return matrix;
	}

	private RealMatrix convertAnchorListToMatrix(List<Square> anchors) {
		RealMatrix matrix = MatrixUtils.createRealMatrix(3, 7);
		int col = 0;
		for (Square s : anchors) {
			matrix.setEntry(0, col, s.getCenter().getX());
			matrix.setEntry(1, col, s.getCenter().getY());
			matrix.setEntry(2, col, 1);
			++col;
		}
		return matrix;
	}

	public RealMatrix orderAnchors(RealMatrix origMatrix) {
		RealMatrix m = MatrixUtils.createRealMatrix(origMatrix.getRowDimension(), origMatrix.getColumnDimension());
		int[][] idx = computeAnchorIndexMatrix(origMatrix);

		dumpAnchorIndexMatrix(idx);

		// upper left
		int idx2 = idx[0][0];
		m.setEntry(0, 0, origMatrix.getEntry(0, idx2));
		m.setEntry(1, 0, origMatrix.getEntry(1, idx2));
		m.setEntry(2, 0, origMatrix.getEntry(2, idx2));

		// mid top
		idx2 = idx[0][1];
		m.setEntry(0, 1, origMatrix.getEntry(0, idx2));
		m.setEntry(1, 1, origMatrix.getEntry(1, idx2));
		m.setEntry(2, 1, origMatrix.getEntry(2, idx2));

		// left mid
		idx2 = idx[1][0];
		m.setEntry(0, 2, origMatrix.getEntry(0, idx2));
		m.setEntry(1, 2, origMatrix.getEntry(1, idx2));
		m.setEntry(2, 2, origMatrix.getEntry(2, idx2));

		// right mid
		idx2 = idx[1][2];
		m.setEntry(0, 3, origMatrix.getEntry(0, idx2));
		m.setEntry(1, 3, origMatrix.getEntry(1, idx2));
		m.setEntry(2, 3, origMatrix.getEntry(2, idx2));

		// bot left
		idx2 = idx[2][0];
		m.setEntry(0, 4, origMatrix.getEntry(0, idx2));
		m.setEntry(1, 4, origMatrix.getEntry(1, idx2));
		m.setEntry(2, 4, origMatrix.getEntry(2, idx2));

		// bot mid
		idx2 = idx[2][1];
		m.setEntry(0, 5, origMatrix.getEntry(0, idx2));
		m.setEntry(1, 5, origMatrix.getEntry(1, idx2));
		m.setEntry(2, 5, origMatrix.getEntry(2, idx2));

		// bot right
		idx2 = idx[2][2];
		m.setEntry(0, 6, origMatrix.getEntry(0, idx2));
		m.setEntry(1, 6, origMatrix.getEntry(1, idx2));
		m.setEntry(2, 6, origMatrix.getEntry(2, idx2));

		return m;
	}

	public static void dumpAnchorIndexMatrix(int[][] m) {
		logger.debug("dumping anchor index matrix");
		for (int i = 0; i < 3; ++i) {
			String row = "";
			for (int j = 0; j < 3; ++j) {
				row += m[i][j] + " ";
			}
			logger.debug(row);
		}
	}

	private static int[][] computeAnchorIndexMatrix(RealMatrix points) {
		// matrix is a 3x3 matrix that stores the index (column) of the point in
		// points
		// an anchor was located at that position
		// There are 7 anchors on the mc test.
		// One corner is missing and there is no anchor in the middle.
		// e.g. if top right anchor was missing then matrix would contain
		// -1 0 1
		// 6 -1 4
		// 2 4 3
		int[][] matrix = new int[3][3];
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				matrix[i][j] = -1;
			}
		}

		double minX = Integer.MAX_VALUE;
		double maxX = Integer.MIN_VALUE;
		double minY = Integer.MAX_VALUE;
		double maxY = Integer.MIN_VALUE;

		// determine max and min values from anchor points
		for (int col = 0; col < points.getColumnDimension(); ++col) {
			minX = Math.min(minX, points.getEntry(0, col));
			maxX = Math.max(maxX, points.getEntry(0, col));
			minY = Math.min(minY, points.getEntry(1, col));
			maxY = Math.max(maxY, points.getEntry(1, col));
		}

		// Scale the anchor point coords to 0, 1, 2
		// and use them for an index into matrix
		for (int col = 0; col < points.getColumnDimension(); ++col) {
			float x = Math.round((points.getEntry(0, col) - minX) / (maxX - minX) * 2f);
			float y = Math.round((points.getEntry(1, col) - minY) / (maxY - minY) * 2f);
			logger.debug("examining point #" + col + " (" + points.getEntry(0, col) + ", " + points.getEntry(1, col)
					+ ") --> (" + y + ", " + x + ")");

			// store the points index in the matrix
			matrix[(int) y][(int) x] = col;
		}

		return matrix;
	}

	public double getLeftAnchor() {
		double left = Math.min(scannedAnchorMatrix.getEntry(0, 0), scannedAnchorMatrix.getEntry(0, 2));
		left = Math.min(left, scannedAnchorMatrix.getEntry(0, 4));
		return left;
	}

	public double getRightAnchor() {
		double right = Math.max(scannedAnchorMatrix.getEntry(0, 3), scannedAnchorMatrix.getEntry(0, 6));
		return right;
	}

	public double getTopAnchor() {
		double top = Math.min(scannedAnchorMatrix.getEntry(1, 0), scannedAnchorMatrix.getEntry(1, 1));
		return top;
	}

	public double getBottomAnchor() {
		double bottom = Math.max(scannedAnchorMatrix.getEntry(1, 4), scannedAnchorMatrix.getEntry(1, 5));
		bottom = Math.max(bottom, scannedAnchorMatrix.getEntry(1, 6));
		return bottom;
	}

	public static void dumpPointList(RealMatrix points) {
		int point = 1;
		for (int col = 0; col < points.getColumnDimension(); ++col) {
			logger.debug("point " + point + " (" + points.getEntry(0, col) + ", " + points.getEntry(1, col) + ", "
					+ points.getEntry(2, col) + ")");
			++point;
		}
	}

	public RealMatrix getAnchors() {
		return scannedAnchorMatrix;
	}

	public Image getImageCropped() {
		double left = getLeftAnchor();
		double top = getTopAnchor();
		double right = getRightAnchor();
		double bot = getBottomAnchor();
		double width = right - left;
		double height = bot - top;

		//BinaryBitmap bmc = binaryBitmap.crop((int) left, (int) top, (int) width, (int) height);
		return image;
		// return bmc;
	}

	public Image getImage() {
		return image;
	}

}
