package ca.tfuller.mctqr.imageregistration;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MatrixOps {
	protected static final Logger logger = LogManager.getLogger(MatrixOps.class);

	public static RealMatrix toRowMatrix(RealMatrix x) {
		RealMatrix col = toColumnMatrix(x);
		return col.transpose();
	}

	public static RealMatrix toColumnMatrix(RealMatrix x) {
		RealMatrix col = MatrixUtils.createRealMatrix(x.getColumnDimension() * x.getRowDimension(), 1);
		for (int i = 0; i < x.getRowDimension(); ++i) {
			for (int j = 0; j < x.getColumnDimension(); ++j) {
				col.setEntry(i * x.getColumnDimension() + j, 0, x.getEntry(i, j));
			}
		}
		return col;
	}

	/*
	 * Compute distance between two points in the matrix m
	 * given column indices i and j.
	 */
	public static double distance(RealMatrix m, int i, int j) {
		double dx = m.getEntry(0, i) - m.getEntry(0, j);
		double dy = m.getEntry(1, i) - m.getEntry(1, j);
		double dist = Math.sqrt(dx * dx + dy * dy);
		return dist;
	}

	public static RealMatrix buildTransform(double tx, double ty) {
		RealMatrix trans = MatrixUtils.createRealMatrix(3, 3);
		trans.setEntry(0, 0, 1);
		trans.setEntry(0, 1, 0);
		trans.setEntry(0, 2, tx);
		trans.setEntry(1, 0, 0);
		trans.setEntry(1, 1, 1);
		trans.setEntry(1, 2, ty);
		trans.setEntry(2, 0, 0);
		trans.setEntry(2, 1, 0);
		trans.setEntry(2, 2, 1);
		return trans;
	}

	public static RealMatrix buildRotation(double angle) {
		RealMatrix trans = MatrixUtils.createRealMatrix(3, 3);
		trans.setEntry(0, 0, Math.cos(angle));
		trans.setEntry(0, 1, -Math.sin(angle));
		trans.setEntry(0, 2, 0);
		trans.setEntry(1, 0, Math.sin(angle));
		trans.setEntry(1, 1, Math.cos(angle));
		trans.setEntry(1, 2, 0);
		trans.setEntry(2, 0, 0);
		trans.setEntry(2, 1, 0);
		trans.setEntry(2, 2, 1);
		return trans;
	}

	public static RealMatrix buildX(RealMatrix orig) {
		RealMatrix X = MatrixUtils.createRealMatrix(orig.getRowDimension() * 2, 6);
		int rowidx = 0;
		for (int i = 0; i < orig.getRowDimension(); ++i) {
			X.setEntry(rowidx, 0, orig.getEntry(i, 0));
			X.setEntry(rowidx, 1, orig.getEntry(i, 1));
			X.setEntry(rowidx, 2, 1);
			X.setEntry(rowidx, 3, 0);
			X.setEntry(rowidx, 4, 0);
			X.setEntry(rowidx, 5, 0);
			++rowidx;
			X.setEntry(rowidx, 0, 0);
			X.setEntry(rowidx, 1, 0);
			X.setEntry(rowidx, 2, 0);
			X.setEntry(rowidx, 3, orig.getEntry(i, 0));
			X.setEntry(rowidx, 4, orig.getEntry(i, 1));
			X.setEntry(rowidx, 5, 1);
			++rowidx;
		}
		return X;
	}

	public static RealMatrix buildXfromAnchors(RealMatrix orig) {
		RealMatrix X = MatrixUtils.createRealMatrix(orig.getColumnDimension() * 2, 6);
		int rowidx = 0;
		for (int i = 0; i < orig.getColumnDimension(); ++i) {
			X.setEntry(rowidx, 0, orig.getEntry(0, i));
			X.setEntry(rowidx, 1, orig.getEntry(1, i));
			X.setEntry(rowidx, 2, orig.getEntry(2, i));
			X.setEntry(rowidx, 3, 0);
			X.setEntry(rowidx, 4, 0);
			X.setEntry(rowidx, 5, 0);
			++rowidx;
			X.setEntry(rowidx, 0, 0);
			X.setEntry(rowidx, 1, 0);
			X.setEntry(rowidx, 2, 0);
			X.setEntry(rowidx, 3, orig.getEntry(0, i));
			X.setEntry(rowidx, 4, orig.getEntry(1, i));
			X.setEntry(rowidx, 5, orig.getEntry(2, i));
			++rowidx;
		}
		return X;
	}

	public static RealMatrix reshape(RealMatrix m, int rows, int cols) {
		if (rows * cols != m.getColumnDimension() * m.getRowDimension()) {
			logger.debug("cannot reshape");
		}

		RealMatrix colM = toColumnMatrix(m);
		RealMatrix newM = MatrixUtils.createRealMatrix(rows, cols);
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < cols; ++j) {
				int idx = i * cols + j;
				colM.getEntry(idx, 0);
				newM.setEntry(i, j, colM.getEntry(idx, 0));
			}
		}
		return newM;
	}

	public static RealMatrix deleteRow(RealMatrix mat, int row) {
		RealMatrix result = MatrixUtils.createRealMatrix(mat.getRowDimension() - 1, mat.getColumnDimension());
		int idx = 0;
		for (int i = 0; i < mat.getRowDimension(); ++i) {
			if (i != row) {
				for (int j = 0; j < mat.getColumnDimension(); ++j) {
					result.setEntry(idx, j, mat.getEntry(i, j));
				}
				++idx;
			}
		}
		return result;
	}

	public static void dump(RealMatrix mat) {
		logger.debug("dumping matrix =============================");
		for (int i = 0; i < mat.getRowDimension(); ++i) {
			String s = "";
			for (int j = 0; j < mat.getColumnDimension(); ++j) {
				s += mat.getEntry(i, j) + " ";
			}
			logger.debug(s);
		}
		logger.debug("=============================");
	}

	public static RealMatrix hstack(RealMatrix a, RealMatrix b) {
		if (a.getRowDimension() != b.getRowDimension()) {
			logger.debug("row sizes don't match");
			return null;
		}
		int cols = a.getColumnDimension() + b.getColumnDimension();
		RealMatrix mat = MatrixUtils.createRealMatrix(a.getRowDimension(), cols);
		for (int i = 0; i < a.getRowDimension(); ++i) {
			for (int j = 0; j < a.getColumnDimension(); ++j) {
				mat.setEntry(i, j, a.getEntry(i, j));
			}
		}
		for (int i = 0; i < b.getRowDimension(); ++i) {
			for (int j = 0; j < b.getColumnDimension(); ++j) {
				mat.setEntry(i, j + a.getColumnDimension(), b.getEntry(i, j));
			}
		}
		return mat;
	}

	public static RealMatrix vstack(RealMatrix a, RealMatrix b) {
		if (a.getColumnDimension() != b.getColumnDimension()) {
			logger.debug("column sizes don't match");
			return null;
		}
		int rows = a.getRowDimension() + b.getRowDimension();
		RealMatrix mat = MatrixUtils.createRealMatrix(rows, a.getColumnDimension());
		for (int i = 0; i < a.getRowDimension(); ++i) {
			for (int j = 0; j < a.getColumnDimension(); ++j) {
				mat.setEntry(i, j, a.getEntry(i, j));
			}
		}
		for (int i = 0; i < b.getRowDimension(); ++i) {
			for (int j = 0; j < b.getColumnDimension(); ++j) {
				mat.setEntry(i + a.getRowDimension(), j, b.getEntry(i, j));
			}
		}
		return mat;
	}

	public static RealMatrix createOnesMatrix(int ii, int jj) {
		return createValueMatrix(ii, jj, 1.0);
	}

	public static RealMatrix createZerosMatrix(int ii, int jj, double value) {
		return createValueMatrix(ii, jj, 1.0);
	}

	public static RealMatrix createValueMatrix(int ii, int jj, double value) {
		RealMatrix mat = MatrixUtils.createRealMatrix(ii, jj);
		for (int i = 0; i < ii; ++i) {
			for (int j = 0; j < jj; ++j) {
				mat.setEntry(i, j, value);
			}
		}
		return mat;
	}

	/* 
	 * build a column matrix [x1' y1' x2' y2' x3' y3' ...]
	 * anchors has
	 * x1 x2 x3 ... x6
	 * y1 y2 y3 ... y6
	 * 1  1  1  ... 1
	 */
	public static RealMatrix buildXprimeColumnMatrix(RealMatrix anchors) {
		RealMatrix xprime = MatrixUtils.createRealMatrix(anchors.getColumnDimension() * 2, 1);
		int idx = 0;
		for (int i = 0; i < anchors.getColumnDimension(); ++i) {
			xprime.setEntry(idx, 0, anchors.getEntry(0, i));
			++idx;
			xprime.setEntry(idx, 0, anchors.getEntry(1, i));
			++idx;
		}
		return xprime;
	}
}
