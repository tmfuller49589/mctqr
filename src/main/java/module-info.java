module mctqr {

	opens ca.tfuller.mctqr.jfx.view;

	exports ca.tfuller.mctqr.jfx.util;
	exports ca.tfuller.mctqr.jfx.model;
	exports ca.tfuller.mctqr.preferences;
	exports ca.tfuller.mctqr.sheetgenerator;
	exports ca.tfuller.mctqr.imageregistration;
	exports ca.tfuller.mctqr.jfx.application;
	exports ca.tfuller.mctqr.jfx.view;
	exports ca.tfuller.mctqr.utils;
	exports ca.tfuller.mctqr.geometry;
	exports ca.tfuller.mctqr.googleclassroom;
	exports ca.tfuller.mctqr.marchingsquares;
	exports ca.tfuller.mctqr.mctest;
	exports ca.tfuller.mctqr.io;

	requires com.google.zxing;
	requires com.google.zxing.javase;
	requires commons.math3;
	requires google.api.client;
	requires google.api.services.classroom.v1.rev120;
	requires google.http.client;
	requires google.http.client.jackson2;
	requires google.oauth.client;
	requires google.oauth.client.java6;
	requires google.oauth.client.jetty;
	requires jasperreports;
	requires java.desktop;
	requires java.sql;
	requires javafx.base;
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires javafx.swing;

	requires org.apache.commons.io;
	requires org.apache.logging.log4j;
	requires commons.lang;
	requires google.api.services.sheets.v4.rev614;
}