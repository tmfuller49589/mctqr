package unneeded;

import java.awt.event.ItemListener;
import java.awt.event.MouseWheelListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// TODO prevent access to addItem(String) should only have access to add(IdName)

/**
 * 
 * @author tfuller
 */
public class IdNameObjectComboBox<T> extends JComboBox<IdNameObject<T>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected static final Logger logger = LogManager.getLogger(IdNameObjectComboBox.class);

	protected List<IdNameObject<T>> idNameList = null;
	private ItemListener itemListeners[] = null;
	private MouseWheelListener mouseWheelListeners[] = null;
	private IdNameObject<T> previousIdName = null;
	private final Class<T> clazz;

	/**
	 *
	 */
	protected static final boolean enable = true;
	/**
	 *
	 */
	protected static final boolean disable = false;

	public IdNameObjectComboBox(Class<T> clazz) {
		super();
		idNameList = new ArrayList<IdNameObject<T>>();
		previousIdName = null;

		this.clazz = clazz;
	}

	public IdNameObject<T> getPreviousIdName() {
		return previousIdName;
	}

	/**
	 *
	 */
	public void setListeners() {
		itemListeners = getItemListeners();
		mouseWheelListeners = getMouseWheelListeners();
	}

	/**
	 * 
	 * @return
	 */
	protected boolean previous() {
		previousIdName = getCurrentIdNameObject();
		boolean retval = false;
		int idx = this.getSelectedIndex();
		idx = idx < 0 ? 0 : idx;
		if (idx > 0) {
			--idx;
			retval = true;
		}
		this.setSelectedIndex(idx);
		return retval;
	}

	/**
	 * 
	 * @return
	 */
	protected boolean next() {
		previousIdName = getCurrentIdNameObject();
		boolean retval = false;
		int idx = this.getSelectedIndex();
		idx = idx < 0 ? 0 : idx;
		if (idx < (this.getItemCount() - 1)) {
			++idx;
			retval = true;
		}
		this.setSelectedIndex(idx);
		return retval;
	}

	/**
	 *
	 */
	protected void first() {
		previousIdName = getCurrentIdNameObject();
		int idx = 0;
		this.setSelectedIndex(idx);
	}

	/**
	 * 
	 */
	protected void last() {
		previousIdName = getCurrentIdNameObject();
		int idx = this.getItemCount();
		this.setSelectedIndex(idx - 1);
	}

	/**
	 *
	 */
	public void removeAllListeners() {
		if (itemListeners != null) {
			for (ItemListener il : itemListeners) {
				removeItemListener(il);
			}
		}
		if (mouseWheelListeners != null) {
			for (MouseWheelListener mwl : mouseWheelListeners) {
				removeMouseWheelListener(mwl);
			}
		}
		itemListeners = getItemListeners();
	}

	@Override
	public void removeAllItems() {
		idNameList.clear();
		super.removeAllItems();
	}

	@Override
	public void addItem(IdNameObject<T> idName) {
		logger.debug("adding " + idName.getId() + " " + idName.getName());
		idNameList.add(idName);
		super.addItem(idName);
	}

	public void add(T object) {

		Method getNameMeth;
		try {
			getNameMeth = clazz.getMethod("getName");

			String name = (String) getNameMeth.invoke(object);

			String getIdMethodName = "get" + clazz.getSimpleName() + "Id";
			Method getIdMeth = clazz.getMethod(getIdMethodName);

			String id = (String) getIdMeth.invoke(object);

			IdNameObject<T> idno = new IdNameObject<T>(id, name, object);
			logger.debug("id=" + id + " name=" + name + " object is " + object);

			addItem(idno);
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param bv
	 */
	public void enableListeners(boolean bv) {
		if (itemListeners != null) {
			for (ItemListener il : itemListeners) {
				if (bv == true) {
					addItemListener(il);
				} else {
					removeItemListener(il);
				}
			}
		}
		if (mouseWheelListeners != null) {
			for (MouseWheelListener mwl : mouseWheelListeners) {
				if (bv == true) {
					addMouseWheelListener(mwl);
				} else {
					removeMouseWheelListener(mwl);
				}
			}
		}
	}

	/**
	 * 
	 * @param id
	 */
	protected void delete(String id) {
		IdNameObject<T> idNameToDelete = null;
		for (IdNameObject<T> idName : idNameList) {
			if (idName.getId().compareTo(id) == 0) {
				idNameToDelete = idName;
				break;
			}
		}

		if (idNameToDelete == null) {
			return;
		}

		enableListeners(disable);
		removeItem(idNameToDelete);
		idNameList.remove(idNameToDelete);
		enableListeners(enable);

	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public boolean setCurrentId(Long id) {
		previousIdName = getCurrentIdNameObject();
		boolean found = false;
		int idx = 0;
		for (IdNameObject<T> idName : idNameList) {
			if (idName.getId().equals(id)) {
				found = true;
				break;
			}
			++idx;
		}
		if (found == true) {
			setSelectedIndex(idx);

			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public boolean setCurrentObject(T object) {
		previousIdName = getCurrentIdNameObject();
		boolean found = false;
		int idx = 0;
		for (IdNameObject<T> idName : idNameList) {
			if (idName.equals(object)) {
				found = true;
				break;
			}
			++idx;
		}
		if (found == true) {
			setSelectedIndex(idx);

			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public boolean setCurrentName(String name) {
		previousIdName = getCurrentIdNameObject();
		boolean found = false;
		int idx = 0;
		for (IdNameObject<T> idName : idNameList) {
			if (idName.getName().equals(name)) {
				found = true;
				break;
			}
			++idx;
		}
		if (found == true) {
			setSelectedIndex(idx);
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @return
	 */
	public String getCurrentId() {
		IdNameObject<T> idName = getCurrentIdNameObject();
		if (idName != null) {
			return idName.getId();
		}
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String getCurrentName() {
		IdNameObject<T> idName = getCurrentIdNameObject();
		if (idName != null) {
			return idName.getName();
		}
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public IdNameObject<T> getCurrentIdNameObject() {
		int idx = getSelectedIndex();
		if (idx >= 0) {
			return idNameList.get(idx);
		}
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public int length() {
		return idNameList.size();
	}

	/**
	 * 
	 * @param evt
	 */
	public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
		if (evt.getWheelRotation() < 0) {
			previous();
		} else {
			next();
		}
	}

	public List<IdNameObject<T>> getIdNameList() {
		return idNameList;
	}

	public void setIdNameObjectList(List<IdNameObject<T>> idNameList) {
		enableListeners(false);
		removeAllItems();
		this.idNameList = idNameList;
		for (IdNameObject<T> idName : idNameList) {
			super.addItem(idName);
		}
		if (idNameList.size() > 0) {
			setSelectedIndex(0);
		}
		enableListeners(true);
		revalidate();
		repaint();
	}

	public IdNameObject<T> getIdNameObjectByName(String name) {
		for (IdNameObject<T> idName : idNameList) {
			if (idName.getName().compareTo(name) == 0) {
				return idName;
			}
		}
		return null;
	}

	public IdNameObject<T> getIdNameObjectById(String id) {
		for (IdNameObject<T> idName : idNameList) {
			if (idName.getId().compareTo(id) == 0) {
				return idName;
			}
		}
		return null;
	}
}
