package unneeded;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class PointMatrix {
	private final RealMatrix matrix = MatrixUtils.createRealMatrix(3, 1);

	public PointMatrix(double x, double y) {
		matrix.setEntry(0, 0, x);
		matrix.setEntry(1, 0, y);
		matrix.setEntry(2, 0, 1);
	}

	public PointMatrix() {
	}

	public double getX() {
		return matrix.getEntry(0, 0);
	}

	public void setX(double x) {
		matrix.setEntry(0, 0, x);
	}

	public double getY() {
		return matrix.getEntry(1, 0);
	}

	public void setY(double y) {
		matrix.setEntry(1, 0, y);
	}

}
